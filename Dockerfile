FROM node:lts-alpine

WORKDIR /app
ENV NODE_ENV production
ENV TZ=Asia/Jakarta
COPY . .

RUN apk add tzdata \ 
  && cp .env.example .env \
  && npm i && npm run build \ 
  && npm ci --only=production

EXPOSE 4100
EXPOSE 4101

CMD ["npm", "run", "start:prod"]
