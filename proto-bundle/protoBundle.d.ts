import * as $protobuf from 'protobufjs';
/** Namespace Auth. */
export namespace Auth {
  /** Properties of a LoginRequest. */
  interface ILoginRequest {
    /** LoginRequest body */
    body?: Auth.LoginRequest.IBody | null;
  }

  /** Represents a LoginRequest. */
  class LoginRequest implements ILoginRequest {
    /**
     * Constructs a new LoginRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.ILoginRequest);

    /** LoginRequest body. */
    public body?: Auth.LoginRequest.IBody | null;

    /**
     * Creates a new LoginRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LoginRequest instance
     */
    public static create(properties?: Auth.ILoginRequest): Auth.LoginRequest;

    /**
     * Encodes the specified LoginRequest message. Does not implicitly {@link Auth.LoginRequest.verify|verify} messages.
     * @param message LoginRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.ILoginRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified LoginRequest message, length delimited. Does not implicitly {@link Auth.LoginRequest.verify|verify} messages.
     * @param message LoginRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.ILoginRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a LoginRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.LoginRequest;

    /**
     * Decodes a LoginRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.LoginRequest;

    /**
     * Verifies a LoginRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a LoginRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LoginRequest
     */
    public static fromObject(object: { [k: string]: any }): Auth.LoginRequest;

    /**
     * Creates a plain object from a LoginRequest message. Also converts values to other types if specified.
     * @param message LoginRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.LoginRequest,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this LoginRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace LoginRequest {
    /** Properties of a Body. */
    interface IBody {
      /** Body username */
      username?: string | null;

      /** Body password */
      password?: string | null;

      /** Body token */
      token?: string | null;
    }

    /** Represents a Body. */
    class Body implements IBody {
      /**
       * Constructs a new Body.
       * @param [properties] Properties to set
       */
      constructor(properties?: Auth.LoginRequest.IBody);

      /** Body username. */
      public username: string;

      /** Body password. */
      public password: string;

      /** Body token. */
      public token: string;

      /**
       * Creates a new Body instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Body instance
       */
      public static create(
        properties?: Auth.LoginRequest.IBody,
      ): Auth.LoginRequest.Body;

      /**
       * Encodes the specified Body message. Does not implicitly {@link Auth.LoginRequest.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Auth.LoginRequest.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link Auth.LoginRequest.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Auth.LoginRequest.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Auth.LoginRequest.Body;

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Auth.LoginRequest.Body;

      /**
       * Verifies a Body message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Body
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Auth.LoginRequest.Body;

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @param message Body
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Auth.LoginRequest.Body,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Body to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a LoginResponse. */
  interface ILoginResponse {
    /** LoginResponse success */
    success?: boolean | null;

    /** LoginResponse data */
    data?: Auth.LoginResponse.IData | null;

    /** LoginResponse message */
    message?: string | null;
  }

  /** Represents a LoginResponse. */
  class LoginResponse implements ILoginResponse {
    /**
     * Constructs a new LoginResponse.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.ILoginResponse);

    /** LoginResponse success. */
    public success: boolean;

    /** LoginResponse data. */
    public data?: Auth.LoginResponse.IData | null;

    /** LoginResponse message. */
    public message?: string | null;

    /** LoginResponse _message. */
    public _message?: 'message';

    /**
     * Creates a new LoginResponse instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LoginResponse instance
     */
    public static create(properties?: Auth.ILoginResponse): Auth.LoginResponse;

    /**
     * Encodes the specified LoginResponse message. Does not implicitly {@link Auth.LoginResponse.verify|verify} messages.
     * @param message LoginResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.ILoginResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified LoginResponse message, length delimited. Does not implicitly {@link Auth.LoginResponse.verify|verify} messages.
     * @param message LoginResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.ILoginResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a LoginResponse message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LoginResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.LoginResponse;

    /**
     * Decodes a LoginResponse message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LoginResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.LoginResponse;

    /**
     * Verifies a LoginResponse message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a LoginResponse message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LoginResponse
     */
    public static fromObject(object: { [k: string]: any }): Auth.LoginResponse;

    /**
     * Creates a plain object from a LoginResponse message. Also converts values to other types if specified.
     * @param message LoginResponse
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.LoginResponse,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this LoginResponse to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace LoginResponse {
    /** Properties of a Data. */
    interface IData {
      /** Data token */
      token?: string | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: Auth.LoginResponse.IData);

      /** Data token. */
      public token: string;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: Auth.LoginResponse.IData,
      ): Auth.LoginResponse.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link Auth.LoginResponse.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Auth.LoginResponse.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Auth.LoginResponse.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Auth.LoginResponse.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Auth.LoginResponse.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Auth.LoginResponse.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Auth.LoginResponse.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Auth.LoginResponse.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a GetCurrentUserRequest. */
  interface IGetCurrentUserRequest {}

  /** Represents a GetCurrentUserRequest. */
  class GetCurrentUserRequest implements IGetCurrentUserRequest {
    /**
     * Constructs a new GetCurrentUserRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.IGetCurrentUserRequest);

    /**
     * Creates a new GetCurrentUserRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns GetCurrentUserRequest instance
     */
    public static create(
      properties?: Auth.IGetCurrentUserRequest,
    ): Auth.GetCurrentUserRequest;

    /**
     * Encodes the specified GetCurrentUserRequest message. Does not implicitly {@link Auth.GetCurrentUserRequest.verify|verify} messages.
     * @param message GetCurrentUserRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.IGetCurrentUserRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified GetCurrentUserRequest message, length delimited. Does not implicitly {@link Auth.GetCurrentUserRequest.verify|verify} messages.
     * @param message GetCurrentUserRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.IGetCurrentUserRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a GetCurrentUserRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns GetCurrentUserRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.GetCurrentUserRequest;

    /**
     * Decodes a GetCurrentUserRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns GetCurrentUserRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.GetCurrentUserRequest;

    /**
     * Verifies a GetCurrentUserRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a GetCurrentUserRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns GetCurrentUserRequest
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Auth.GetCurrentUserRequest;

    /**
     * Creates a plain object from a GetCurrentUserRequest message. Also converts values to other types if specified.
     * @param message GetCurrentUserRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.GetCurrentUserRequest,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this GetCurrentUserRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a GetCurrentUserResponse. */
  interface IGetCurrentUserResponse {
    /** GetCurrentUserResponse success */
    success?: boolean | null;

    /** GetCurrentUserResponse data */
    data?: Auth.GetCurrentUserResponse.IUserData | null;

    /** GetCurrentUserResponse message */
    message?: string | null;
  }

  /** Represents a GetCurrentUserResponse. */
  class GetCurrentUserResponse implements IGetCurrentUserResponse {
    /**
     * Constructs a new GetCurrentUserResponse.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.IGetCurrentUserResponse);

    /** GetCurrentUserResponse success. */
    public success: boolean;

    /** GetCurrentUserResponse data. */
    public data?: Auth.GetCurrentUserResponse.IUserData | null;

    /** GetCurrentUserResponse message. */
    public message?: string | null;

    /** GetCurrentUserResponse _message. */
    public _message?: 'message';

    /**
     * Creates a new GetCurrentUserResponse instance using the specified properties.
     * @param [properties] Properties to set
     * @returns GetCurrentUserResponse instance
     */
    public static create(
      properties?: Auth.IGetCurrentUserResponse,
    ): Auth.GetCurrentUserResponse;

    /**
     * Encodes the specified GetCurrentUserResponse message. Does not implicitly {@link Auth.GetCurrentUserResponse.verify|verify} messages.
     * @param message GetCurrentUserResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.IGetCurrentUserResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified GetCurrentUserResponse message, length delimited. Does not implicitly {@link Auth.GetCurrentUserResponse.verify|verify} messages.
     * @param message GetCurrentUserResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.IGetCurrentUserResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a GetCurrentUserResponse message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns GetCurrentUserResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.GetCurrentUserResponse;

    /**
     * Decodes a GetCurrentUserResponse message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns GetCurrentUserResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.GetCurrentUserResponse;

    /**
     * Verifies a GetCurrentUserResponse message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a GetCurrentUserResponse message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns GetCurrentUserResponse
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Auth.GetCurrentUserResponse;

    /**
     * Creates a plain object from a GetCurrentUserResponse message. Also converts values to other types if specified.
     * @param message GetCurrentUserResponse
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.GetCurrentUserResponse,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this GetCurrentUserResponse to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace GetCurrentUserResponse {
    /** Properties of a UserData. */
    interface IUserData {
      /** UserData id */
      id?: number | null;

      /** UserData username */
      username?: string | null;
    }

    /** Represents a UserData. */
    class UserData implements IUserData {
      /**
       * Constructs a new UserData.
       * @param [properties] Properties to set
       */
      constructor(properties?: Auth.GetCurrentUserResponse.IUserData);

      /** UserData id. */
      public id: number;

      /** UserData username. */
      public username: string;

      /**
       * Creates a new UserData instance using the specified properties.
       * @param [properties] Properties to set
       * @returns UserData instance
       */
      public static create(
        properties?: Auth.GetCurrentUserResponse.IUserData,
      ): Auth.GetCurrentUserResponse.UserData;

      /**
       * Encodes the specified UserData message. Does not implicitly {@link Auth.GetCurrentUserResponse.UserData.verify|verify} messages.
       * @param message UserData message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Auth.GetCurrentUserResponse.IUserData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified UserData message, length delimited. Does not implicitly {@link Auth.GetCurrentUserResponse.UserData.verify|verify} messages.
       * @param message UserData message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Auth.GetCurrentUserResponse.IUserData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a UserData message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns UserData
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Auth.GetCurrentUserResponse.UserData;

      /**
       * Decodes a UserData message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns UserData
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Auth.GetCurrentUserResponse.UserData;

      /**
       * Verifies a UserData message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a UserData message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns UserData
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Auth.GetCurrentUserResponse.UserData;

      /**
       * Creates a plain object from a UserData message. Also converts values to other types if specified.
       * @param message UserData
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Auth.GetCurrentUserResponse.UserData,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this UserData to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a RefreshTokenRequest. */
  interface IRefreshTokenRequest {}

  /** Represents a RefreshTokenRequest. */
  class RefreshTokenRequest implements IRefreshTokenRequest {
    /**
     * Constructs a new RefreshTokenRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.IRefreshTokenRequest);

    /**
     * Creates a new RefreshTokenRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns RefreshTokenRequest instance
     */
    public static create(
      properties?: Auth.IRefreshTokenRequest,
    ): Auth.RefreshTokenRequest;

    /**
     * Encodes the specified RefreshTokenRequest message. Does not implicitly {@link Auth.RefreshTokenRequest.verify|verify} messages.
     * @param message RefreshTokenRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.IRefreshTokenRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified RefreshTokenRequest message, length delimited. Does not implicitly {@link Auth.RefreshTokenRequest.verify|verify} messages.
     * @param message RefreshTokenRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.IRefreshTokenRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a RefreshTokenRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns RefreshTokenRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.RefreshTokenRequest;

    /**
     * Decodes a RefreshTokenRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns RefreshTokenRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.RefreshTokenRequest;

    /**
     * Verifies a RefreshTokenRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a RefreshTokenRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns RefreshTokenRequest
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Auth.RefreshTokenRequest;

    /**
     * Creates a plain object from a RefreshTokenRequest message. Also converts values to other types if specified.
     * @param message RefreshTokenRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.RefreshTokenRequest,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this RefreshTokenRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a RefreshTokenResponse. */
  interface IRefreshTokenResponse {
    /** RefreshTokenResponse success */
    success?: boolean | null;

    /** RefreshTokenResponse data */
    data?: Auth.RefreshTokenResponse.IData | null;

    /** RefreshTokenResponse message */
    message?: string | null;
  }

  /** Represents a RefreshTokenResponse. */
  class RefreshTokenResponse implements IRefreshTokenResponse {
    /**
     * Constructs a new RefreshTokenResponse.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.IRefreshTokenResponse);

    /** RefreshTokenResponse success. */
    public success: boolean;

    /** RefreshTokenResponse data. */
    public data?: Auth.RefreshTokenResponse.IData | null;

    /** RefreshTokenResponse message. */
    public message?: string | null;

    /** RefreshTokenResponse _message. */
    public _message?: 'message';

    /**
     * Creates a new RefreshTokenResponse instance using the specified properties.
     * @param [properties] Properties to set
     * @returns RefreshTokenResponse instance
     */
    public static create(
      properties?: Auth.IRefreshTokenResponse,
    ): Auth.RefreshTokenResponse;

    /**
     * Encodes the specified RefreshTokenResponse message. Does not implicitly {@link Auth.RefreshTokenResponse.verify|verify} messages.
     * @param message RefreshTokenResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.IRefreshTokenResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified RefreshTokenResponse message, length delimited. Does not implicitly {@link Auth.RefreshTokenResponse.verify|verify} messages.
     * @param message RefreshTokenResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.IRefreshTokenResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a RefreshTokenResponse message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns RefreshTokenResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.RefreshTokenResponse;

    /**
     * Decodes a RefreshTokenResponse message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns RefreshTokenResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.RefreshTokenResponse;

    /**
     * Verifies a RefreshTokenResponse message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a RefreshTokenResponse message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns RefreshTokenResponse
     */
    public static fromObject(object: {
      [k: string]: any;
    }): Auth.RefreshTokenResponse;

    /**
     * Creates a plain object from a RefreshTokenResponse message. Also converts values to other types if specified.
     * @param message RefreshTokenResponse
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.RefreshTokenResponse,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this RefreshTokenResponse to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace RefreshTokenResponse {
    /** Properties of a Data. */
    interface IData {
      /** Data token */
      token?: string | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: Auth.RefreshTokenResponse.IData);

      /** Data token. */
      public token: string;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: Auth.RefreshTokenResponse.IData,
      ): Auth.RefreshTokenResponse.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link Auth.RefreshTokenResponse.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: Auth.RefreshTokenResponse.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Auth.RefreshTokenResponse.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: Auth.RefreshTokenResponse.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): Auth.RefreshTokenResponse.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): Auth.RefreshTokenResponse.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): Auth.RefreshTokenResponse.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: Auth.RefreshTokenResponse.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a LogoutRequest. */
  interface ILogoutRequest {}

  /** Represents a LogoutRequest. */
  class LogoutRequest implements ILogoutRequest {
    /**
     * Constructs a new LogoutRequest.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.ILogoutRequest);

    /**
     * Creates a new LogoutRequest instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LogoutRequest instance
     */
    public static create(properties?: Auth.ILogoutRequest): Auth.LogoutRequest;

    /**
     * Encodes the specified LogoutRequest message. Does not implicitly {@link Auth.LogoutRequest.verify|verify} messages.
     * @param message LogoutRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.ILogoutRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified LogoutRequest message, length delimited. Does not implicitly {@link Auth.LogoutRequest.verify|verify} messages.
     * @param message LogoutRequest message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.ILogoutRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a LogoutRequest message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LogoutRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.LogoutRequest;

    /**
     * Decodes a LogoutRequest message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LogoutRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.LogoutRequest;

    /**
     * Verifies a LogoutRequest message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a LogoutRequest message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LogoutRequest
     */
    public static fromObject(object: { [k: string]: any }): Auth.LogoutRequest;

    /**
     * Creates a plain object from a LogoutRequest message. Also converts values to other types if specified.
     * @param message LogoutRequest
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.LogoutRequest,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this LogoutRequest to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a LogoutResponse. */
  interface ILogoutResponse {
    /** LogoutResponse success */
    success?: string | null;

    /** LogoutResponse message */
    message?: string | null;
  }

  /** Represents a LogoutResponse. */
  class LogoutResponse implements ILogoutResponse {
    /**
     * Constructs a new LogoutResponse.
     * @param [properties] Properties to set
     */
    constructor(properties?: Auth.ILogoutResponse);

    /** LogoutResponse success. */
    public success: string;

    /** LogoutResponse message. */
    public message: string;

    /**
     * Creates a new LogoutResponse instance using the specified properties.
     * @param [properties] Properties to set
     * @returns LogoutResponse instance
     */
    public static create(
      properties?: Auth.ILogoutResponse,
    ): Auth.LogoutResponse;

    /**
     * Encodes the specified LogoutResponse message. Does not implicitly {@link Auth.LogoutResponse.verify|verify} messages.
     * @param message LogoutResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: Auth.ILogoutResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified LogoutResponse message, length delimited. Does not implicitly {@link Auth.LogoutResponse.verify|verify} messages.
     * @param message LogoutResponse message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: Auth.ILogoutResponse,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a LogoutResponse message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns LogoutResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): Auth.LogoutResponse;

    /**
     * Decodes a LogoutResponse message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns LogoutResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): Auth.LogoutResponse;

    /**
     * Verifies a LogoutResponse message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a LogoutResponse message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns LogoutResponse
     */
    public static fromObject(object: { [k: string]: any }): Auth.LogoutResponse;

    /**
     * Creates a plain object from a LogoutResponse message. Also converts values to other types if specified.
     * @param message LogoutResponse
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: Auth.LogoutResponse,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this LogoutResponse to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }
}

/** Namespace DataCore. */
export namespace DataCore {
  /** Represents a Greeter */
  class Greeter extends $protobuf.rpc.Service {
    /**
     * Constructs a new Greeter service.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     */
    constructor(
      rpcImpl: $protobuf.RPCImpl,
      requestDelimited?: boolean,
      responseDelimited?: boolean,
    );

    /**
     * Creates new Greeter service using the specified rpc implementation.
     * @param rpcImpl RPC implementation
     * @param [requestDelimited=false] Whether requests are length-delimited
     * @param [responseDelimited=false] Whether responses are length-delimited
     * @returns RPC service. Useful where requests and/or responses are streamed.
     */
    public static create(
      rpcImpl: $protobuf.RPCImpl,
      requestDelimited?: boolean,
      responseDelimited?: boolean,
    ): Greeter;

    /**
     * Calls users.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseGetAll
     */
    public users(
      request: User.IRequest,
      callback: DataCore.Greeter.usersCallback,
    ): void;

    /**
     * Calls users.
     * @param request Request message or plain object
     * @returns Promise
     */
    public users(request: User.IRequest): Promise<User.ResponseGetAll>;

    /**
     * Calls user.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseDetail
     */
    public user(
      request: User.IRequest,
      callback: DataCore.Greeter.userCallback,
    ): void;

    /**
     * Calls user.
     * @param request Request message or plain object
     * @returns Promise
     */
    public user(request: User.IRequest): Promise<User.ResponseDetail>;

    /**
     * Calls userCreate.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseCreate
     */
    public userCreate(
      request: User.IRequest,
      callback: DataCore.Greeter.userCreateCallback,
    ): void;

    /**
     * Calls userCreate.
     * @param request Request message or plain object
     * @returns Promise
     */
    public userCreate(request: User.IRequest): Promise<User.ResponseCreate>;

    /**
     * Calls userUpdate.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseUpdate
     */
    public userUpdate(
      request: User.IRequest,
      callback: DataCore.Greeter.userUpdateCallback,
    ): void;

    /**
     * Calls userUpdate.
     * @param request Request message or plain object
     * @returns Promise
     */
    public userUpdate(request: User.IRequest): Promise<User.ResponseUpdate>;

    /**
     * Calls userDelete.
     * @param request Request message or plain object
     * @param callback Node-style callback called with the error, if any, and ResponseDelete
     */
    public userDelete(
      request: User.IRequest,
      callback: DataCore.Greeter.userDeleteCallback,
    ): void;

    /**
     * Calls userDelete.
     * @param request Request message or plain object
     * @returns Promise
     */
    public userDelete(request: User.IRequest): Promise<User.ResponseDelete>;

    /**
     * Calls login.
     * @param request LoginRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and LoginResponse
     */
    public login(
      request: Auth.ILoginRequest,
      callback: DataCore.Greeter.loginCallback,
    ): void;

    /**
     * Calls login.
     * @param request LoginRequest message or plain object
     * @returns Promise
     */
    public login(request: Auth.ILoginRequest): Promise<Auth.LoginResponse>;

    /**
     * Calls getCurrentUser.
     * @param request GetCurrentUserRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and GetCurrentUserResponse
     */
    public getCurrentUser(
      request: Auth.IGetCurrentUserRequest,
      callback: DataCore.Greeter.getCurrentUserCallback,
    ): void;

    /**
     * Calls getCurrentUser.
     * @param request GetCurrentUserRequest message or plain object
     * @returns Promise
     */
    public getCurrentUser(
      request: Auth.IGetCurrentUserRequest,
    ): Promise<Auth.GetCurrentUserResponse>;

    /**
     * Calls refreshToken.
     * @param request RefreshTokenRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and RefreshTokenResponse
     */
    public refreshToken(
      request: Auth.IRefreshTokenRequest,
      callback: DataCore.Greeter.refreshTokenCallback,
    ): void;

    /**
     * Calls refreshToken.
     * @param request RefreshTokenRequest message or plain object
     * @returns Promise
     */
    public refreshToken(
      request: Auth.IRefreshTokenRequest,
    ): Promise<Auth.RefreshTokenResponse>;

    /**
     * Calls logout.
     * @param request LogoutRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and LogoutResponse
     */
    public logout(
      request: Auth.ILogoutRequest,
      callback: DataCore.Greeter.logoutCallback,
    ): void;

    /**
     * Calls logout.
     * @param request LogoutRequest message or plain object
     * @returns Promise
     */
    public logout(request: Auth.ILogoutRequest): Promise<Auth.LogoutResponse>;

    /**
     * Calls checkToken.
     * @param request LoginRequest message or plain object
     * @param callback Node-style callback called with the error, if any, and GetCurrentUserResponse
     */
    public checkToken(
      request: Auth.ILoginRequest,
      callback: DataCore.Greeter.checkTokenCallback,
    ): void;

    /**
     * Calls checkToken.
     * @param request LoginRequest message or plain object
     * @returns Promise
     */
    public checkToken(
      request: Auth.ILoginRequest,
    ): Promise<Auth.GetCurrentUserResponse>;
  }

  namespace Greeter {
    /**
     * Callback as used by {@link DataCore.Greeter#users}.
     * @param error Error, if any
     * @param [response] ResponseGetAll
     */
    type usersCallback = (
      error: Error | null,
      response?: User.ResponseGetAll,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#user}.
     * @param error Error, if any
     * @param [response] ResponseDetail
     */
    type userCallback = (
      error: Error | null,
      response?: User.ResponseDetail,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#userCreate}.
     * @param error Error, if any
     * @param [response] ResponseCreate
     */
    type userCreateCallback = (
      error: Error | null,
      response?: User.ResponseCreate,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#userUpdate}.
     * @param error Error, if any
     * @param [response] ResponseUpdate
     */
    type userUpdateCallback = (
      error: Error | null,
      response?: User.ResponseUpdate,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#userDelete}.
     * @param error Error, if any
     * @param [response] ResponseDelete
     */
    type userDeleteCallback = (
      error: Error | null,
      response?: User.ResponseDelete,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#login}.
     * @param error Error, if any
     * @param [response] LoginResponse
     */
    type loginCallback = (
      error: Error | null,
      response?: Auth.LoginResponse,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#getCurrentUser}.
     * @param error Error, if any
     * @param [response] GetCurrentUserResponse
     */
    type getCurrentUserCallback = (
      error: Error | null,
      response?: Auth.GetCurrentUserResponse,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#refreshToken}.
     * @param error Error, if any
     * @param [response] RefreshTokenResponse
     */
    type refreshTokenCallback = (
      error: Error | null,
      response?: Auth.RefreshTokenResponse,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#logout}.
     * @param error Error, if any
     * @param [response] LogoutResponse
     */
    type logoutCallback = (
      error: Error | null,
      response?: Auth.LogoutResponse,
    ) => void;

    /**
     * Callback as used by {@link DataCore.Greeter#checkToken}.
     * @param error Error, if any
     * @param [response] GetCurrentUserResponse
     */
    type checkTokenCallback = (
      error: Error | null,
      response?: Auth.GetCurrentUserResponse,
    ) => void;
  }
}

/** Namespace User. */
export namespace User {
  /** Properties of a Request. */
  interface IRequest {
    /** Request authorization */
    authorization?: string | null;

    /** Request query */
    query?: User.Request.IQuery | null;

    /** Request body */
    body?: User.Request.IBody | null;

    /** Request params */
    params?: User.Request.IParams | null;

    /** Request user */
    user?: User.Request.IUser | null;
  }

  /** Represents a Request. */
  class Request implements IRequest {
    /**
     * Constructs a new Request.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IRequest);

    /** Request authorization. */
    public authorization?: string | null;

    /** Request query. */
    public query?: User.Request.IQuery | null;

    /** Request body. */
    public body?: User.Request.IBody | null;

    /** Request params. */
    public params?: User.Request.IParams | null;

    /** Request user. */
    public user?: User.Request.IUser | null;

    /** Request _authorization. */
    public _authorization?: 'authorization';

    /** Request _query. */
    public _query?: 'query';

    /** Request _body. */
    public _body?: 'body';

    /** Request _params. */
    public _params?: 'params';

    /** Request _user. */
    public _user?: 'user';

    /**
     * Creates a new Request instance using the specified properties.
     * @param [properties] Properties to set
     * @returns Request instance
     */
    public static create(properties?: User.IRequest): User.Request;

    /**
     * Encodes the specified Request message. Does not implicitly {@link User.Request.verify|verify} messages.
     * @param message Request message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified Request message, length delimited. Does not implicitly {@link User.Request.verify|verify} messages.
     * @param message Request message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IRequest,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a Request message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.Request;

    /**
     * Decodes a Request message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.Request;

    /**
     * Verifies a Request message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a Request message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns Request
     */
    public static fromObject(object: { [k: string]: any }): User.Request;

    /**
     * Creates a plain object from a Request message. Also converts values to other types if specified.
     * @param message Request
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.Request,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this Request to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace Request {
    /** Properties of a Query. */
    interface IQuery {
      /** Query pagination */
      pagination?: number | null;

      /** Query limit */
      limit?: number | null;

      /** Query filterBy */
      filterBy?: string | null;

      /** Query search */
      search?: string | null;

      /** Query lowerThan */
      lowerThan?: string | null;

      /** Query greaterThan */
      greaterThan?: string | null;

      /** Query ids */
      ids?: number[] | null;
    }

    /** Represents a Query. */
    class Query implements IQuery {
      /**
       * Constructs a new Query.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.Request.IQuery);

      /** Query pagination. */
      public pagination?: number | null;

      /** Query limit. */
      public limit?: number | null;

      /** Query filterBy. */
      public filterBy?: string | null;

      /** Query search. */
      public search?: string | null;

      /** Query lowerThan. */
      public lowerThan?: string | null;

      /** Query greaterThan. */
      public greaterThan?: string | null;

      /** Query ids. */
      public ids: number[];

      /** Query _pagination. */
      public _pagination?: 'pagination';

      /** Query _limit. */
      public _limit?: 'limit';

      /** Query _filterBy. */
      public _filterBy?: 'filterBy';

      /** Query _search. */
      public _search?: 'search';

      /** Query _lowerThan. */
      public _lowerThan?: 'lowerThan';

      /** Query _greaterThan. */
      public _greaterThan?: 'greaterThan';

      /**
       * Creates a new Query instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Query instance
       */
      public static create(
        properties?: User.Request.IQuery,
      ): User.Request.Query;

      /**
       * Encodes the specified Query message. Does not implicitly {@link User.Request.Query.verify|verify} messages.
       * @param message Query message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.Request.IQuery,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Query message, length delimited. Does not implicitly {@link User.Request.Query.verify|verify} messages.
       * @param message Query message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.Request.IQuery,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Query message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.Request.Query;

      /**
       * Decodes a Query message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.Request.Query;

      /**
       * Verifies a Query message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Query message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Query
       */
      public static fromObject(object: {
        [k: string]: any;
      }): User.Request.Query;

      /**
       * Creates a plain object from a Query message. Also converts values to other types if specified.
       * @param message Query
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.Request.Query,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Query to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Body. */
    interface IBody {
      /** Body username */
      username?: string | null;

      /** Body password */
      password?: string | null;

      /** Body isActive */
      isActive?: boolean | null;

      /** Body additional */
      additional?: google.protobuf.IStruct | null;

      /** Body createdBy */
      createdBy?: number | null;

      /** Body fullName */
      fullName?: string | null;

      /** Body firstName */
      firstName?: string | null;

      /** Body lastName */
      lastName?: string | null;

      /** Body email */
      email?: string | null;

      /** Body phone */
      phone?: string | null;

      /** Body address */
      address?: string | null;

      /** Body postCode */
      postCode?: string | null;

      /** Body photo */
      photo?: string | null;

      /** Body cover */
      cover?: string | null;

      /** Body gender */
      gender?: string | null;

      /** Body religion */
      religion?: string | null;
    }

    /** Represents a Body. */
    class Body implements IBody {
      /**
       * Constructs a new Body.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.Request.IBody);

      /** Body username. */
      public username: string;

      /** Body password. */
      public password: string;

      /** Body isActive. */
      public isActive: boolean;

      /** Body additional. */
      public additional?: google.protobuf.IStruct | null;

      /** Body createdBy. */
      public createdBy: number;

      /** Body fullName. */
      public fullName: string;

      /** Body firstName. */
      public firstName: string;

      /** Body lastName. */
      public lastName: string;

      /** Body email. */
      public email: string;

      /** Body phone. */
      public phone: string;

      /** Body address. */
      public address: string;

      /** Body postCode. */
      public postCode: string;

      /** Body photo. */
      public photo: string;

      /** Body cover. */
      public cover: string;

      /** Body gender. */
      public gender: string;

      /** Body religion. */
      public religion: string;

      /**
       * Creates a new Body instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Body instance
       */
      public static create(properties?: User.Request.IBody): User.Request.Body;

      /**
       * Encodes the specified Body message. Does not implicitly {@link User.Request.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.Request.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link User.Request.Body.verify|verify} messages.
       * @param message Body message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.Request.IBody,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.Request.Body;

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.Request.Body;

      /**
       * Verifies a Body message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Body
       */
      public static fromObject(object: { [k: string]: any }): User.Request.Body;

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @param message Body
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.Request.Body,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Body to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Params. */
    interface IParams {
      /** Params id */
      id?: string | null;

      /** Params ids */
      ids?: number[] | null;

      /** Params flag */
      flag?: string | null;
    }

    /** Represents a Params. */
    class Params implements IParams {
      /**
       * Constructs a new Params.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.Request.IParams);

      /** Params id. */
      public id: string;

      /** Params ids. */
      public ids: number[];

      /** Params flag. */
      public flag?: string | null;

      /** Params _flag. */
      public _flag?: 'flag';

      /**
       * Creates a new Params instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Params instance
       */
      public static create(
        properties?: User.Request.IParams,
      ): User.Request.Params;

      /**
       * Encodes the specified Params message. Does not implicitly {@link User.Request.Params.verify|verify} messages.
       * @param message Params message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.Request.IParams,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Params message, length delimited. Does not implicitly {@link User.Request.Params.verify|verify} messages.
       * @param message Params message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.Request.IParams,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Params message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.Request.Params;

      /**
       * Decodes a Params message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.Request.Params;

      /**
       * Verifies a Params message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Params message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Params
       */
      public static fromObject(object: {
        [k: string]: any;
      }): User.Request.Params;

      /**
       * Creates a plain object from a Params message. Also converts values to other types if specified.
       * @param message Params
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.Request.Params,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Params to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a User. */
    interface IUser {
      /** User id */
      id?: number | null;
    }

    /** Represents a User. */
    class User implements IUser {
      /**
       * Constructs a new User.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.Request.IUser);

      /** User id. */
      public id: number;

      /**
       * Creates a new User instance using the specified properties.
       * @param [properties] Properties to set
       * @returns User instance
       */
      public static create(properties?: User.Request.IUser): User.Request.User;

      /**
       * Encodes the specified User message. Does not implicitly {@link User.Request.User.verify|verify} messages.
       * @param message User message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.Request.IUser,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified User message, length delimited. Does not implicitly {@link User.Request.User.verify|verify} messages.
       * @param message User message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.Request.IUser,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a User message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.Request.User;

      /**
       * Decodes a User message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.Request.User;

      /**
       * Verifies a User message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a User message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns User
       */
      public static fromObject(object: { [k: string]: any }): User.Request.User;

      /**
       * Creates a plain object from a User message. Also converts values to other types if specified.
       * @param message User
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.Request.User,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this User to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a ResponseGetAll. */
  interface IResponseGetAll {
    /** ResponseGetAll data */
    data?: User.ResponseGetAll.IData[] | null;

    /** ResponseGetAll meta */
    meta?: User.ResponseGetAll.IMeta | null;

    /** ResponseGetAll success */
    success?: boolean | null;

    /** ResponseGetAll message */
    message?: string | null;
  }

  /** Represents a ResponseGetAll. */
  class ResponseGetAll implements IResponseGetAll {
    /**
     * Constructs a new ResponseGetAll.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IResponseGetAll);

    /** ResponseGetAll data. */
    public data: User.ResponseGetAll.IData[];

    /** ResponseGetAll meta. */
    public meta?: User.ResponseGetAll.IMeta | null;

    /** ResponseGetAll success. */
    public success: boolean;

    /** ResponseGetAll message. */
    public message: string;

    /**
     * Creates a new ResponseGetAll instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseGetAll instance
     */
    public static create(
      properties?: User.IResponseGetAll,
    ): User.ResponseGetAll;

    /**
     * Encodes the specified ResponseGetAll message. Does not implicitly {@link User.ResponseGetAll.verify|verify} messages.
     * @param message ResponseGetAll message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IResponseGetAll,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseGetAll message, length delimited. Does not implicitly {@link User.ResponseGetAll.verify|verify} messages.
     * @param message ResponseGetAll message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IResponseGetAll,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.ResponseGetAll;

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.ResponseGetAll;

    /**
     * Verifies a ResponseGetAll message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseGetAll message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseGetAll
     */
    public static fromObject(object: { [k: string]: any }): User.ResponseGetAll;

    /**
     * Creates a plain object from a ResponseGetAll message. Also converts values to other types if specified.
     * @param message ResponseGetAll
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.ResponseGetAll,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseGetAll to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace ResponseGetAll {
    /** Properties of a Data. */
    interface IData {
      /** Data id */
      id?: string | null;

      /** Data username */
      username?: string | null;

      /** Data password */
      password?: string | null;

      /** Data isActive */
      isActive?: boolean | null;

      /** Data additional */
      additional?: google.protobuf.IStruct | null;

      /** Data createdBy */
      createdBy?: number | null;

      /** Data fullName */
      fullName?: string | null;

      /** Data firstName */
      firstName?: string | null;

      /** Data lastName */
      lastName?: string | null;

      /** Data email */
      email?: string | null;

      /** Data idNumber */
      idNumber?: string | null;

      /** Data phone */
      phone?: string | null;

      /** Data address */
      address?: string | null;

      /** Data postCode */
      postCode?: string | null;

      /** Data photo */
      photo?: string | null;

      /** Data cover */
      cover?: string | null;

      /** Data gender */
      gender?: string | null;

      /** Data religion */
      religion?: string | null;

      /** Data createdAt */
      createdAt?: string | null;

      /** Data updatedAt */
      updatedAt?: string | null;

      /** Data deletedAt */
      deletedAt?: string | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.ResponseGetAll.IData);

      /** Data id. */
      public id: string;

      /** Data username. */
      public username: string;

      /** Data password. */
      public password: string;

      /** Data isActive. */
      public isActive: boolean;

      /** Data additional. */
      public additional?: google.protobuf.IStruct | null;

      /** Data createdBy. */
      public createdBy: number;

      /** Data fullName. */
      public fullName: string;

      /** Data firstName. */
      public firstName: string;

      /** Data lastName. */
      public lastName: string;

      /** Data email. */
      public email: string;

      /** Data idNumber. */
      public idNumber: string;

      /** Data phone. */
      public phone: string;

      /** Data address. */
      public address: string;

      /** Data postCode. */
      public postCode: string;

      /** Data photo. */
      public photo: string;

      /** Data cover. */
      public cover: string;

      /** Data gender. */
      public gender: string;

      /** Data religion. */
      public religion: string;

      /** Data createdAt. */
      public createdAt: string;

      /** Data updatedAt. */
      public updatedAt: string;

      /** Data deletedAt. */
      public deletedAt: string;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: User.ResponseGetAll.IData,
      ): User.ResponseGetAll.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link User.ResponseGetAll.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.ResponseGetAll.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link User.ResponseGetAll.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.ResponseGetAll.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.ResponseGetAll.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.ResponseGetAll.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): User.ResponseGetAll.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.ResponseGetAll.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Meta. */
    interface IMeta {
      /** Meta pagination */
      pagination?: number | null;

      /** Meta limit */
      limit?: number | null;

      /** Meta totalPage */
      totalPage?: number | null;

      /** Meta count */
      count?: number | null;

      /** Meta lowerThan */
      lowerThan?: string | null;

      /** Meta greaterThan */
      greaterThan?: string | null;
    }

    /** Represents a Meta. */
    class Meta implements IMeta {
      /**
       * Constructs a new Meta.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.ResponseGetAll.IMeta);

      /** Meta pagination. */
      public pagination: number;

      /** Meta limit. */
      public limit: number;

      /** Meta totalPage. */
      public totalPage: number;

      /** Meta count. */
      public count: number;

      /** Meta lowerThan. */
      public lowerThan: string;

      /** Meta greaterThan. */
      public greaterThan: string;

      /**
       * Creates a new Meta instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Meta instance
       */
      public static create(
        properties?: User.ResponseGetAll.IMeta,
      ): User.ResponseGetAll.Meta;

      /**
       * Encodes the specified Meta message. Does not implicitly {@link User.ResponseGetAll.Meta.verify|verify} messages.
       * @param message Meta message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.ResponseGetAll.IMeta,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Meta message, length delimited. Does not implicitly {@link User.ResponseGetAll.Meta.verify|verify} messages.
       * @param message Meta message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.ResponseGetAll.IMeta,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Meta message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.ResponseGetAll.Meta;

      /**
       * Decodes a Meta message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.ResponseGetAll.Meta;

      /**
       * Verifies a Meta message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Meta message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Meta
       */
      public static fromObject(object: {
        [k: string]: any;
      }): User.ResponseGetAll.Meta;

      /**
       * Creates a plain object from a Meta message. Also converts values to other types if specified.
       * @param message Meta
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.ResponseGetAll.Meta,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Meta to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }

  /** Properties of a ResponseDetail. */
  interface IResponseDetail {
    /** ResponseDetail data */
    data?: User.ResponseGetAll.IData | null;

    /** ResponseDetail success */
    success?: boolean | null;

    /** ResponseDetail message */
    message?: string | null;
  }

  /** Represents a ResponseDetail. */
  class ResponseDetail implements IResponseDetail {
    /**
     * Constructs a new ResponseDetail.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IResponseDetail);

    /** ResponseDetail data. */
    public data?: User.ResponseGetAll.IData | null;

    /** ResponseDetail success. */
    public success: boolean;

    /** ResponseDetail message. */
    public message: string;

    /**
     * Creates a new ResponseDetail instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseDetail instance
     */
    public static create(
      properties?: User.IResponseDetail,
    ): User.ResponseDetail;

    /**
     * Encodes the specified ResponseDetail message. Does not implicitly {@link User.ResponseDetail.verify|verify} messages.
     * @param message ResponseDetail message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IResponseDetail,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseDetail message, length delimited. Does not implicitly {@link User.ResponseDetail.verify|verify} messages.
     * @param message ResponseDetail message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IResponseDetail,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.ResponseDetail;

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.ResponseDetail;

    /**
     * Verifies a ResponseDetail message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseDetail message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseDetail
     */
    public static fromObject(object: { [k: string]: any }): User.ResponseDetail;

    /**
     * Creates a plain object from a ResponseDetail message. Also converts values to other types if specified.
     * @param message ResponseDetail
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.ResponseDetail,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseDetail to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseCreate. */
  interface IResponseCreate {
    /** ResponseCreate data */
    data?: User.ResponseGetAll.IData | null;

    /** ResponseCreate message */
    message?: string | null;

    /** ResponseCreate success */
    success?: boolean | null;
  }

  /** Represents a ResponseCreate. */
  class ResponseCreate implements IResponseCreate {
    /**
     * Constructs a new ResponseCreate.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IResponseCreate);

    /** ResponseCreate data. */
    public data?: User.ResponseGetAll.IData | null;

    /** ResponseCreate message. */
    public message: string;

    /** ResponseCreate success. */
    public success: boolean;

    /**
     * Creates a new ResponseCreate instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseCreate instance
     */
    public static create(
      properties?: User.IResponseCreate,
    ): User.ResponseCreate;

    /**
     * Encodes the specified ResponseCreate message. Does not implicitly {@link User.ResponseCreate.verify|verify} messages.
     * @param message ResponseCreate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IResponseCreate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseCreate message, length delimited. Does not implicitly {@link User.ResponseCreate.verify|verify} messages.
     * @param message ResponseCreate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IResponseCreate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.ResponseCreate;

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.ResponseCreate;

    /**
     * Verifies a ResponseCreate message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseCreate message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseCreate
     */
    public static fromObject(object: { [k: string]: any }): User.ResponseCreate;

    /**
     * Creates a plain object from a ResponseCreate message. Also converts values to other types if specified.
     * @param message ResponseCreate
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.ResponseCreate,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseCreate to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseUpdate. */
  interface IResponseUpdate {
    /** ResponseUpdate data */
    data?: User.ResponseGetAll.IData | null;

    /** ResponseUpdate success */
    success?: boolean | null;

    /** ResponseUpdate message */
    message?: string | null;
  }

  /** Represents a ResponseUpdate. */
  class ResponseUpdate implements IResponseUpdate {
    /**
     * Constructs a new ResponseUpdate.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IResponseUpdate);

    /** ResponseUpdate data. */
    public data?: User.ResponseGetAll.IData | null;

    /** ResponseUpdate success. */
    public success: boolean;

    /** ResponseUpdate message. */
    public message: string;

    /**
     * Creates a new ResponseUpdate instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseUpdate instance
     */
    public static create(
      properties?: User.IResponseUpdate,
    ): User.ResponseUpdate;

    /**
     * Encodes the specified ResponseUpdate message. Does not implicitly {@link User.ResponseUpdate.verify|verify} messages.
     * @param message ResponseUpdate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IResponseUpdate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseUpdate message, length delimited. Does not implicitly {@link User.ResponseUpdate.verify|verify} messages.
     * @param message ResponseUpdate message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IResponseUpdate,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.ResponseUpdate;

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.ResponseUpdate;

    /**
     * Verifies a ResponseUpdate message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseUpdate message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseUpdate
     */
    public static fromObject(object: { [k: string]: any }): User.ResponseUpdate;

    /**
     * Creates a plain object from a ResponseUpdate message. Also converts values to other types if specified.
     * @param message ResponseUpdate
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.ResponseUpdate,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseUpdate to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  /** Properties of a ResponseDelete. */
  interface IResponseDelete {
    /** ResponseDelete data */
    data?: User.ResponseDelete.IData | null;

    /** ResponseDelete success */
    success?: boolean | null;

    /** ResponseDelete message */
    message?: string | null;
  }

  /** Represents a ResponseDelete. */
  class ResponseDelete implements IResponseDelete {
    /**
     * Constructs a new ResponseDelete.
     * @param [properties] Properties to set
     */
    constructor(properties?: User.IResponseDelete);

    /** ResponseDelete data. */
    public data?: User.ResponseDelete.IData | null;

    /** ResponseDelete success. */
    public success: boolean;

    /** ResponseDelete message. */
    public message: string;

    /**
     * Creates a new ResponseDelete instance using the specified properties.
     * @param [properties] Properties to set
     * @returns ResponseDelete instance
     */
    public static create(
      properties?: User.IResponseDelete,
    ): User.ResponseDelete;

    /**
     * Encodes the specified ResponseDelete message. Does not implicitly {@link User.ResponseDelete.verify|verify} messages.
     * @param message ResponseDelete message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encode(
      message: User.IResponseDelete,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Encodes the specified ResponseDelete message, length delimited. Does not implicitly {@link User.ResponseDelete.verify|verify} messages.
     * @param message ResponseDelete message or plain object to encode
     * @param [writer] Writer to encode to
     * @returns Writer
     */
    public static encodeDelimited(
      message: User.IResponseDelete,
      writer?: $protobuf.Writer,
    ): $protobuf.Writer;

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer.
     * @param reader Reader or buffer to decode from
     * @param [length] Message length if known beforehand
     * @returns ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decode(
      reader: $protobuf.Reader | Uint8Array,
      length?: number,
    ): User.ResponseDelete;

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer, length delimited.
     * @param reader Reader or buffer to decode from
     * @returns ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    public static decodeDelimited(
      reader: $protobuf.Reader | Uint8Array,
    ): User.ResponseDelete;

    /**
     * Verifies a ResponseDelete message.
     * @param message Plain object to verify
     * @returns `null` if valid, otherwise the reason why it is not
     */
    public static verify(message: { [k: string]: any }): string | null;

    /**
     * Creates a ResponseDelete message from a plain object. Also converts values to their respective internal types.
     * @param object Plain object
     * @returns ResponseDelete
     */
    public static fromObject(object: { [k: string]: any }): User.ResponseDelete;

    /**
     * Creates a plain object from a ResponseDelete message. Also converts values to other types if specified.
     * @param message ResponseDelete
     * @param [options] Conversion options
     * @returns Plain object
     */
    public static toObject(
      message: User.ResponseDelete,
      options?: $protobuf.IConversionOptions,
    ): { [k: string]: any };

    /**
     * Converts this ResponseDelete to JSON.
     * @returns JSON object
     */
    public toJSON(): { [k: string]: any };
  }

  namespace ResponseDelete {
    /** Properties of a Data. */
    interface IData {
      /** Data message */
      message?: string | null;
    }

    /** Represents a Data. */
    class Data implements IData {
      /**
       * Constructs a new Data.
       * @param [properties] Properties to set
       */
      constructor(properties?: User.ResponseDelete.IData);

      /** Data message. */
      public message: string;

      /**
       * Creates a new Data instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Data instance
       */
      public static create(
        properties?: User.ResponseDelete.IData,
      ): User.ResponseDelete.Data;

      /**
       * Encodes the specified Data message. Does not implicitly {@link User.ResponseDelete.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: User.ResponseDelete.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link User.ResponseDelete.Data.verify|verify} messages.
       * @param message Data message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: User.ResponseDelete.IData,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): User.ResponseDelete.Data;

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): User.ResponseDelete.Data;

      /**
       * Verifies a Data message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Data
       */
      public static fromObject(object: {
        [k: string]: any;
      }): User.ResponseDelete.Data;

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @param message Data
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: User.ResponseDelete.Data,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Data to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }
}

/** Namespace google. */
export namespace google {
  /** Namespace protobuf. */
  namespace protobuf {
    /** Properties of an Any. */
    interface IAny {
      /** Any type_url */
      type_url?: string | null;

      /** Any value */
      value?: Uint8Array | null;
    }

    /** Represents an Any. */
    class Any implements IAny {
      /**
       * Constructs a new Any.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IAny);

      /** Any type_url. */
      public type_url: string;

      /** Any value. */
      public value: Uint8Array;

      /**
       * Creates a new Any instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Any instance
       */
      public static create(
        properties?: google.protobuf.IAny,
      ): google.protobuf.Any;

      /**
       * Encodes the specified Any message. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @param message Any message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IAny,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Any message, length delimited. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @param message Any message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IAny,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes an Any message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Any;

      /**
       * Decodes an Any message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Any;

      /**
       * Verifies an Any message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates an Any message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Any
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Any;

      /**
       * Creates a plain object from an Any message. Also converts values to other types if specified.
       * @param message Any
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Any,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Any to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Struct. */
    interface IStruct {
      /** Struct fields */
      fields?: { [k: string]: google.protobuf.IValue } | null;
    }

    /** Represents a Struct. */
    class Struct implements IStruct {
      /**
       * Constructs a new Struct.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IStruct);

      /** Struct fields. */
      public fields: { [k: string]: google.protobuf.IValue };

      /**
       * Creates a new Struct instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Struct instance
       */
      public static create(
        properties?: google.protobuf.IStruct,
      ): google.protobuf.Struct;

      /**
       * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @param message Struct message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IStruct,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @param message Struct message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IStruct,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Struct message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Struct;

      /**
       * Decodes a Struct message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Struct;

      /**
       * Verifies a Struct message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Struct message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Struct
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Struct;

      /**
       * Creates a plain object from a Struct message. Also converts values to other types if specified.
       * @param message Struct
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Struct,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Struct to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** Properties of a Value. */
    interface IValue {
      /** Value nullValue */
      nullValue?: google.protobuf.NullValue | null;

      /** Value numberValue */
      numberValue?: number | null;

      /** Value stringValue */
      stringValue?: string | null;

      /** Value boolValue */
      boolValue?: boolean | null;

      /** Value structValue */
      structValue?: google.protobuf.IStruct | null;

      /** Value listValue */
      listValue?: google.protobuf.IListValue | null;
    }

    /** Represents a Value. */
    class Value implements IValue {
      /**
       * Constructs a new Value.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IValue);

      /** Value nullValue. */
      public nullValue?: google.protobuf.NullValue | null;

      /** Value numberValue. */
      public numberValue?: number | null;

      /** Value stringValue. */
      public stringValue?: string | null;

      /** Value boolValue. */
      public boolValue?: boolean | null;

      /** Value structValue. */
      public structValue?: google.protobuf.IStruct | null;

      /** Value listValue. */
      public listValue?: google.protobuf.IListValue | null;

      /** Value kind. */
      public kind?:
        | 'nullValue'
        | 'numberValue'
        | 'stringValue'
        | 'boolValue'
        | 'structValue'
        | 'listValue';

      /**
       * Creates a new Value instance using the specified properties.
       * @param [properties] Properties to set
       * @returns Value instance
       */
      public static create(
        properties?: google.protobuf.IValue,
      ): google.protobuf.Value;

      /**
       * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @param message Value message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @param message Value message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a Value message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.Value;

      /**
       * Decodes a Value message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.Value;

      /**
       * Verifies a Value message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a Value message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns Value
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.Value;

      /**
       * Creates a plain object from a Value message. Also converts values to other types if specified.
       * @param message Value
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.Value,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this Value to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }

    /** NullValue enum. */
    enum NullValue {
      NULL_VALUE = 0,
    }

    /** Properties of a ListValue. */
    interface IListValue {
      /** ListValue values */
      values?: google.protobuf.IValue[] | null;
    }

    /** Represents a ListValue. */
    class ListValue implements IListValue {
      /**
       * Constructs a new ListValue.
       * @param [properties] Properties to set
       */
      constructor(properties?: google.protobuf.IListValue);

      /** ListValue values. */
      public values: google.protobuf.IValue[];

      /**
       * Creates a new ListValue instance using the specified properties.
       * @param [properties] Properties to set
       * @returns ListValue instance
       */
      public static create(
        properties?: google.protobuf.IListValue,
      ): google.protobuf.ListValue;

      /**
       * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @param message ListValue message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encode(
        message: google.protobuf.IListValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @param message ListValue message or plain object to encode
       * @param [writer] Writer to encode to
       * @returns Writer
       */
      public static encodeDelimited(
        message: google.protobuf.IListValue,
        writer?: $protobuf.Writer,
      ): $protobuf.Writer;

      /**
       * Decodes a ListValue message from the specified reader or buffer.
       * @param reader Reader or buffer to decode from
       * @param [length] Message length if known beforehand
       * @returns ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decode(
        reader: $protobuf.Reader | Uint8Array,
        length?: number,
      ): google.protobuf.ListValue;

      /**
       * Decodes a ListValue message from the specified reader or buffer, length delimited.
       * @param reader Reader or buffer to decode from
       * @returns ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      public static decodeDelimited(
        reader: $protobuf.Reader | Uint8Array,
      ): google.protobuf.ListValue;

      /**
       * Verifies a ListValue message.
       * @param message Plain object to verify
       * @returns `null` if valid, otherwise the reason why it is not
       */
      public static verify(message: { [k: string]: any }): string | null;

      /**
       * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
       * @param object Plain object
       * @returns ListValue
       */
      public static fromObject(object: {
        [k: string]: any;
      }): google.protobuf.ListValue;

      /**
       * Creates a plain object from a ListValue message. Also converts values to other types if specified.
       * @param message ListValue
       * @param [options] Conversion options
       * @returns Plain object
       */
      public static toObject(
        message: google.protobuf.ListValue,
        options?: $protobuf.IConversionOptions,
      ): { [k: string]: any };

      /**
       * Converts this ListValue to JSON.
       * @returns JSON object
       */
      public toJSON(): { [k: string]: any };
    }
  }
}
