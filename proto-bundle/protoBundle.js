/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
'use strict';

var $protobuf = require('protobufjs/minimal');

// Common aliases
var $Reader = $protobuf.Reader,
  $Writer = $protobuf.Writer,
  $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots['default'] || ($protobuf.roots['default'] = {});

$root.Auth = (function () {
  /**
   * Namespace Auth.
   * @exports Auth
   * @namespace
   */
  var Auth = {};

  Auth.LoginRequest = (function () {
    /**
     * Properties of a LoginRequest.
     * @memberof Auth
     * @interface ILoginRequest
     * @property {Auth.LoginRequest.IBody|null} [body] LoginRequest body
     */

    /**
     * Constructs a new LoginRequest.
     * @memberof Auth
     * @classdesc Represents a LoginRequest.
     * @implements ILoginRequest
     * @constructor
     * @param {Auth.ILoginRequest=} [properties] Properties to set
     */
    function LoginRequest(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * LoginRequest body.
     * @member {Auth.LoginRequest.IBody|null|undefined} body
     * @memberof Auth.LoginRequest
     * @instance
     */
    LoginRequest.prototype.body = null;

    /**
     * Creates a new LoginRequest instance using the specified properties.
     * @function create
     * @memberof Auth.LoginRequest
     * @static
     * @param {Auth.ILoginRequest=} [properties] Properties to set
     * @returns {Auth.LoginRequest} LoginRequest instance
     */
    LoginRequest.create = function create(properties) {
      return new LoginRequest(properties);
    };

    /**
     * Encodes the specified LoginRequest message. Does not implicitly {@link Auth.LoginRequest.verify|verify} messages.
     * @function encode
     * @memberof Auth.LoginRequest
     * @static
     * @param {Auth.ILoginRequest} message LoginRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LoginRequest.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.body != null && Object.hasOwnProperty.call(message, 'body'))
        $root.Auth.LoginRequest.Body.encode(
          message.body,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      return writer;
    };

    /**
     * Encodes the specified LoginRequest message, length delimited. Does not implicitly {@link Auth.LoginRequest.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.LoginRequest
     * @static
     * @param {Auth.ILoginRequest} message LoginRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LoginRequest.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a LoginRequest message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.LoginRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.LoginRequest} LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LoginRequest.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.LoginRequest();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.body = $root.Auth.LoginRequest.Body.decode(
              reader,
              reader.uint32(),
            );
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a LoginRequest message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.LoginRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.LoginRequest} LoginRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LoginRequest.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a LoginRequest message.
     * @function verify
     * @memberof Auth.LoginRequest
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    LoginRequest.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.body != null && message.hasOwnProperty('body')) {
        var error = $root.Auth.LoginRequest.Body.verify(message.body);
        if (error) return 'body.' + error;
      }
      return null;
    };

    /**
     * Creates a LoginRequest message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.LoginRequest
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.LoginRequest} LoginRequest
     */
    LoginRequest.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.LoginRequest) return object;
      var message = new $root.Auth.LoginRequest();
      if (object.body != null) {
        if (typeof object.body !== 'object')
          throw TypeError('.Auth.LoginRequest.body: object expected');
        message.body = $root.Auth.LoginRequest.Body.fromObject(object.body);
      }
      return message;
    };

    /**
     * Creates a plain object from a LoginRequest message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.LoginRequest
     * @static
     * @param {Auth.LoginRequest} message LoginRequest
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    LoginRequest.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) object.body = null;
      if (message.body != null && message.hasOwnProperty('body'))
        object.body = $root.Auth.LoginRequest.Body.toObject(
          message.body,
          options,
        );
      return object;
    };

    /**
     * Converts this LoginRequest to JSON.
     * @function toJSON
     * @memberof Auth.LoginRequest
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    LoginRequest.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    LoginRequest.Body = (function () {
      /**
       * Properties of a Body.
       * @memberof Auth.LoginRequest
       * @interface IBody
       * @property {string|null} [username] Body username
       * @property {string|null} [password] Body password
       * @property {string|null} [token] Body token
       */

      /**
       * Constructs a new Body.
       * @memberof Auth.LoginRequest
       * @classdesc Represents a Body.
       * @implements IBody
       * @constructor
       * @param {Auth.LoginRequest.IBody=} [properties] Properties to set
       */
      function Body(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Body username.
       * @member {string} username
       * @memberof Auth.LoginRequest.Body
       * @instance
       */
      Body.prototype.username = '';

      /**
       * Body password.
       * @member {string} password
       * @memberof Auth.LoginRequest.Body
       * @instance
       */
      Body.prototype.password = '';

      /**
       * Body token.
       * @member {string} token
       * @memberof Auth.LoginRequest.Body
       * @instance
       */
      Body.prototype.token = '';

      /**
       * Creates a new Body instance using the specified properties.
       * @function create
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Auth.LoginRequest.IBody=} [properties] Properties to set
       * @returns {Auth.LoginRequest.Body} Body instance
       */
      Body.create = function create(properties) {
        return new Body(properties);
      };

      /**
       * Encodes the specified Body message. Does not implicitly {@link Auth.LoginRequest.Body.verify|verify} messages.
       * @function encode
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Auth.LoginRequest.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.username != null &&
          Object.hasOwnProperty.call(message, 'username')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.username);
        if (
          message.password != null &&
          Object.hasOwnProperty.call(message, 'password')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.password);
        if (
          message.token != null &&
          Object.hasOwnProperty.call(message, 'token')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.token);
        return writer;
      };

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link Auth.LoginRequest.Body.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Auth.LoginRequest.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @function decode
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Auth.LoginRequest.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Auth.LoginRequest.Body();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.username = reader.string();
              break;
            case 2:
              message.password = reader.string();
              break;
            case 3:
              message.token = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Auth.LoginRequest.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Body message.
       * @function verify
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Body.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.username != null && message.hasOwnProperty('username'))
          if (!$util.isString(message.username))
            return 'username: string expected';
        if (message.password != null && message.hasOwnProperty('password'))
          if (!$util.isString(message.password))
            return 'password: string expected';
        if (message.token != null && message.hasOwnProperty('token'))
          if (!$util.isString(message.token)) return 'token: string expected';
        return null;
      };

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Auth.LoginRequest.Body} Body
       */
      Body.fromObject = function fromObject(object) {
        if (object instanceof $root.Auth.LoginRequest.Body) return object;
        var message = new $root.Auth.LoginRequest.Body();
        if (object.username != null) message.username = String(object.username);
        if (object.password != null) message.password = String(object.password);
        if (object.token != null) message.token = String(object.token);
        return message;
      };

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Auth.LoginRequest.Body
       * @static
       * @param {Auth.LoginRequest.Body} message Body
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Body.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.username = '';
          object.password = '';
          object.token = '';
        }
        if (message.username != null && message.hasOwnProperty('username'))
          object.username = message.username;
        if (message.password != null && message.hasOwnProperty('password'))
          object.password = message.password;
        if (message.token != null && message.hasOwnProperty('token'))
          object.token = message.token;
        return object;
      };

      /**
       * Converts this Body to JSON.
       * @function toJSON
       * @memberof Auth.LoginRequest.Body
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Body.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Body;
    })();

    return LoginRequest;
  })();

  Auth.LoginResponse = (function () {
    /**
     * Properties of a LoginResponse.
     * @memberof Auth
     * @interface ILoginResponse
     * @property {boolean|null} [success] LoginResponse success
     * @property {Auth.LoginResponse.IData|null} [data] LoginResponse data
     * @property {string|null} [message] LoginResponse message
     */

    /**
     * Constructs a new LoginResponse.
     * @memberof Auth
     * @classdesc Represents a LoginResponse.
     * @implements ILoginResponse
     * @constructor
     * @param {Auth.ILoginResponse=} [properties] Properties to set
     */
    function LoginResponse(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * LoginResponse success.
     * @member {boolean} success
     * @memberof Auth.LoginResponse
     * @instance
     */
    LoginResponse.prototype.success = false;

    /**
     * LoginResponse data.
     * @member {Auth.LoginResponse.IData|null|undefined} data
     * @memberof Auth.LoginResponse
     * @instance
     */
    LoginResponse.prototype.data = null;

    /**
     * LoginResponse message.
     * @member {string|null|undefined} message
     * @memberof Auth.LoginResponse
     * @instance
     */
    LoginResponse.prototype.message = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * LoginResponse _message.
     * @member {"message"|undefined} _message
     * @memberof Auth.LoginResponse
     * @instance
     */
    Object.defineProperty(LoginResponse.prototype, '_message', {
      get: $util.oneOfGetter(($oneOfFields = ['message'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Creates a new LoginResponse instance using the specified properties.
     * @function create
     * @memberof Auth.LoginResponse
     * @static
     * @param {Auth.ILoginResponse=} [properties] Properties to set
     * @returns {Auth.LoginResponse} LoginResponse instance
     */
    LoginResponse.create = function create(properties) {
      return new LoginResponse(properties);
    };

    /**
     * Encodes the specified LoginResponse message. Does not implicitly {@link Auth.LoginResponse.verify|verify} messages.
     * @function encode
     * @memberof Auth.LoginResponse
     * @static
     * @param {Auth.ILoginResponse} message LoginResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LoginResponse.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 1, wireType 0 =*/ 8).bool(message.success);
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Auth.LoginResponse.Data.encode(
          message.data,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified LoginResponse message, length delimited. Does not implicitly {@link Auth.LoginResponse.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.LoginResponse
     * @static
     * @param {Auth.ILoginResponse} message LoginResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LoginResponse.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a LoginResponse message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.LoginResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.LoginResponse} LoginResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LoginResponse.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.LoginResponse();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.success = reader.bool();
            break;
          case 2:
            message.data = $root.Auth.LoginResponse.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a LoginResponse message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.LoginResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.LoginResponse} LoginResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LoginResponse.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a LoginResponse message.
     * @function verify
     * @memberof Auth.LoginResponse
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    LoginResponse.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      var properties = {};
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Auth.LoginResponse.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.message != null && message.hasOwnProperty('message')) {
        properties._message = 1;
        if (!$util.isString(message.message)) return 'message: string expected';
      }
      return null;
    };

    /**
     * Creates a LoginResponse message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.LoginResponse
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.LoginResponse} LoginResponse
     */
    LoginResponse.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.LoginResponse) return object;
      var message = new $root.Auth.LoginResponse();
      if (object.success != null) message.success = Boolean(object.success);
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Auth.LoginResponse.data: object expected');
        message.data = $root.Auth.LoginResponse.Data.fromObject(object.data);
      }
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a LoginResponse message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.LoginResponse
     * @static
     * @param {Auth.LoginResponse} message LoginResponse
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    LoginResponse.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.success = false;
        object.data = null;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Auth.LoginResponse.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message')) {
        object.message = message.message;
        if (options.oneofs) object._message = 'message';
      }
      return object;
    };

    /**
     * Converts this LoginResponse to JSON.
     * @function toJSON
     * @memberof Auth.LoginResponse
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    LoginResponse.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    LoginResponse.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof Auth.LoginResponse
       * @interface IData
       * @property {string|null} [token] Data token
       */

      /**
       * Constructs a new Data.
       * @memberof Auth.LoginResponse
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {Auth.LoginResponse.IData=} [properties] Properties to set
       */
      function Data(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data token.
       * @member {string} token
       * @memberof Auth.LoginResponse.Data
       * @instance
       */
      Data.prototype.token = '';

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Auth.LoginResponse.IData=} [properties] Properties to set
       * @returns {Auth.LoginResponse.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link Auth.LoginResponse.Data.verify|verify} messages.
       * @function encode
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Auth.LoginResponse.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.token != null &&
          Object.hasOwnProperty.call(message, 'token')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.token);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Auth.LoginResponse.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Auth.LoginResponse.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Auth.LoginResponse.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Auth.LoginResponse.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.token = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Auth.LoginResponse.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.token != null && message.hasOwnProperty('token'))
          if (!$util.isString(message.token)) return 'token: string expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Auth.LoginResponse.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.Auth.LoginResponse.Data) return object;
        var message = new $root.Auth.LoginResponse.Data();
        if (object.token != null) message.token = String(object.token);
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Auth.LoginResponse.Data
       * @static
       * @param {Auth.LoginResponse.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.token = '';
        if (message.token != null && message.hasOwnProperty('token'))
          object.token = message.token;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof Auth.LoginResponse.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Data;
    })();

    return LoginResponse;
  })();

  Auth.GetCurrentUserRequest = (function () {
    /**
     * Properties of a GetCurrentUserRequest.
     * @memberof Auth
     * @interface IGetCurrentUserRequest
     */

    /**
     * Constructs a new GetCurrentUserRequest.
     * @memberof Auth
     * @classdesc Represents a GetCurrentUserRequest.
     * @implements IGetCurrentUserRequest
     * @constructor
     * @param {Auth.IGetCurrentUserRequest=} [properties] Properties to set
     */
    function GetCurrentUserRequest(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * Creates a new GetCurrentUserRequest instance using the specified properties.
     * @function create
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Auth.IGetCurrentUserRequest=} [properties] Properties to set
     * @returns {Auth.GetCurrentUserRequest} GetCurrentUserRequest instance
     */
    GetCurrentUserRequest.create = function create(properties) {
      return new GetCurrentUserRequest(properties);
    };

    /**
     * Encodes the specified GetCurrentUserRequest message. Does not implicitly {@link Auth.GetCurrentUserRequest.verify|verify} messages.
     * @function encode
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Auth.IGetCurrentUserRequest} message GetCurrentUserRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    GetCurrentUserRequest.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      return writer;
    };

    /**
     * Encodes the specified GetCurrentUserRequest message, length delimited. Does not implicitly {@link Auth.GetCurrentUserRequest.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Auth.IGetCurrentUserRequest} message GetCurrentUserRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    GetCurrentUserRequest.encodeDelimited = function encodeDelimited(
      message,
      writer,
    ) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a GetCurrentUserRequest message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.GetCurrentUserRequest} GetCurrentUserRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    GetCurrentUserRequest.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.GetCurrentUserRequest();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a GetCurrentUserRequest message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.GetCurrentUserRequest} GetCurrentUserRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    GetCurrentUserRequest.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a GetCurrentUserRequest message.
     * @function verify
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    GetCurrentUserRequest.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      return null;
    };

    /**
     * Creates a GetCurrentUserRequest message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.GetCurrentUserRequest} GetCurrentUserRequest
     */
    GetCurrentUserRequest.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.GetCurrentUserRequest) return object;
      return new $root.Auth.GetCurrentUserRequest();
    };

    /**
     * Creates a plain object from a GetCurrentUserRequest message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.GetCurrentUserRequest
     * @static
     * @param {Auth.GetCurrentUserRequest} message GetCurrentUserRequest
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    GetCurrentUserRequest.toObject = function toObject() {
      return {};
    };

    /**
     * Converts this GetCurrentUserRequest to JSON.
     * @function toJSON
     * @memberof Auth.GetCurrentUserRequest
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    GetCurrentUserRequest.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return GetCurrentUserRequest;
  })();

  Auth.GetCurrentUserResponse = (function () {
    /**
     * Properties of a GetCurrentUserResponse.
     * @memberof Auth
     * @interface IGetCurrentUserResponse
     * @property {boolean|null} [success] GetCurrentUserResponse success
     * @property {Auth.GetCurrentUserResponse.IUserData|null} [data] GetCurrentUserResponse data
     * @property {string|null} [message] GetCurrentUserResponse message
     */

    /**
     * Constructs a new GetCurrentUserResponse.
     * @memberof Auth
     * @classdesc Represents a GetCurrentUserResponse.
     * @implements IGetCurrentUserResponse
     * @constructor
     * @param {Auth.IGetCurrentUserResponse=} [properties] Properties to set
     */
    function GetCurrentUserResponse(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * GetCurrentUserResponse success.
     * @member {boolean} success
     * @memberof Auth.GetCurrentUserResponse
     * @instance
     */
    GetCurrentUserResponse.prototype.success = false;

    /**
     * GetCurrentUserResponse data.
     * @member {Auth.GetCurrentUserResponse.IUserData|null|undefined} data
     * @memberof Auth.GetCurrentUserResponse
     * @instance
     */
    GetCurrentUserResponse.prototype.data = null;

    /**
     * GetCurrentUserResponse message.
     * @member {string|null|undefined} message
     * @memberof Auth.GetCurrentUserResponse
     * @instance
     */
    GetCurrentUserResponse.prototype.message = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * GetCurrentUserResponse _message.
     * @member {"message"|undefined} _message
     * @memberof Auth.GetCurrentUserResponse
     * @instance
     */
    Object.defineProperty(GetCurrentUserResponse.prototype, '_message', {
      get: $util.oneOfGetter(($oneOfFields = ['message'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Creates a new GetCurrentUserResponse instance using the specified properties.
     * @function create
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Auth.IGetCurrentUserResponse=} [properties] Properties to set
     * @returns {Auth.GetCurrentUserResponse} GetCurrentUserResponse instance
     */
    GetCurrentUserResponse.create = function create(properties) {
      return new GetCurrentUserResponse(properties);
    };

    /**
     * Encodes the specified GetCurrentUserResponse message. Does not implicitly {@link Auth.GetCurrentUserResponse.verify|verify} messages.
     * @function encode
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Auth.IGetCurrentUserResponse} message GetCurrentUserResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    GetCurrentUserResponse.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 1, wireType 0 =*/ 8).bool(message.success);
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Auth.GetCurrentUserResponse.UserData.encode(
          message.data,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified GetCurrentUserResponse message, length delimited. Does not implicitly {@link Auth.GetCurrentUserResponse.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Auth.IGetCurrentUserResponse} message GetCurrentUserResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    GetCurrentUserResponse.encodeDelimited = function encodeDelimited(
      message,
      writer,
    ) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a GetCurrentUserResponse message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.GetCurrentUserResponse} GetCurrentUserResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    GetCurrentUserResponse.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.GetCurrentUserResponse();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.success = reader.bool();
            break;
          case 2:
            message.data = $root.Auth.GetCurrentUserResponse.UserData.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a GetCurrentUserResponse message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.GetCurrentUserResponse} GetCurrentUserResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    GetCurrentUserResponse.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a GetCurrentUserResponse message.
     * @function verify
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    GetCurrentUserResponse.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      var properties = {};
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Auth.GetCurrentUserResponse.UserData.verify(
          message.data,
        );
        if (error) return 'data.' + error;
      }
      if (message.message != null && message.hasOwnProperty('message')) {
        properties._message = 1;
        if (!$util.isString(message.message)) return 'message: string expected';
      }
      return null;
    };

    /**
     * Creates a GetCurrentUserResponse message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.GetCurrentUserResponse} GetCurrentUserResponse
     */
    GetCurrentUserResponse.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.GetCurrentUserResponse) return object;
      var message = new $root.Auth.GetCurrentUserResponse();
      if (object.success != null) message.success = Boolean(object.success);
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Auth.GetCurrentUserResponse.data: object expected');
        message.data = $root.Auth.GetCurrentUserResponse.UserData.fromObject(
          object.data,
        );
      }
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a GetCurrentUserResponse message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.GetCurrentUserResponse
     * @static
     * @param {Auth.GetCurrentUserResponse} message GetCurrentUserResponse
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    GetCurrentUserResponse.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.success = false;
        object.data = null;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Auth.GetCurrentUserResponse.UserData.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message')) {
        object.message = message.message;
        if (options.oneofs) object._message = 'message';
      }
      return object;
    };

    /**
     * Converts this GetCurrentUserResponse to JSON.
     * @function toJSON
     * @memberof Auth.GetCurrentUserResponse
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    GetCurrentUserResponse.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    GetCurrentUserResponse.UserData = (function () {
      /**
       * Properties of a UserData.
       * @memberof Auth.GetCurrentUserResponse
       * @interface IUserData
       * @property {number|null} [id] UserData id
       * @property {string|null} [username] UserData username
       */

      /**
       * Constructs a new UserData.
       * @memberof Auth.GetCurrentUserResponse
       * @classdesc Represents a UserData.
       * @implements IUserData
       * @constructor
       * @param {Auth.GetCurrentUserResponse.IUserData=} [properties] Properties to set
       */
      function UserData(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * UserData id.
       * @member {number} id
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @instance
       */
      UserData.prototype.id = 0;

      /**
       * UserData username.
       * @member {string} username
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @instance
       */
      UserData.prototype.username = '';

      /**
       * Creates a new UserData instance using the specified properties.
       * @function create
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Auth.GetCurrentUserResponse.IUserData=} [properties] Properties to set
       * @returns {Auth.GetCurrentUserResponse.UserData} UserData instance
       */
      UserData.create = function create(properties) {
        return new UserData(properties);
      };

      /**
       * Encodes the specified UserData message. Does not implicitly {@link Auth.GetCurrentUserResponse.UserData.verify|verify} messages.
       * @function encode
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Auth.GetCurrentUserResponse.IUserData} message UserData message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      UserData.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.id);
        if (
          message.username != null &&
          Object.hasOwnProperty.call(message, 'username')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.username);
        return writer;
      };

      /**
       * Encodes the specified UserData message, length delimited. Does not implicitly {@link Auth.GetCurrentUserResponse.UserData.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Auth.GetCurrentUserResponse.IUserData} message UserData message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      UserData.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a UserData message from the specified reader or buffer.
       * @function decode
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Auth.GetCurrentUserResponse.UserData} UserData
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      UserData.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Auth.GetCurrentUserResponse.UserData();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.int32();
              break;
            case 2:
              message.username = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a UserData message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Auth.GetCurrentUserResponse.UserData} UserData
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      UserData.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a UserData message.
       * @function verify
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      UserData.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isInteger(message.id)) return 'id: integer expected';
        if (message.username != null && message.hasOwnProperty('username'))
          if (!$util.isString(message.username))
            return 'username: string expected';
        return null;
      };

      /**
       * Creates a UserData message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Auth.GetCurrentUserResponse.UserData} UserData
       */
      UserData.fromObject = function fromObject(object) {
        if (object instanceof $root.Auth.GetCurrentUserResponse.UserData)
          return object;
        var message = new $root.Auth.GetCurrentUserResponse.UserData();
        if (object.id != null) message.id = object.id | 0;
        if (object.username != null) message.username = String(object.username);
        return message;
      };

      /**
       * Creates a plain object from a UserData message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @static
       * @param {Auth.GetCurrentUserResponse.UserData} message UserData
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      UserData.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.id = 0;
          object.username = '';
        }
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        if (message.username != null && message.hasOwnProperty('username'))
          object.username = message.username;
        return object;
      };

      /**
       * Converts this UserData to JSON.
       * @function toJSON
       * @memberof Auth.GetCurrentUserResponse.UserData
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      UserData.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return UserData;
    })();

    return GetCurrentUserResponse;
  })();

  Auth.RefreshTokenRequest = (function () {
    /**
     * Properties of a RefreshTokenRequest.
     * @memberof Auth
     * @interface IRefreshTokenRequest
     */

    /**
     * Constructs a new RefreshTokenRequest.
     * @memberof Auth
     * @classdesc Represents a RefreshTokenRequest.
     * @implements IRefreshTokenRequest
     * @constructor
     * @param {Auth.IRefreshTokenRequest=} [properties] Properties to set
     */
    function RefreshTokenRequest(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * Creates a new RefreshTokenRequest instance using the specified properties.
     * @function create
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Auth.IRefreshTokenRequest=} [properties] Properties to set
     * @returns {Auth.RefreshTokenRequest} RefreshTokenRequest instance
     */
    RefreshTokenRequest.create = function create(properties) {
      return new RefreshTokenRequest(properties);
    };

    /**
     * Encodes the specified RefreshTokenRequest message. Does not implicitly {@link Auth.RefreshTokenRequest.verify|verify} messages.
     * @function encode
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Auth.IRefreshTokenRequest} message RefreshTokenRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RefreshTokenRequest.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      return writer;
    };

    /**
     * Encodes the specified RefreshTokenRequest message, length delimited. Does not implicitly {@link Auth.RefreshTokenRequest.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Auth.IRefreshTokenRequest} message RefreshTokenRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RefreshTokenRequest.encodeDelimited = function encodeDelimited(
      message,
      writer,
    ) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RefreshTokenRequest message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.RefreshTokenRequest} RefreshTokenRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RefreshTokenRequest.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.RefreshTokenRequest();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a RefreshTokenRequest message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.RefreshTokenRequest} RefreshTokenRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RefreshTokenRequest.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RefreshTokenRequest message.
     * @function verify
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    RefreshTokenRequest.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      return null;
    };

    /**
     * Creates a RefreshTokenRequest message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.RefreshTokenRequest} RefreshTokenRequest
     */
    RefreshTokenRequest.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.RefreshTokenRequest) return object;
      return new $root.Auth.RefreshTokenRequest();
    };

    /**
     * Creates a plain object from a RefreshTokenRequest message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.RefreshTokenRequest
     * @static
     * @param {Auth.RefreshTokenRequest} message RefreshTokenRequest
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RefreshTokenRequest.toObject = function toObject() {
      return {};
    };

    /**
     * Converts this RefreshTokenRequest to JSON.
     * @function toJSON
     * @memberof Auth.RefreshTokenRequest
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    RefreshTokenRequest.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return RefreshTokenRequest;
  })();

  Auth.RefreshTokenResponse = (function () {
    /**
     * Properties of a RefreshTokenResponse.
     * @memberof Auth
     * @interface IRefreshTokenResponse
     * @property {boolean|null} [success] RefreshTokenResponse success
     * @property {Auth.RefreshTokenResponse.IData|null} [data] RefreshTokenResponse data
     * @property {string|null} [message] RefreshTokenResponse message
     */

    /**
     * Constructs a new RefreshTokenResponse.
     * @memberof Auth
     * @classdesc Represents a RefreshTokenResponse.
     * @implements IRefreshTokenResponse
     * @constructor
     * @param {Auth.IRefreshTokenResponse=} [properties] Properties to set
     */
    function RefreshTokenResponse(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * RefreshTokenResponse success.
     * @member {boolean} success
     * @memberof Auth.RefreshTokenResponse
     * @instance
     */
    RefreshTokenResponse.prototype.success = false;

    /**
     * RefreshTokenResponse data.
     * @member {Auth.RefreshTokenResponse.IData|null|undefined} data
     * @memberof Auth.RefreshTokenResponse
     * @instance
     */
    RefreshTokenResponse.prototype.data = null;

    /**
     * RefreshTokenResponse message.
     * @member {string|null|undefined} message
     * @memberof Auth.RefreshTokenResponse
     * @instance
     */
    RefreshTokenResponse.prototype.message = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * RefreshTokenResponse _message.
     * @member {"message"|undefined} _message
     * @memberof Auth.RefreshTokenResponse
     * @instance
     */
    Object.defineProperty(RefreshTokenResponse.prototype, '_message', {
      get: $util.oneOfGetter(($oneOfFields = ['message'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Creates a new RefreshTokenResponse instance using the specified properties.
     * @function create
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Auth.IRefreshTokenResponse=} [properties] Properties to set
     * @returns {Auth.RefreshTokenResponse} RefreshTokenResponse instance
     */
    RefreshTokenResponse.create = function create(properties) {
      return new RefreshTokenResponse(properties);
    };

    /**
     * Encodes the specified RefreshTokenResponse message. Does not implicitly {@link Auth.RefreshTokenResponse.verify|verify} messages.
     * @function encode
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Auth.IRefreshTokenResponse} message RefreshTokenResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RefreshTokenResponse.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 1, wireType 0 =*/ 8).bool(message.success);
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.Auth.RefreshTokenResponse.Data.encode(
          message.data,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified RefreshTokenResponse message, length delimited. Does not implicitly {@link Auth.RefreshTokenResponse.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Auth.IRefreshTokenResponse} message RefreshTokenResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    RefreshTokenResponse.encodeDelimited = function encodeDelimited(
      message,
      writer,
    ) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a RefreshTokenResponse message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.RefreshTokenResponse} RefreshTokenResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RefreshTokenResponse.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.RefreshTokenResponse();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.success = reader.bool();
            break;
          case 2:
            message.data = $root.Auth.RefreshTokenResponse.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a RefreshTokenResponse message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.RefreshTokenResponse} RefreshTokenResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    RefreshTokenResponse.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a RefreshTokenResponse message.
     * @function verify
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    RefreshTokenResponse.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      var properties = {};
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.Auth.RefreshTokenResponse.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.message != null && message.hasOwnProperty('message')) {
        properties._message = 1;
        if (!$util.isString(message.message)) return 'message: string expected';
      }
      return null;
    };

    /**
     * Creates a RefreshTokenResponse message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.RefreshTokenResponse} RefreshTokenResponse
     */
    RefreshTokenResponse.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.RefreshTokenResponse) return object;
      var message = new $root.Auth.RefreshTokenResponse();
      if (object.success != null) message.success = Boolean(object.success);
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.Auth.RefreshTokenResponse.data: object expected');
        message.data = $root.Auth.RefreshTokenResponse.Data.fromObject(
          object.data,
        );
      }
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a RefreshTokenResponse message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.RefreshTokenResponse
     * @static
     * @param {Auth.RefreshTokenResponse} message RefreshTokenResponse
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    RefreshTokenResponse.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.success = false;
        object.data = null;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.Auth.RefreshTokenResponse.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message')) {
        object.message = message.message;
        if (options.oneofs) object._message = 'message';
      }
      return object;
    };

    /**
     * Converts this RefreshTokenResponse to JSON.
     * @function toJSON
     * @memberof Auth.RefreshTokenResponse
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    RefreshTokenResponse.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    RefreshTokenResponse.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof Auth.RefreshTokenResponse
       * @interface IData
       * @property {string|null} [token] Data token
       */

      /**
       * Constructs a new Data.
       * @memberof Auth.RefreshTokenResponse
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {Auth.RefreshTokenResponse.IData=} [properties] Properties to set
       */
      function Data(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data token.
       * @member {string} token
       * @memberof Auth.RefreshTokenResponse.Data
       * @instance
       */
      Data.prototype.token = '';

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Auth.RefreshTokenResponse.IData=} [properties] Properties to set
       * @returns {Auth.RefreshTokenResponse.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link Auth.RefreshTokenResponse.Data.verify|verify} messages.
       * @function encode
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Auth.RefreshTokenResponse.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.token != null &&
          Object.hasOwnProperty.call(message, 'token')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.token);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link Auth.RefreshTokenResponse.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Auth.RefreshTokenResponse.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {Auth.RefreshTokenResponse.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.Auth.RefreshTokenResponse.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.token = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {Auth.RefreshTokenResponse.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.token != null && message.hasOwnProperty('token'))
          if (!$util.isString(message.token)) return 'token: string expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {Auth.RefreshTokenResponse.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.Auth.RefreshTokenResponse.Data)
          return object;
        var message = new $root.Auth.RefreshTokenResponse.Data();
        if (object.token != null) message.token = String(object.token);
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof Auth.RefreshTokenResponse.Data
       * @static
       * @param {Auth.RefreshTokenResponse.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.token = '';
        if (message.token != null && message.hasOwnProperty('token'))
          object.token = message.token;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof Auth.RefreshTokenResponse.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Data;
    })();

    return RefreshTokenResponse;
  })();

  Auth.LogoutRequest = (function () {
    /**
     * Properties of a LogoutRequest.
     * @memberof Auth
     * @interface ILogoutRequest
     */

    /**
     * Constructs a new LogoutRequest.
     * @memberof Auth
     * @classdesc Represents a LogoutRequest.
     * @implements ILogoutRequest
     * @constructor
     * @param {Auth.ILogoutRequest=} [properties] Properties to set
     */
    function LogoutRequest(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * Creates a new LogoutRequest instance using the specified properties.
     * @function create
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Auth.ILogoutRequest=} [properties] Properties to set
     * @returns {Auth.LogoutRequest} LogoutRequest instance
     */
    LogoutRequest.create = function create(properties) {
      return new LogoutRequest(properties);
    };

    /**
     * Encodes the specified LogoutRequest message. Does not implicitly {@link Auth.LogoutRequest.verify|verify} messages.
     * @function encode
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Auth.ILogoutRequest} message LogoutRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LogoutRequest.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      return writer;
    };

    /**
     * Encodes the specified LogoutRequest message, length delimited. Does not implicitly {@link Auth.LogoutRequest.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Auth.ILogoutRequest} message LogoutRequest message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LogoutRequest.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a LogoutRequest message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.LogoutRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.LogoutRequest} LogoutRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LogoutRequest.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.LogoutRequest();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a LogoutRequest message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.LogoutRequest
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.LogoutRequest} LogoutRequest
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LogoutRequest.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a LogoutRequest message.
     * @function verify
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    LogoutRequest.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      return null;
    };

    /**
     * Creates a LogoutRequest message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.LogoutRequest} LogoutRequest
     */
    LogoutRequest.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.LogoutRequest) return object;
      return new $root.Auth.LogoutRequest();
    };

    /**
     * Creates a plain object from a LogoutRequest message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.LogoutRequest
     * @static
     * @param {Auth.LogoutRequest} message LogoutRequest
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    LogoutRequest.toObject = function toObject() {
      return {};
    };

    /**
     * Converts this LogoutRequest to JSON.
     * @function toJSON
     * @memberof Auth.LogoutRequest
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    LogoutRequest.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return LogoutRequest;
  })();

  Auth.LogoutResponse = (function () {
    /**
     * Properties of a LogoutResponse.
     * @memberof Auth
     * @interface ILogoutResponse
     * @property {string|null} [success] LogoutResponse success
     * @property {string|null} [message] LogoutResponse message
     */

    /**
     * Constructs a new LogoutResponse.
     * @memberof Auth
     * @classdesc Represents a LogoutResponse.
     * @implements ILogoutResponse
     * @constructor
     * @param {Auth.ILogoutResponse=} [properties] Properties to set
     */
    function LogoutResponse(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * LogoutResponse success.
     * @member {string} success
     * @memberof Auth.LogoutResponse
     * @instance
     */
    LogoutResponse.prototype.success = '';

    /**
     * LogoutResponse message.
     * @member {string} message
     * @memberof Auth.LogoutResponse
     * @instance
     */
    LogoutResponse.prototype.message = '';

    /**
     * Creates a new LogoutResponse instance using the specified properties.
     * @function create
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Auth.ILogoutResponse=} [properties] Properties to set
     * @returns {Auth.LogoutResponse} LogoutResponse instance
     */
    LogoutResponse.create = function create(properties) {
      return new LogoutResponse(properties);
    };

    /**
     * Encodes the specified LogoutResponse message. Does not implicitly {@link Auth.LogoutResponse.verify|verify} messages.
     * @function encode
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Auth.ILogoutResponse} message LogoutResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LogoutResponse.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.success);
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified LogoutResponse message, length delimited. Does not implicitly {@link Auth.LogoutResponse.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Auth.ILogoutResponse} message LogoutResponse message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    LogoutResponse.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a LogoutResponse message from the specified reader or buffer.
     * @function decode
     * @memberof Auth.LogoutResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Auth.LogoutResponse} LogoutResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LogoutResponse.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.Auth.LogoutResponse();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.success = reader.string();
            break;
          case 2:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a LogoutResponse message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Auth.LogoutResponse
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Auth.LogoutResponse} LogoutResponse
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    LogoutResponse.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a LogoutResponse message.
     * @function verify
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    LogoutResponse.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.success != null && message.hasOwnProperty('success'))
        if (!$util.isString(message.success)) return 'success: string expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a LogoutResponse message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Auth.LogoutResponse} LogoutResponse
     */
    LogoutResponse.fromObject = function fromObject(object) {
      if (object instanceof $root.Auth.LogoutResponse) return object;
      var message = new $root.Auth.LogoutResponse();
      if (object.success != null) message.success = String(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a LogoutResponse message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Auth.LogoutResponse
     * @static
     * @param {Auth.LogoutResponse} message LogoutResponse
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    LogoutResponse.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.success = '';
        object.message = '';
      }
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      return object;
    };

    /**
     * Converts this LogoutResponse to JSON.
     * @function toJSON
     * @memberof Auth.LogoutResponse
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    LogoutResponse.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return LogoutResponse;
  })();

  return Auth;
})();

$root.DataCore = (function () {
  /**
   * Namespace DataCore.
   * @exports DataCore
   * @namespace
   */
  var DataCore = {};

  DataCore.Greeter = (function () {
    /**
     * Constructs a new Greeter service.
     * @memberof DataCore
     * @classdesc Represents a Greeter
     * @extends $protobuf.rpc.Service
     * @constructor
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     */
    function Greeter(rpcImpl, requestDelimited, responseDelimited) {
      $protobuf.rpc.Service.call(
        this,
        rpcImpl,
        requestDelimited,
        responseDelimited,
      );
    }

    (Greeter.prototype = Object.create(
      $protobuf.rpc.Service.prototype,
    )).constructor = Greeter;

    /**
     * Creates new Greeter service using the specified rpc implementation.
     * @function create
     * @memberof DataCore.Greeter
     * @static
     * @param {$protobuf.RPCImpl} rpcImpl RPC implementation
     * @param {boolean} [requestDelimited=false] Whether requests are length-delimited
     * @param {boolean} [responseDelimited=false] Whether responses are length-delimited
     * @returns {Greeter} RPC service. Useful where requests and/or responses are streamed.
     */
    Greeter.create = function create(
      rpcImpl,
      requestDelimited,
      responseDelimited,
    ) {
      return new this(rpcImpl, requestDelimited, responseDelimited);
    };

    /**
     * Callback as used by {@link DataCore.Greeter#users}.
     * @memberof DataCore.Greeter
     * @typedef usersCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {User.ResponseGetAll} [response] ResponseGetAll
     */

    /**
     * Calls users.
     * @function users
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @param {DataCore.Greeter.usersCallback} callback Node-style callback called with the error, if any, and ResponseGetAll
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.users = function users(request, callback) {
        return this.rpcCall(
          users,
          $root.User.Request,
          $root.User.ResponseGetAll,
          request,
          callback,
        );
      }),
      'name',
      { value: 'users' },
    );

    /**
     * Calls users.
     * @function users
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @returns {Promise<User.ResponseGetAll>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#user}.
     * @memberof DataCore.Greeter
     * @typedef userCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {User.ResponseDetail} [response] ResponseDetail
     */

    /**
     * Calls user.
     * @function user
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @param {DataCore.Greeter.userCallback} callback Node-style callback called with the error, if any, and ResponseDetail
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.user = function user(request, callback) {
        return this.rpcCall(
          user,
          $root.User.Request,
          $root.User.ResponseDetail,
          request,
          callback,
        );
      }),
      'name',
      { value: 'user' },
    );

    /**
     * Calls user.
     * @function user
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @returns {Promise<User.ResponseDetail>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#userCreate}.
     * @memberof DataCore.Greeter
     * @typedef userCreateCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {User.ResponseCreate} [response] ResponseCreate
     */

    /**
     * Calls userCreate.
     * @function userCreate
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @param {DataCore.Greeter.userCreateCallback} callback Node-style callback called with the error, if any, and ResponseCreate
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.userCreate = function userCreate(request, callback) {
        return this.rpcCall(
          userCreate,
          $root.User.Request,
          $root.User.ResponseCreate,
          request,
          callback,
        );
      }),
      'name',
      { value: 'userCreate' },
    );

    /**
     * Calls userCreate.
     * @function userCreate
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @returns {Promise<User.ResponseCreate>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#userUpdate}.
     * @memberof DataCore.Greeter
     * @typedef userUpdateCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {User.ResponseUpdate} [response] ResponseUpdate
     */

    /**
     * Calls userUpdate.
     * @function userUpdate
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @param {DataCore.Greeter.userUpdateCallback} callback Node-style callback called with the error, if any, and ResponseUpdate
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.userUpdate = function userUpdate(request, callback) {
        return this.rpcCall(
          userUpdate,
          $root.User.Request,
          $root.User.ResponseUpdate,
          request,
          callback,
        );
      }),
      'name',
      { value: 'userUpdate' },
    );

    /**
     * Calls userUpdate.
     * @function userUpdate
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @returns {Promise<User.ResponseUpdate>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#userDelete}.
     * @memberof DataCore.Greeter
     * @typedef userDeleteCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {User.ResponseDelete} [response] ResponseDelete
     */

    /**
     * Calls userDelete.
     * @function userDelete
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @param {DataCore.Greeter.userDeleteCallback} callback Node-style callback called with the error, if any, and ResponseDelete
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.userDelete = function userDelete(request, callback) {
        return this.rpcCall(
          userDelete,
          $root.User.Request,
          $root.User.ResponseDelete,
          request,
          callback,
        );
      }),
      'name',
      { value: 'userDelete' },
    );

    /**
     * Calls userDelete.
     * @function userDelete
     * @memberof DataCore.Greeter
     * @instance
     * @param {User.IRequest} request Request message or plain object
     * @returns {Promise<User.ResponseDelete>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#login}.
     * @memberof DataCore.Greeter
     * @typedef loginCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Auth.LoginResponse} [response] LoginResponse
     */

    /**
     * Calls login.
     * @function login
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILoginRequest} request LoginRequest message or plain object
     * @param {DataCore.Greeter.loginCallback} callback Node-style callback called with the error, if any, and LoginResponse
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.login = function login(request, callback) {
        return this.rpcCall(
          login,
          $root.Auth.LoginRequest,
          $root.Auth.LoginResponse,
          request,
          callback,
        );
      }),
      'name',
      { value: 'login' },
    );

    /**
     * Calls login.
     * @function login
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILoginRequest} request LoginRequest message or plain object
     * @returns {Promise<Auth.LoginResponse>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#getCurrentUser}.
     * @memberof DataCore.Greeter
     * @typedef getCurrentUserCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Auth.GetCurrentUserResponse} [response] GetCurrentUserResponse
     */

    /**
     * Calls getCurrentUser.
     * @function getCurrentUser
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.IGetCurrentUserRequest} request GetCurrentUserRequest message or plain object
     * @param {DataCore.Greeter.getCurrentUserCallback} callback Node-style callback called with the error, if any, and GetCurrentUserResponse
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.getCurrentUser = function getCurrentUser(
        request,
        callback,
      ) {
        return this.rpcCall(
          getCurrentUser,
          $root.Auth.GetCurrentUserRequest,
          $root.Auth.GetCurrentUserResponse,
          request,
          callback,
        );
      }),
      'name',
      { value: 'getCurrentUser' },
    );

    /**
     * Calls getCurrentUser.
     * @function getCurrentUser
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.IGetCurrentUserRequest} request GetCurrentUserRequest message or plain object
     * @returns {Promise<Auth.GetCurrentUserResponse>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#refreshToken}.
     * @memberof DataCore.Greeter
     * @typedef refreshTokenCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Auth.RefreshTokenResponse} [response] RefreshTokenResponse
     */

    /**
     * Calls refreshToken.
     * @function refreshToken
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.IRefreshTokenRequest} request RefreshTokenRequest message or plain object
     * @param {DataCore.Greeter.refreshTokenCallback} callback Node-style callback called with the error, if any, and RefreshTokenResponse
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.refreshToken = function refreshToken(
        request,
        callback,
      ) {
        return this.rpcCall(
          refreshToken,
          $root.Auth.RefreshTokenRequest,
          $root.Auth.RefreshTokenResponse,
          request,
          callback,
        );
      }),
      'name',
      { value: 'refreshToken' },
    );

    /**
     * Calls refreshToken.
     * @function refreshToken
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.IRefreshTokenRequest} request RefreshTokenRequest message or plain object
     * @returns {Promise<Auth.RefreshTokenResponse>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#logout}.
     * @memberof DataCore.Greeter
     * @typedef logoutCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Auth.LogoutResponse} [response] LogoutResponse
     */

    /**
     * Calls logout.
     * @function logout
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILogoutRequest} request LogoutRequest message or plain object
     * @param {DataCore.Greeter.logoutCallback} callback Node-style callback called with the error, if any, and LogoutResponse
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.logout = function logout(request, callback) {
        return this.rpcCall(
          logout,
          $root.Auth.LogoutRequest,
          $root.Auth.LogoutResponse,
          request,
          callback,
        );
      }),
      'name',
      { value: 'logout' },
    );

    /**
     * Calls logout.
     * @function logout
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILogoutRequest} request LogoutRequest message or plain object
     * @returns {Promise<Auth.LogoutResponse>} Promise
     * @variation 2
     */

    /**
     * Callback as used by {@link DataCore.Greeter#checkToken}.
     * @memberof DataCore.Greeter
     * @typedef checkTokenCallback
     * @type {function}
     * @param {Error|null} error Error, if any
     * @param {Auth.GetCurrentUserResponse} [response] GetCurrentUserResponse
     */

    /**
     * Calls checkToken.
     * @function checkToken
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILoginRequest} request LoginRequest message or plain object
     * @param {DataCore.Greeter.checkTokenCallback} callback Node-style callback called with the error, if any, and GetCurrentUserResponse
     * @returns {undefined}
     * @variation 1
     */
    Object.defineProperty(
      (Greeter.prototype.checkToken = function checkToken(request, callback) {
        return this.rpcCall(
          checkToken,
          $root.Auth.LoginRequest,
          $root.Auth.GetCurrentUserResponse,
          request,
          callback,
        );
      }),
      'name',
      { value: 'checkToken' },
    );

    /**
     * Calls checkToken.
     * @function checkToken
     * @memberof DataCore.Greeter
     * @instance
     * @param {Auth.ILoginRequest} request LoginRequest message or plain object
     * @returns {Promise<Auth.GetCurrentUserResponse>} Promise
     * @variation 2
     */

    return Greeter;
  })();

  return DataCore;
})();

$root.User = (function () {
  /**
   * Namespace User.
   * @exports User
   * @namespace
   */
  var User = {};

  User.Request = (function () {
    /**
     * Properties of a Request.
     * @memberof User
     * @interface IRequest
     * @property {string|null} [authorization] Request authorization
     * @property {User.Request.IQuery|null} [query] Request query
     * @property {User.Request.IBody|null} [body] Request body
     * @property {User.Request.IParams|null} [params] Request params
     * @property {User.Request.IUser|null} [user] Request user
     */

    /**
     * Constructs a new Request.
     * @memberof User
     * @classdesc Represents a Request.
     * @implements IRequest
     * @constructor
     * @param {User.IRequest=} [properties] Properties to set
     */
    function Request(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * Request authorization.
     * @member {string|null|undefined} authorization
     * @memberof User.Request
     * @instance
     */
    Request.prototype.authorization = null;

    /**
     * Request query.
     * @member {User.Request.IQuery|null|undefined} query
     * @memberof User.Request
     * @instance
     */
    Request.prototype.query = null;

    /**
     * Request body.
     * @member {User.Request.IBody|null|undefined} body
     * @memberof User.Request
     * @instance
     */
    Request.prototype.body = null;

    /**
     * Request params.
     * @member {User.Request.IParams|null|undefined} params
     * @memberof User.Request
     * @instance
     */
    Request.prototype.params = null;

    /**
     * Request user.
     * @member {User.Request.IUser|null|undefined} user
     * @memberof User.Request
     * @instance
     */
    Request.prototype.user = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * Request _authorization.
     * @member {"authorization"|undefined} _authorization
     * @memberof User.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_authorization', {
      get: $util.oneOfGetter(($oneOfFields = ['authorization'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _query.
     * @member {"query"|undefined} _query
     * @memberof User.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_query', {
      get: $util.oneOfGetter(($oneOfFields = ['query'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _body.
     * @member {"body"|undefined} _body
     * @memberof User.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_body', {
      get: $util.oneOfGetter(($oneOfFields = ['body'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _params.
     * @member {"params"|undefined} _params
     * @memberof User.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_params', {
      get: $util.oneOfGetter(($oneOfFields = ['params'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Request _user.
     * @member {"user"|undefined} _user
     * @memberof User.Request
     * @instance
     */
    Object.defineProperty(Request.prototype, '_user', {
      get: $util.oneOfGetter(($oneOfFields = ['user'])),
      set: $util.oneOfSetter($oneOfFields),
    });

    /**
     * Creates a new Request instance using the specified properties.
     * @function create
     * @memberof User.Request
     * @static
     * @param {User.IRequest=} [properties] Properties to set
     * @returns {User.Request} Request instance
     */
    Request.create = function create(properties) {
      return new Request(properties);
    };

    /**
     * Encodes the specified Request message. Does not implicitly {@link User.Request.verify|verify} messages.
     * @function encode
     * @memberof User.Request
     * @static
     * @param {User.IRequest} message Request message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Request.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (
        message.authorization != null &&
        Object.hasOwnProperty.call(message, 'authorization')
      )
        writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.authorization);
      if (message.query != null && Object.hasOwnProperty.call(message, 'query'))
        $root.User.Request.Query.encode(
          message.query,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (message.body != null && Object.hasOwnProperty.call(message, 'body'))
        $root.User.Request.Body.encode(
          message.body,
          writer.uint32(/* id 3, wireType 2 =*/ 26).fork(),
        ).ldelim();
      if (
        message.params != null &&
        Object.hasOwnProperty.call(message, 'params')
      )
        $root.User.Request.Params.encode(
          message.params,
          writer.uint32(/* id 4, wireType 2 =*/ 34).fork(),
        ).ldelim();
      if (message.user != null && Object.hasOwnProperty.call(message, 'user'))
        $root.User.Request.User.encode(
          message.user,
          writer.uint32(/* id 5, wireType 2 =*/ 42).fork(),
        ).ldelim();
      return writer;
    };

    /**
     * Encodes the specified Request message, length delimited. Does not implicitly {@link User.Request.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.Request
     * @static
     * @param {User.IRequest} message Request message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Request.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Request message from the specified reader or buffer.
     * @function decode
     * @memberof User.Request
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.Request} Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Request.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.Request();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.authorization = reader.string();
            break;
          case 2:
            message.query = $root.User.Request.Query.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.body = $root.User.Request.Body.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.params = $root.User.Request.Params.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 5:
            message.user = $root.User.Request.User.decode(
              reader,
              reader.uint32(),
            );
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a Request message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.Request
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.Request} Request
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Request.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Request message.
     * @function verify
     * @memberof User.Request
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Request.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      var properties = {};
      if (
        message.authorization != null &&
        message.hasOwnProperty('authorization')
      ) {
        properties._authorization = 1;
        if (!$util.isString(message.authorization))
          return 'authorization: string expected';
      }
      if (message.query != null && message.hasOwnProperty('query')) {
        properties._query = 1;
        {
          var error = $root.User.Request.Query.verify(message.query);
          if (error) return 'query.' + error;
        }
      }
      if (message.body != null && message.hasOwnProperty('body')) {
        properties._body = 1;
        {
          var error = $root.User.Request.Body.verify(message.body);
          if (error) return 'body.' + error;
        }
      }
      if (message.params != null && message.hasOwnProperty('params')) {
        properties._params = 1;
        {
          var error = $root.User.Request.Params.verify(message.params);
          if (error) return 'params.' + error;
        }
      }
      if (message.user != null && message.hasOwnProperty('user')) {
        properties._user = 1;
        {
          var error = $root.User.Request.User.verify(message.user);
          if (error) return 'user.' + error;
        }
      }
      return null;
    };

    /**
     * Creates a Request message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.Request
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.Request} Request
     */
    Request.fromObject = function fromObject(object) {
      if (object instanceof $root.User.Request) return object;
      var message = new $root.User.Request();
      if (object.authorization != null)
        message.authorization = String(object.authorization);
      if (object.query != null) {
        if (typeof object.query !== 'object')
          throw TypeError('.User.Request.query: object expected');
        message.query = $root.User.Request.Query.fromObject(object.query);
      }
      if (object.body != null) {
        if (typeof object.body !== 'object')
          throw TypeError('.User.Request.body: object expected');
        message.body = $root.User.Request.Body.fromObject(object.body);
      }
      if (object.params != null) {
        if (typeof object.params !== 'object')
          throw TypeError('.User.Request.params: object expected');
        message.params = $root.User.Request.Params.fromObject(object.params);
      }
      if (object.user != null) {
        if (typeof object.user !== 'object')
          throw TypeError('.User.Request.user: object expected');
        message.user = $root.User.Request.User.fromObject(object.user);
      }
      return message;
    };

    /**
     * Creates a plain object from a Request message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.Request
     * @static
     * @param {User.Request} message Request
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Request.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (
        message.authorization != null &&
        message.hasOwnProperty('authorization')
      ) {
        object.authorization = message.authorization;
        if (options.oneofs) object._authorization = 'authorization';
      }
      if (message.query != null && message.hasOwnProperty('query')) {
        object.query = $root.User.Request.Query.toObject(
          message.query,
          options,
        );
        if (options.oneofs) object._query = 'query';
      }
      if (message.body != null && message.hasOwnProperty('body')) {
        object.body = $root.User.Request.Body.toObject(message.body, options);
        if (options.oneofs) object._body = 'body';
      }
      if (message.params != null && message.hasOwnProperty('params')) {
        object.params = $root.User.Request.Params.toObject(
          message.params,
          options,
        );
        if (options.oneofs) object._params = 'params';
      }
      if (message.user != null && message.hasOwnProperty('user')) {
        object.user = $root.User.Request.User.toObject(message.user, options);
        if (options.oneofs) object._user = 'user';
      }
      return object;
    };

    /**
     * Converts this Request to JSON.
     * @function toJSON
     * @memberof User.Request
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Request.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    Request.Query = (function () {
      /**
       * Properties of a Query.
       * @memberof User.Request
       * @interface IQuery
       * @property {number|null} [pagination] Query pagination
       * @property {number|null} [limit] Query limit
       * @property {string|null} [filterBy] Query filterBy
       * @property {string|null} [search] Query search
       * @property {string|null} [lowerThan] Query lowerThan
       * @property {string|null} [greaterThan] Query greaterThan
       * @property {Array.<number>|null} [ids] Query ids
       */

      /**
       * Constructs a new Query.
       * @memberof User.Request
       * @classdesc Represents a Query.
       * @implements IQuery
       * @constructor
       * @param {User.Request.IQuery=} [properties] Properties to set
       */
      function Query(properties) {
        this.ids = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Query pagination.
       * @member {number|null|undefined} pagination
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.pagination = null;

      /**
       * Query limit.
       * @member {number|null|undefined} limit
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.limit = null;

      /**
       * Query filterBy.
       * @member {string|null|undefined} filterBy
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.filterBy = null;

      /**
       * Query search.
       * @member {string|null|undefined} search
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.search = null;

      /**
       * Query lowerThan.
       * @member {string|null|undefined} lowerThan
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.lowerThan = null;

      /**
       * Query greaterThan.
       * @member {string|null|undefined} greaterThan
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.greaterThan = null;

      /**
       * Query ids.
       * @member {Array.<number>} ids
       * @memberof User.Request.Query
       * @instance
       */
      Query.prototype.ids = $util.emptyArray;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Query _pagination.
       * @member {"pagination"|undefined} _pagination
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_pagination', {
        get: $util.oneOfGetter(($oneOfFields = ['pagination'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _limit.
       * @member {"limit"|undefined} _limit
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_limit', {
        get: $util.oneOfGetter(($oneOfFields = ['limit'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _filterBy.
       * @member {"filterBy"|undefined} _filterBy
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_filterBy', {
        get: $util.oneOfGetter(($oneOfFields = ['filterBy'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _search.
       * @member {"search"|undefined} _search
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_search', {
        get: $util.oneOfGetter(($oneOfFields = ['search'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _lowerThan.
       * @member {"lowerThan"|undefined} _lowerThan
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_lowerThan', {
        get: $util.oneOfGetter(($oneOfFields = ['lowerThan'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Query _greaterThan.
       * @member {"greaterThan"|undefined} _greaterThan
       * @memberof User.Request.Query
       * @instance
       */
      Object.defineProperty(Query.prototype, '_greaterThan', {
        get: $util.oneOfGetter(($oneOfFields = ['greaterThan'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Query instance using the specified properties.
       * @function create
       * @memberof User.Request.Query
       * @static
       * @param {User.Request.IQuery=} [properties] Properties to set
       * @returns {User.Request.Query} Query instance
       */
      Query.create = function create(properties) {
        return new Query(properties);
      };

      /**
       * Encodes the specified Query message. Does not implicitly {@link User.Request.Query.verify|verify} messages.
       * @function encode
       * @memberof User.Request.Query
       * @static
       * @param {User.Request.IQuery} message Query message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Query.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.pagination != null &&
          Object.hasOwnProperty.call(message, 'pagination')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.pagination);
        if (
          message.limit != null &&
          Object.hasOwnProperty.call(message, 'limit')
        )
          writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.limit);
        if (
          message.filterBy != null &&
          Object.hasOwnProperty.call(message, 'filterBy')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.filterBy);
        if (
          message.search != null &&
          Object.hasOwnProperty.call(message, 'search')
        )
          writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.search);
        if (
          message.lowerThan != null &&
          Object.hasOwnProperty.call(message, 'lowerThan')
        )
          writer.uint32(/* id 5, wireType 2 =*/ 42).string(message.lowerThan);
        if (
          message.greaterThan != null &&
          Object.hasOwnProperty.call(message, 'greaterThan')
        )
          writer.uint32(/* id 6, wireType 2 =*/ 50).string(message.greaterThan);
        if (message.ids != null && message.ids.length) {
          writer.uint32(/* id 7, wireType 2 =*/ 58).fork();
          for (var i = 0; i < message.ids.length; ++i)
            writer.int32(message.ids[i]);
          writer.ldelim();
        }
        return writer;
      };

      /**
       * Encodes the specified Query message, length delimited. Does not implicitly {@link User.Request.Query.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.Request.Query
       * @static
       * @param {User.Request.IQuery} message Query message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Query.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Query message from the specified reader or buffer.
       * @function decode
       * @memberof User.Request.Query
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.Request.Query} Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Query.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.Request.Query();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.pagination = reader.int32();
              break;
            case 2:
              message.limit = reader.int32();
              break;
            case 3:
              message.filterBy = reader.string();
              break;
            case 4:
              message.search = reader.string();
              break;
            case 5:
              message.lowerThan = reader.string();
              break;
            case 6:
              message.greaterThan = reader.string();
              break;
            case 7:
              if (!(message.ids && message.ids.length)) message.ids = [];
              if ((tag & 7) === 2) {
                var end2 = reader.uint32() + reader.pos;
                while (reader.pos < end2) message.ids.push(reader.int32());
              } else message.ids.push(reader.int32());
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Query message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.Request.Query
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.Request.Query} Query
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Query.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Query message.
       * @function verify
       * @memberof User.Request.Query
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Query.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (
          message.pagination != null &&
          message.hasOwnProperty('pagination')
        ) {
          properties._pagination = 1;
          if (!$util.isInteger(message.pagination))
            return 'pagination: integer expected';
        }
        if (message.limit != null && message.hasOwnProperty('limit')) {
          properties._limit = 1;
          if (!$util.isInteger(message.limit)) return 'limit: integer expected';
        }
        if (message.filterBy != null && message.hasOwnProperty('filterBy')) {
          properties._filterBy = 1;
          if (!$util.isString(message.filterBy))
            return 'filterBy: string expected';
        }
        if (message.search != null && message.hasOwnProperty('search')) {
          properties._search = 1;
          if (!$util.isString(message.search)) return 'search: string expected';
        }
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan')) {
          properties._lowerThan = 1;
          if (!$util.isString(message.lowerThan))
            return 'lowerThan: string expected';
        }
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        ) {
          properties._greaterThan = 1;
          if (!$util.isString(message.greaterThan))
            return 'greaterThan: string expected';
        }
        if (message.ids != null && message.hasOwnProperty('ids')) {
          if (!Array.isArray(message.ids)) return 'ids: array expected';
          for (var i = 0; i < message.ids.length; ++i)
            if (!$util.isInteger(message.ids[i]))
              return 'ids: integer[] expected';
        }
        return null;
      };

      /**
       * Creates a Query message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.Request.Query
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.Request.Query} Query
       */
      Query.fromObject = function fromObject(object) {
        if (object instanceof $root.User.Request.Query) return object;
        var message = new $root.User.Request.Query();
        if (object.pagination != null)
          message.pagination = object.pagination | 0;
        if (object.limit != null) message.limit = object.limit | 0;
        if (object.filterBy != null) message.filterBy = String(object.filterBy);
        if (object.search != null) message.search = String(object.search);
        if (object.lowerThan != null)
          message.lowerThan = String(object.lowerThan);
        if (object.greaterThan != null)
          message.greaterThan = String(object.greaterThan);
        if (object.ids) {
          if (!Array.isArray(object.ids))
            throw TypeError('.User.Request.Query.ids: array expected');
          message.ids = [];
          for (var i = 0; i < object.ids.length; ++i)
            message.ids[i] = object.ids[i] | 0;
        }
        return message;
      };

      /**
       * Creates a plain object from a Query message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.Request.Query
       * @static
       * @param {User.Request.Query} message Query
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Query.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.ids = [];
        if (
          message.pagination != null &&
          message.hasOwnProperty('pagination')
        ) {
          object.pagination = message.pagination;
          if (options.oneofs) object._pagination = 'pagination';
        }
        if (message.limit != null && message.hasOwnProperty('limit')) {
          object.limit = message.limit;
          if (options.oneofs) object._limit = 'limit';
        }
        if (message.filterBy != null && message.hasOwnProperty('filterBy')) {
          object.filterBy = message.filterBy;
          if (options.oneofs) object._filterBy = 'filterBy';
        }
        if (message.search != null && message.hasOwnProperty('search')) {
          object.search = message.search;
          if (options.oneofs) object._search = 'search';
        }
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan')) {
          object.lowerThan = message.lowerThan;
          if (options.oneofs) object._lowerThan = 'lowerThan';
        }
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        ) {
          object.greaterThan = message.greaterThan;
          if (options.oneofs) object._greaterThan = 'greaterThan';
        }
        if (message.ids && message.ids.length) {
          object.ids = [];
          for (var j = 0; j < message.ids.length; ++j)
            object.ids[j] = message.ids[j];
        }
        return object;
      };

      /**
       * Converts this Query to JSON.
       * @function toJSON
       * @memberof User.Request.Query
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Query.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Query;
    })();

    Request.Body = (function () {
      /**
       * Properties of a Body.
       * @memberof User.Request
       * @interface IBody
       * @property {string|null} [username] Body username
       * @property {string|null} [password] Body password
       * @property {boolean|null} [isActive] Body isActive
       * @property {google.protobuf.IStruct|null} [additional] Body additional
       * @property {number|null} [createdBy] Body createdBy
       * @property {string|null} [fullName] Body fullName
       * @property {string|null} [firstName] Body firstName
       * @property {string|null} [lastName] Body lastName
       * @property {string|null} [email] Body email
       * @property {string|null} [phone] Body phone
       * @property {string|null} [address] Body address
       * @property {string|null} [postCode] Body postCode
       * @property {string|null} [photo] Body photo
       * @property {string|null} [cover] Body cover
       * @property {string|null} [gender] Body gender
       * @property {string|null} [religion] Body religion
       */

      /**
       * Constructs a new Body.
       * @memberof User.Request
       * @classdesc Represents a Body.
       * @implements IBody
       * @constructor
       * @param {User.Request.IBody=} [properties] Properties to set
       */
      function Body(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Body username.
       * @member {string} username
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.username = 'NULL';

      /**
       * Body password.
       * @member {string} password
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.password = 'NULL';

      /**
       * Body isActive.
       * @member {boolean} isActive
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.isActive = 'NULL';

      /**
       * Body additional.
       * @member {google.protobuf.IStruct|null|undefined} additional
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.additional = 'NULL';

      /**
       * Body createdBy.
       * @member {number} createdBy
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.createdBy = 'NULL';

      /**
       * Body fullName.
       * @member {string} fullName
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.fullName = 'NULL';

      /**
       * Body firstName.
       * @member {string} firstName
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.firstName = 'NULL';

      /**
       * Body lastName.
       * @member {string} lastName
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.lastName = 'NULL';

      /**
       * Body email.
       * @member {string} email
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.email = 'NULL';

      /**
       * Body phone.
       * @member {string} phone
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.phone = 'NULL';

      /**
       * Body address.
       * @member {string} address
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.address = 'NULL';

      /**
       * Body postCode.
       * @member {string} postCode
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.postCode = 'NULL';

      /**
       * Body photo.
       * @member {string} photo
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.photo = 'NULL';

      /**
       * Body cover.
       * @member {string} cover
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.cover = 'NULL';

      /**
       * Body gender.
       * @member {string} gender
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.gender = 'NULL';

      /**
       * Body religion.
       * @member {string} religion
       * @memberof User.Request.Body
       * @instance
       */
      Body.prototype.religion = 'NULL';

      /**
       * Creates a new Body instance using the specified properties.
       * @function create
       * @memberof User.Request.Body
       * @static
       * @param {User.Request.IBody=} [properties] Properties to set
       * @returns {User.Request.Body} Body instance
       */
      Body.create = function create(properties) {
        return new Body(properties);
      };

      /**
       * Encodes the specified Body message. Does not implicitly {@link User.Request.Body.verify|verify} messages.
       * @function encode
       * @memberof User.Request.Body
       * @static
       * @param {User.Request.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.username != null &&
          Object.hasOwnProperty.call(message, 'username')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.username);
        if (
          message.password != null &&
          Object.hasOwnProperty.call(message, 'password')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.password);
        if (
          message.isActive != null &&
          Object.hasOwnProperty.call(message, 'isActive')
        )
          writer.uint32(/* id 8, wireType 0 =*/ 64).bool(message.isActive);
        if (
          message.additional != null &&
          Object.hasOwnProperty.call(message, 'additional')
        )
          $root.google.protobuf.Struct.encode(
            message.additional,
            writer.uint32(/* id 9, wireType 2 =*/ 74).fork(),
          ).ldelim();
        if (
          message.createdBy != null &&
          Object.hasOwnProperty.call(message, 'createdBy')
        )
          writer.uint32(/* id 10, wireType 0 =*/ 80).int32(message.createdBy);
        if (
          message.fullName != null &&
          Object.hasOwnProperty.call(message, 'fullName')
        )
          writer.uint32(/* id 11, wireType 2 =*/ 90).string(message.fullName);
        if (
          message.firstName != null &&
          Object.hasOwnProperty.call(message, 'firstName')
        )
          writer.uint32(/* id 12, wireType 2 =*/ 98).string(message.firstName);
        if (
          message.lastName != null &&
          Object.hasOwnProperty.call(message, 'lastName')
        )
          writer.uint32(/* id 13, wireType 2 =*/ 106).string(message.lastName);
        if (
          message.email != null &&
          Object.hasOwnProperty.call(message, 'email')
        )
          writer.uint32(/* id 14, wireType 2 =*/ 114).string(message.email);
        if (
          message.phone != null &&
          Object.hasOwnProperty.call(message, 'phone')
        )
          writer.uint32(/* id 16, wireType 2 =*/ 130).string(message.phone);
        if (
          message.address != null &&
          Object.hasOwnProperty.call(message, 'address')
        )
          writer.uint32(/* id 17, wireType 2 =*/ 138).string(message.address);
        if (
          message.postCode != null &&
          Object.hasOwnProperty.call(message, 'postCode')
        )
          writer.uint32(/* id 18, wireType 2 =*/ 146).string(message.postCode);
        if (
          message.photo != null &&
          Object.hasOwnProperty.call(message, 'photo')
        )
          writer.uint32(/* id 19, wireType 2 =*/ 154).string(message.photo);
        if (
          message.cover != null &&
          Object.hasOwnProperty.call(message, 'cover')
        )
          writer.uint32(/* id 20, wireType 2 =*/ 162).string(message.cover);
        if (
          message.gender != null &&
          Object.hasOwnProperty.call(message, 'gender')
        )
          writer.uint32(/* id 21, wireType 2 =*/ 170).string(message.gender);
        if (
          message.religion != null &&
          Object.hasOwnProperty.call(message, 'religion')
        )
          writer.uint32(/* id 22, wireType 2 =*/ 178).string(message.religion);
        return writer;
      };

      /**
       * Encodes the specified Body message, length delimited. Does not implicitly {@link User.Request.Body.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.Request.Body
       * @static
       * @param {User.Request.IBody} message Body message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Body.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Body message from the specified reader or buffer.
       * @function decode
       * @memberof User.Request.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.Request.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.Request.Body();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.username = reader.string();
              break;
            case 2:
              message.password = reader.string();
              break;
            case 8:
              message.isActive = reader.bool();
              break;
            case 9:
              message.additional = $root.google.protobuf.Struct.decode(
                reader,
                reader.uint32(),
              );
              break;
            case 10:
              message.createdBy = reader.int32();
              break;
            case 11:
              message.fullName = reader.string();
              break;
            case 12:
              message.firstName = reader.string();
              break;
            case 13:
              message.lastName = reader.string();
              break;
            case 14:
              message.email = reader.string();
              break;
            case 16:
              message.phone = reader.string();
              break;
            case 17:
              message.address = reader.string();
              break;
            case 18:
              message.postCode = reader.string();
              break;
            case 19:
              message.photo = reader.string();
              break;
            case 20:
              message.cover = reader.string();
              break;
            case 21:
              message.gender = reader.string();
              break;
            case 22:
              message.religion = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Body message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.Request.Body
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.Request.Body} Body
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Body.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Body message.
       * @function verify
       * @memberof User.Request.Body
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Body.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.username != null && message.hasOwnProperty('username'))
          if (!$util.isString(message.username))
            return 'username: string expected';
        if (message.password != null && message.hasOwnProperty('password'))
          if (!$util.isString(message.password))
            return 'password: string expected';
        if (message.isActive != null && message.hasOwnProperty('isActive'))
          if (typeof message.isActive !== 'boolean')
            return 'isActive: boolean expected';
        if (
          message.additional != null &&
          message.hasOwnProperty('additional')
        ) {
          var error = $root.google.protobuf.Struct.verify(message.additional);
          if (error) return 'additional.' + error;
        }
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          if (!$util.isInteger(message.createdBy))
            return 'createdBy: integer expected';
        if (message.fullName != null && message.hasOwnProperty('fullName'))
          if (!$util.isString(message.fullName))
            return 'fullName: string expected';
        if (message.firstName != null && message.hasOwnProperty('firstName'))
          if (!$util.isString(message.firstName))
            return 'firstName: string expected';
        if (message.lastName != null && message.hasOwnProperty('lastName'))
          if (!$util.isString(message.lastName))
            return 'lastName: string expected';
        if (message.email != null && message.hasOwnProperty('email'))
          if (!$util.isString(message.email)) return 'email: string expected';
        if (message.phone != null && message.hasOwnProperty('phone'))
          if (!$util.isString(message.phone)) return 'phone: string expected';
        if (message.address != null && message.hasOwnProperty('address'))
          if (!$util.isString(message.address))
            return 'address: string expected';
        if (message.postCode != null && message.hasOwnProperty('postCode'))
          if (!$util.isString(message.postCode))
            return 'postCode: string expected';
        if (message.photo != null && message.hasOwnProperty('photo'))
          if (!$util.isString(message.photo)) return 'photo: string expected';
        if (message.cover != null && message.hasOwnProperty('cover'))
          if (!$util.isString(message.cover)) return 'cover: string expected';
        if (message.gender != null && message.hasOwnProperty('gender'))
          if (!$util.isString(message.gender)) return 'gender: string expected';
        if (message.religion != null && message.hasOwnProperty('religion'))
          if (!$util.isString(message.religion))
            return 'religion: string expected';
        return null;
      };

      /**
       * Creates a Body message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.Request.Body
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.Request.Body} Body
       */
      Body.fromObject = function fromObject(object) {
        if (object instanceof $root.User.Request.Body) return object;
        var message = new $root.User.Request.Body();
        if (object.username != null) message.username = String(object.username);
        if (object.password != null) message.password = String(object.password);
        if (object.isActive != null)
          message.isActive = Boolean(object.isActive);
        if (object.additional != null) {
          if (typeof object.additional !== 'object')
            throw TypeError('.User.Request.Body.additional: object expected');
          message.additional = $root.google.protobuf.Struct.fromObject(
            object.additional,
          );
        }
        if (object.createdBy != null) message.createdBy = object.createdBy | 0;
        if (object.fullName != null) message.fullName = String(object.fullName);
        if (object.firstName != null)
          message.firstName = String(object.firstName);
        if (object.lastName != null) message.lastName = String(object.lastName);
        if (object.email != null) message.email = String(object.email);
        if (object.phone != null) message.phone = String(object.phone);
        if (object.address != null) message.address = String(object.address);
        if (object.postCode != null) message.postCode = String(object.postCode);
        if (object.photo != null) message.photo = String(object.photo);
        if (object.cover != null) message.cover = String(object.cover);
        if (object.gender != null) message.gender = String(object.gender);
        if (object.religion != null) message.religion = String(object.religion);
        return message;
      };

      /**
       * Creates a plain object from a Body message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.Request.Body
       * @static
       * @param {User.Request.Body} message Body
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Body.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.username = 'NULL';
          object.password = 'NULL';
          object.isActive = 'NULL';
          object.additional = 'NULL';
          object.createdBy = 'NULL';
          object.fullName = 'NULL';
          object.firstName = 'NULL';
          object.lastName = 'NULL';
          object.email = 'NULL';
          object.phone = 'NULL';
          object.address = 'NULL';
          object.postCode = 'NULL';
          object.photo = 'NULL';
          object.cover = 'NULL';
          object.gender = 'NULL';
          object.religion = 'NULL';
        }
        if (message.username != null && message.hasOwnProperty('username'))
          object.username = message.username;
        if (message.password != null && message.hasOwnProperty('password'))
          object.password = message.password;
        if (message.isActive != null && message.hasOwnProperty('isActive'))
          object.isActive = message.isActive;
        if (message.additional != null && message.hasOwnProperty('additional'))
          object.additional = $root.google.protobuf.Struct.toObject(
            message.additional,
            options,
          );
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          object.createdBy = message.createdBy;
        if (message.fullName != null && message.hasOwnProperty('fullName'))
          object.fullName = message.fullName;
        if (message.firstName != null && message.hasOwnProperty('firstName'))
          object.firstName = message.firstName;
        if (message.lastName != null && message.hasOwnProperty('lastName'))
          object.lastName = message.lastName;
        if (message.email != null && message.hasOwnProperty('email'))
          object.email = message.email;
        if (message.phone != null && message.hasOwnProperty('phone'))
          object.phone = message.phone;
        if (message.address != null && message.hasOwnProperty('address'))
          object.address = message.address;
        if (message.postCode != null && message.hasOwnProperty('postCode'))
          object.postCode = message.postCode;
        if (message.photo != null && message.hasOwnProperty('photo'))
          object.photo = message.photo;
        if (message.cover != null && message.hasOwnProperty('cover'))
          object.cover = message.cover;
        if (message.gender != null && message.hasOwnProperty('gender'))
          object.gender = message.gender;
        if (message.religion != null && message.hasOwnProperty('religion'))
          object.religion = message.religion;
        return object;
      };

      /**
       * Converts this Body to JSON.
       * @function toJSON
       * @memberof User.Request.Body
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Body.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Body;
    })();

    Request.Params = (function () {
      /**
       * Properties of a Params.
       * @memberof User.Request
       * @interface IParams
       * @property {string|null} [id] Params id
       * @property {Array.<number>|null} [ids] Params ids
       * @property {string|null} [flag] Params flag
       */

      /**
       * Constructs a new Params.
       * @memberof User.Request
       * @classdesc Represents a Params.
       * @implements IParams
       * @constructor
       * @param {User.Request.IParams=} [properties] Properties to set
       */
      function Params(properties) {
        this.ids = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Params id.
       * @member {string} id
       * @memberof User.Request.Params
       * @instance
       */
      Params.prototype.id = '';

      /**
       * Params ids.
       * @member {Array.<number>} ids
       * @memberof User.Request.Params
       * @instance
       */
      Params.prototype.ids = $util.emptyArray;

      /**
       * Params flag.
       * @member {string|null|undefined} flag
       * @memberof User.Request.Params
       * @instance
       */
      Params.prototype.flag = null;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Params _flag.
       * @member {"flag"|undefined} _flag
       * @memberof User.Request.Params
       * @instance
       */
      Object.defineProperty(Params.prototype, '_flag', {
        get: $util.oneOfGetter(($oneOfFields = ['flag'])),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Params instance using the specified properties.
       * @function create
       * @memberof User.Request.Params
       * @static
       * @param {User.Request.IParams=} [properties] Properties to set
       * @returns {User.Request.Params} Params instance
       */
      Params.create = function create(properties) {
        return new Params(properties);
      };

      /**
       * Encodes the specified Params message. Does not implicitly {@link User.Request.Params.verify|verify} messages.
       * @function encode
       * @memberof User.Request.Params
       * @static
       * @param {User.Request.IParams} message Params message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Params.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.id);
        if (message.ids != null && message.ids.length) {
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork();
          for (var i = 0; i < message.ids.length; ++i)
            writer.int32(message.ids[i]);
          writer.ldelim();
        }
        if (message.flag != null && Object.hasOwnProperty.call(message, 'flag'))
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.flag);
        return writer;
      };

      /**
       * Encodes the specified Params message, length delimited. Does not implicitly {@link User.Request.Params.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.Request.Params
       * @static
       * @param {User.Request.IParams} message Params message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Params.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Params message from the specified reader or buffer.
       * @function decode
       * @memberof User.Request.Params
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.Request.Params} Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Params.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.Request.Params();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;
            case 2:
              if (!(message.ids && message.ids.length)) message.ids = [];
              if ((tag & 7) === 2) {
                var end2 = reader.uint32() + reader.pos;
                while (reader.pos < end2) message.ids.push(reader.int32());
              } else message.ids.push(reader.int32());
              break;
            case 3:
              message.flag = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Params message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.Request.Params
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.Request.Params} Params
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Params.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Params message.
       * @function verify
       * @memberof User.Request.Params
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Params.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isString(message.id)) return 'id: string expected';
        if (message.ids != null && message.hasOwnProperty('ids')) {
          if (!Array.isArray(message.ids)) return 'ids: array expected';
          for (var i = 0; i < message.ids.length; ++i)
            if (!$util.isInteger(message.ids[i]))
              return 'ids: integer[] expected';
        }
        if (message.flag != null && message.hasOwnProperty('flag')) {
          properties._flag = 1;
          if (!$util.isString(message.flag)) return 'flag: string expected';
        }
        return null;
      };

      /**
       * Creates a Params message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.Request.Params
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.Request.Params} Params
       */
      Params.fromObject = function fromObject(object) {
        if (object instanceof $root.User.Request.Params) return object;
        var message = new $root.User.Request.Params();
        if (object.id != null) message.id = String(object.id);
        if (object.ids) {
          if (!Array.isArray(object.ids))
            throw TypeError('.User.Request.Params.ids: array expected');
          message.ids = [];
          for (var i = 0; i < object.ids.length; ++i)
            message.ids[i] = object.ids[i] | 0;
        }
        if (object.flag != null) message.flag = String(object.flag);
        return message;
      };

      /**
       * Creates a plain object from a Params message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.Request.Params
       * @static
       * @param {User.Request.Params} message Params
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Params.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.ids = [];
        if (options.defaults) object.id = '';
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        if (message.ids && message.ids.length) {
          object.ids = [];
          for (var j = 0; j < message.ids.length; ++j)
            object.ids[j] = message.ids[j];
        }
        if (message.flag != null && message.hasOwnProperty('flag')) {
          object.flag = message.flag;
          if (options.oneofs) object._flag = 'flag';
        }
        return object;
      };

      /**
       * Converts this Params to JSON.
       * @function toJSON
       * @memberof User.Request.Params
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Params.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Params;
    })();

    Request.User = (function () {
      /**
       * Properties of a User.
       * @memberof User.Request
       * @interface IUser
       * @property {number|null} [id] User id
       */

      /**
       * Constructs a new User.
       * @memberof User.Request
       * @classdesc Represents a User.
       * @implements IUser
       * @constructor
       * @param {User.Request.IUser=} [properties] Properties to set
       */
      function User(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * User id.
       * @member {number} id
       * @memberof User.Request.User
       * @instance
       */
      User.prototype.id = 0;

      /**
       * Creates a new User instance using the specified properties.
       * @function create
       * @memberof User.Request.User
       * @static
       * @param {User.Request.IUser=} [properties] Properties to set
       * @returns {User.Request.User} User instance
       */
      User.create = function create(properties) {
        return new User(properties);
      };

      /**
       * Encodes the specified User message. Does not implicitly {@link User.Request.User.verify|verify} messages.
       * @function encode
       * @memberof User.Request.User
       * @static
       * @param {User.Request.IUser} message User message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      User.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.id);
        return writer;
      };

      /**
       * Encodes the specified User message, length delimited. Does not implicitly {@link User.Request.User.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.Request.User
       * @static
       * @param {User.Request.IUser} message User message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      User.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a User message from the specified reader or buffer.
       * @function decode
       * @memberof User.Request.User
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.Request.User} User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      User.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.Request.User();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.int32();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a User message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.Request.User
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.Request.User} User
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      User.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a User message.
       * @function verify
       * @memberof User.Request.User
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      User.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isInteger(message.id)) return 'id: integer expected';
        return null;
      };

      /**
       * Creates a User message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.Request.User
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.Request.User} User
       */
      User.fromObject = function fromObject(object) {
        if (object instanceof $root.User.Request.User) return object;
        var message = new $root.User.Request.User();
        if (object.id != null) message.id = object.id | 0;
        return message;
      };

      /**
       * Creates a plain object from a User message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.Request.User
       * @static
       * @param {User.Request.User} message User
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      User.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.id = 0;
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        return object;
      };

      /**
       * Converts this User to JSON.
       * @function toJSON
       * @memberof User.Request.User
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      User.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return User;
    })();

    return Request;
  })();

  User.ResponseGetAll = (function () {
    /**
     * Properties of a ResponseGetAll.
     * @memberof User
     * @interface IResponseGetAll
     * @property {Array.<User.ResponseGetAll.IData>|null} [data] ResponseGetAll data
     * @property {User.ResponseGetAll.IMeta|null} [meta] ResponseGetAll meta
     * @property {boolean|null} [success] ResponseGetAll success
     * @property {string|null} [message] ResponseGetAll message
     */

    /**
     * Constructs a new ResponseGetAll.
     * @memberof User
     * @classdesc Represents a ResponseGetAll.
     * @implements IResponseGetAll
     * @constructor
     * @param {User.IResponseGetAll=} [properties] Properties to set
     */
    function ResponseGetAll(properties) {
      this.data = [];
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseGetAll data.
     * @member {Array.<User.ResponseGetAll.IData>} data
     * @memberof User.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.data = $util.emptyArray;

    /**
     * ResponseGetAll meta.
     * @member {User.ResponseGetAll.IMeta|null|undefined} meta
     * @memberof User.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.meta = null;

    /**
     * ResponseGetAll success.
     * @member {boolean} success
     * @memberof User.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.success = false;

    /**
     * ResponseGetAll message.
     * @member {string} message
     * @memberof User.ResponseGetAll
     * @instance
     */
    ResponseGetAll.prototype.message = '';

    /**
     * Creates a new ResponseGetAll instance using the specified properties.
     * @function create
     * @memberof User.ResponseGetAll
     * @static
     * @param {User.IResponseGetAll=} [properties] Properties to set
     * @returns {User.ResponseGetAll} ResponseGetAll instance
     */
    ResponseGetAll.create = function create(properties) {
      return new ResponseGetAll(properties);
    };

    /**
     * Encodes the specified ResponseGetAll message. Does not implicitly {@link User.ResponseGetAll.verify|verify} messages.
     * @function encode
     * @memberof User.ResponseGetAll
     * @static
     * @param {User.IResponseGetAll} message ResponseGetAll message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseGetAll.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && message.data.length)
        for (var i = 0; i < message.data.length; ++i)
          $root.User.ResponseGetAll.Data.encode(
            message.data[i],
            writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
          ).ldelim();
      if (message.meta != null && Object.hasOwnProperty.call(message, 'meta'))
        $root.User.ResponseGetAll.Meta.encode(
          message.meta,
          writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
        ).ldelim();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 3, wireType 0 =*/ 24).bool(message.success);
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 4, wireType 2 =*/ 34).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified ResponseGetAll message, length delimited. Does not implicitly {@link User.ResponseGetAll.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.ResponseGetAll
     * @static
     * @param {User.IResponseGetAll} message ResponseGetAll message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseGetAll.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer.
     * @function decode
     * @memberof User.ResponseGetAll
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.ResponseGetAll} ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseGetAll.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.ResponseGetAll();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            if (!(message.data && message.data.length)) message.data = [];
            message.data.push(
              $root.User.ResponseGetAll.Data.decode(reader, reader.uint32()),
            );
            break;
          case 2:
            message.meta = $root.User.ResponseGetAll.Meta.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 3:
            message.success = reader.bool();
            break;
          case 4:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseGetAll message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.ResponseGetAll
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.ResponseGetAll} ResponseGetAll
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseGetAll.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseGetAll message.
     * @function verify
     * @memberof User.ResponseGetAll
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseGetAll.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        if (!Array.isArray(message.data)) return 'data: array expected';
        for (var i = 0; i < message.data.length; ++i) {
          var error = $root.User.ResponseGetAll.Data.verify(message.data[i]);
          if (error) return 'data.' + error;
        }
      }
      if (message.meta != null && message.hasOwnProperty('meta')) {
        var error = $root.User.ResponseGetAll.Meta.verify(message.meta);
        if (error) return 'meta.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseGetAll message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.ResponseGetAll
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.ResponseGetAll} ResponseGetAll
     */
    ResponseGetAll.fromObject = function fromObject(object) {
      if (object instanceof $root.User.ResponseGetAll) return object;
      var message = new $root.User.ResponseGetAll();
      if (object.data) {
        if (!Array.isArray(object.data))
          throw TypeError('.User.ResponseGetAll.data: array expected');
        message.data = [];
        for (var i = 0; i < object.data.length; ++i) {
          if (typeof object.data[i] !== 'object')
            throw TypeError('.User.ResponseGetAll.data: object expected');
          message.data[i] = $root.User.ResponseGetAll.Data.fromObject(
            object.data[i],
          );
        }
      }
      if (object.meta != null) {
        if (typeof object.meta !== 'object')
          throw TypeError('.User.ResponseGetAll.meta: object expected');
        message.meta = $root.User.ResponseGetAll.Meta.fromObject(object.meta);
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseGetAll message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.ResponseGetAll
     * @static
     * @param {User.ResponseGetAll} message ResponseGetAll
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseGetAll.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.arrays || options.defaults) object.data = [];
      if (options.defaults) {
        object.meta = null;
        object.success = false;
        object.message = '';
      }
      if (message.data && message.data.length) {
        object.data = [];
        for (var j = 0; j < message.data.length; ++j)
          object.data[j] = $root.User.ResponseGetAll.Data.toObject(
            message.data[j],
            options,
          );
      }
      if (message.meta != null && message.hasOwnProperty('meta'))
        object.meta = $root.User.ResponseGetAll.Meta.toObject(
          message.meta,
          options,
        );
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      return object;
    };

    /**
     * Converts this ResponseGetAll to JSON.
     * @function toJSON
     * @memberof User.ResponseGetAll
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseGetAll.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    ResponseGetAll.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof User.ResponseGetAll
       * @interface IData
       * @property {string|null} [id] Data id
       * @property {string|null} [username] Data username
       * @property {string|null} [password] Data password
       * @property {boolean|null} [isActive] Data isActive
       * @property {google.protobuf.IStruct|null} [additional] Data additional
       * @property {number|null} [createdBy] Data createdBy
       * @property {string|null} [fullName] Data fullName
       * @property {string|null} [firstName] Data firstName
       * @property {string|null} [lastName] Data lastName
       * @property {string|null} [email] Data email
       * @property {string|null} [idNumber] Data idNumber
       * @property {string|null} [phone] Data phone
       * @property {string|null} [address] Data address
       * @property {string|null} [postCode] Data postCode
       * @property {string|null} [photo] Data photo
       * @property {string|null} [cover] Data cover
       * @property {string|null} [gender] Data gender
       * @property {string|null} [religion] Data religion
       * @property {string|null} [createdAt] Data createdAt
       * @property {string|null} [updatedAt] Data updatedAt
       * @property {string|null} [deletedAt] Data deletedAt
       */

      /**
       * Constructs a new Data.
       * @memberof User.ResponseGetAll
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {User.ResponseGetAll.IData=} [properties] Properties to set
       */
      function Data(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data id.
       * @member {string} id
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.id = '';

      /**
       * Data username.
       * @member {string} username
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.username = '';

      /**
       * Data password.
       * @member {string} password
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.password = '';

      /**
       * Data isActive.
       * @member {boolean} isActive
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.isActive = false;

      /**
       * Data additional.
       * @member {google.protobuf.IStruct|null|undefined} additional
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.additional = null;

      /**
       * Data createdBy.
       * @member {number} createdBy
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.createdBy = 0;

      /**
       * Data fullName.
       * @member {string} fullName
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.fullName = '';

      /**
       * Data firstName.
       * @member {string} firstName
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.firstName = '';

      /**
       * Data lastName.
       * @member {string} lastName
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.lastName = '';

      /**
       * Data email.
       * @member {string} email
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.email = '';

      /**
       * Data idNumber.
       * @member {string} idNumber
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.idNumber = '';

      /**
       * Data phone.
       * @member {string} phone
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.phone = '';

      /**
       * Data address.
       * @member {string} address
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.address = '';

      /**
       * Data postCode.
       * @member {string} postCode
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.postCode = '';

      /**
       * Data photo.
       * @member {string} photo
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.photo = '';

      /**
       * Data cover.
       * @member {string} cover
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.cover = '';

      /**
       * Data gender.
       * @member {string} gender
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.gender = '';

      /**
       * Data religion.
       * @member {string} religion
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.religion = '';

      /**
       * Data createdAt.
       * @member {string} createdAt
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.createdAt = '';

      /**
       * Data updatedAt.
       * @member {string} updatedAt
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.updatedAt = '';

      /**
       * Data deletedAt.
       * @member {string} deletedAt
       * @memberof User.ResponseGetAll.Data
       * @instance
       */
      Data.prototype.deletedAt = '';

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {User.ResponseGetAll.IData=} [properties] Properties to set
       * @returns {User.ResponseGetAll.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link User.ResponseGetAll.Data.verify|verify} messages.
       * @function encode
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {User.ResponseGetAll.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.id != null && Object.hasOwnProperty.call(message, 'id'))
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.id);
        if (
          message.username != null &&
          Object.hasOwnProperty.call(message, 'username')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.username);
        if (
          message.password != null &&
          Object.hasOwnProperty.call(message, 'password')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.password);
        if (
          message.isActive != null &&
          Object.hasOwnProperty.call(message, 'isActive')
        )
          writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.isActive);
        if (
          message.additional != null &&
          Object.hasOwnProperty.call(message, 'additional')
        )
          $root.google.protobuf.Struct.encode(
            message.additional,
            writer.uint32(/* id 5, wireType 2 =*/ 42).fork(),
          ).ldelim();
        if (
          message.createdBy != null &&
          Object.hasOwnProperty.call(message, 'createdBy')
        )
          writer.uint32(/* id 6, wireType 0 =*/ 48).int32(message.createdBy);
        if (
          message.fullName != null &&
          Object.hasOwnProperty.call(message, 'fullName')
        )
          writer.uint32(/* id 7, wireType 2 =*/ 58).string(message.fullName);
        if (
          message.firstName != null &&
          Object.hasOwnProperty.call(message, 'firstName')
        )
          writer.uint32(/* id 8, wireType 2 =*/ 66).string(message.firstName);
        if (
          message.lastName != null &&
          Object.hasOwnProperty.call(message, 'lastName')
        )
          writer.uint32(/* id 9, wireType 2 =*/ 74).string(message.lastName);
        if (
          message.email != null &&
          Object.hasOwnProperty.call(message, 'email')
        )
          writer.uint32(/* id 10, wireType 2 =*/ 82).string(message.email);
        if (
          message.idNumber != null &&
          Object.hasOwnProperty.call(message, 'idNumber')
        )
          writer.uint32(/* id 11, wireType 2 =*/ 90).string(message.idNumber);
        if (
          message.phone != null &&
          Object.hasOwnProperty.call(message, 'phone')
        )
          writer.uint32(/* id 12, wireType 2 =*/ 98).string(message.phone);
        if (
          message.address != null &&
          Object.hasOwnProperty.call(message, 'address')
        )
          writer.uint32(/* id 13, wireType 2 =*/ 106).string(message.address);
        if (
          message.postCode != null &&
          Object.hasOwnProperty.call(message, 'postCode')
        )
          writer.uint32(/* id 14, wireType 2 =*/ 114).string(message.postCode);
        if (
          message.photo != null &&
          Object.hasOwnProperty.call(message, 'photo')
        )
          writer.uint32(/* id 15, wireType 2 =*/ 122).string(message.photo);
        if (
          message.cover != null &&
          Object.hasOwnProperty.call(message, 'cover')
        )
          writer.uint32(/* id 16, wireType 2 =*/ 130).string(message.cover);
        if (
          message.gender != null &&
          Object.hasOwnProperty.call(message, 'gender')
        )
          writer.uint32(/* id 17, wireType 2 =*/ 138).string(message.gender);
        if (
          message.religion != null &&
          Object.hasOwnProperty.call(message, 'religion')
        )
          writer.uint32(/* id 18, wireType 2 =*/ 146).string(message.religion);
        if (
          message.createdAt != null &&
          Object.hasOwnProperty.call(message, 'createdAt')
        )
          writer.uint32(/* id 19, wireType 2 =*/ 154).string(message.createdAt);
        if (
          message.updatedAt != null &&
          Object.hasOwnProperty.call(message, 'updatedAt')
        )
          writer.uint32(/* id 20, wireType 2 =*/ 162).string(message.updatedAt);
        if (
          message.deletedAt != null &&
          Object.hasOwnProperty.call(message, 'deletedAt')
        )
          writer.uint32(/* id 21, wireType 2 =*/ 170).string(message.deletedAt);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link User.ResponseGetAll.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {User.ResponseGetAll.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.ResponseGetAll.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.ResponseGetAll.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.id = reader.string();
              break;
            case 2:
              message.username = reader.string();
              break;
            case 3:
              message.password = reader.string();
              break;
            case 4:
              message.isActive = reader.bool();
              break;
            case 5:
              message.additional = $root.google.protobuf.Struct.decode(
                reader,
                reader.uint32(),
              );
              break;
            case 6:
              message.createdBy = reader.int32();
              break;
            case 7:
              message.fullName = reader.string();
              break;
            case 8:
              message.firstName = reader.string();
              break;
            case 9:
              message.lastName = reader.string();
              break;
            case 10:
              message.email = reader.string();
              break;
            case 11:
              message.idNumber = reader.string();
              break;
            case 12:
              message.phone = reader.string();
              break;
            case 13:
              message.address = reader.string();
              break;
            case 14:
              message.postCode = reader.string();
              break;
            case 15:
              message.photo = reader.string();
              break;
            case 16:
              message.cover = reader.string();
              break;
            case 17:
              message.gender = reader.string();
              break;
            case 18:
              message.religion = reader.string();
              break;
            case 19:
              message.createdAt = reader.string();
              break;
            case 20:
              message.updatedAt = reader.string();
              break;
            case 21:
              message.deletedAt = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.ResponseGetAll.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.id != null && message.hasOwnProperty('id'))
          if (!$util.isString(message.id)) return 'id: string expected';
        if (message.username != null && message.hasOwnProperty('username'))
          if (!$util.isString(message.username))
            return 'username: string expected';
        if (message.password != null && message.hasOwnProperty('password'))
          if (!$util.isString(message.password))
            return 'password: string expected';
        if (message.isActive != null && message.hasOwnProperty('isActive'))
          if (typeof message.isActive !== 'boolean')
            return 'isActive: boolean expected';
        if (
          message.additional != null &&
          message.hasOwnProperty('additional')
        ) {
          var error = $root.google.protobuf.Struct.verify(message.additional);
          if (error) return 'additional.' + error;
        }
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          if (!$util.isInteger(message.createdBy))
            return 'createdBy: integer expected';
        if (message.fullName != null && message.hasOwnProperty('fullName'))
          if (!$util.isString(message.fullName))
            return 'fullName: string expected';
        if (message.firstName != null && message.hasOwnProperty('firstName'))
          if (!$util.isString(message.firstName))
            return 'firstName: string expected';
        if (message.lastName != null && message.hasOwnProperty('lastName'))
          if (!$util.isString(message.lastName))
            return 'lastName: string expected';
        if (message.email != null && message.hasOwnProperty('email'))
          if (!$util.isString(message.email)) return 'email: string expected';
        if (message.idNumber != null && message.hasOwnProperty('idNumber'))
          if (!$util.isString(message.idNumber))
            return 'idNumber: string expected';
        if (message.phone != null && message.hasOwnProperty('phone'))
          if (!$util.isString(message.phone)) return 'phone: string expected';
        if (message.address != null && message.hasOwnProperty('address'))
          if (!$util.isString(message.address))
            return 'address: string expected';
        if (message.postCode != null && message.hasOwnProperty('postCode'))
          if (!$util.isString(message.postCode))
            return 'postCode: string expected';
        if (message.photo != null && message.hasOwnProperty('photo'))
          if (!$util.isString(message.photo)) return 'photo: string expected';
        if (message.cover != null && message.hasOwnProperty('cover'))
          if (!$util.isString(message.cover)) return 'cover: string expected';
        if (message.gender != null && message.hasOwnProperty('gender'))
          if (!$util.isString(message.gender)) return 'gender: string expected';
        if (message.religion != null && message.hasOwnProperty('religion'))
          if (!$util.isString(message.religion))
            return 'religion: string expected';
        if (message.createdAt != null && message.hasOwnProperty('createdAt'))
          if (!$util.isString(message.createdAt))
            return 'createdAt: string expected';
        if (message.updatedAt != null && message.hasOwnProperty('updatedAt'))
          if (!$util.isString(message.updatedAt))
            return 'updatedAt: string expected';
        if (message.deletedAt != null && message.hasOwnProperty('deletedAt'))
          if (!$util.isString(message.deletedAt))
            return 'deletedAt: string expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.ResponseGetAll.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.User.ResponseGetAll.Data) return object;
        var message = new $root.User.ResponseGetAll.Data();
        if (object.id != null) message.id = String(object.id);
        if (object.username != null) message.username = String(object.username);
        if (object.password != null) message.password = String(object.password);
        if (object.isActive != null)
          message.isActive = Boolean(object.isActive);
        if (object.additional != null) {
          if (typeof object.additional !== 'object')
            throw TypeError(
              '.User.ResponseGetAll.Data.additional: object expected',
            );
          message.additional = $root.google.protobuf.Struct.fromObject(
            object.additional,
          );
        }
        if (object.createdBy != null) message.createdBy = object.createdBy | 0;
        if (object.fullName != null) message.fullName = String(object.fullName);
        if (object.firstName != null)
          message.firstName = String(object.firstName);
        if (object.lastName != null) message.lastName = String(object.lastName);
        if (object.email != null) message.email = String(object.email);
        if (object.idNumber != null) message.idNumber = String(object.idNumber);
        if (object.phone != null) message.phone = String(object.phone);
        if (object.address != null) message.address = String(object.address);
        if (object.postCode != null) message.postCode = String(object.postCode);
        if (object.photo != null) message.photo = String(object.photo);
        if (object.cover != null) message.cover = String(object.cover);
        if (object.gender != null) message.gender = String(object.gender);
        if (object.religion != null) message.religion = String(object.religion);
        if (object.createdAt != null)
          message.createdAt = String(object.createdAt);
        if (object.updatedAt != null)
          message.updatedAt = String(object.updatedAt);
        if (object.deletedAt != null)
          message.deletedAt = String(object.deletedAt);
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.ResponseGetAll.Data
       * @static
       * @param {User.ResponseGetAll.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.id = '';
          object.username = '';
          object.password = '';
          object.isActive = false;
          object.additional = null;
          object.createdBy = 0;
          object.fullName = '';
          object.firstName = '';
          object.lastName = '';
          object.email = '';
          object.idNumber = '';
          object.phone = '';
          object.address = '';
          object.postCode = '';
          object.photo = '';
          object.cover = '';
          object.gender = '';
          object.religion = '';
          object.createdAt = '';
          object.updatedAt = '';
          object.deletedAt = '';
        }
        if (message.id != null && message.hasOwnProperty('id'))
          object.id = message.id;
        if (message.username != null && message.hasOwnProperty('username'))
          object.username = message.username;
        if (message.password != null && message.hasOwnProperty('password'))
          object.password = message.password;
        if (message.isActive != null && message.hasOwnProperty('isActive'))
          object.isActive = message.isActive;
        if (message.additional != null && message.hasOwnProperty('additional'))
          object.additional = $root.google.protobuf.Struct.toObject(
            message.additional,
            options,
          );
        if (message.createdBy != null && message.hasOwnProperty('createdBy'))
          object.createdBy = message.createdBy;
        if (message.fullName != null && message.hasOwnProperty('fullName'))
          object.fullName = message.fullName;
        if (message.firstName != null && message.hasOwnProperty('firstName'))
          object.firstName = message.firstName;
        if (message.lastName != null && message.hasOwnProperty('lastName'))
          object.lastName = message.lastName;
        if (message.email != null && message.hasOwnProperty('email'))
          object.email = message.email;
        if (message.idNumber != null && message.hasOwnProperty('idNumber'))
          object.idNumber = message.idNumber;
        if (message.phone != null && message.hasOwnProperty('phone'))
          object.phone = message.phone;
        if (message.address != null && message.hasOwnProperty('address'))
          object.address = message.address;
        if (message.postCode != null && message.hasOwnProperty('postCode'))
          object.postCode = message.postCode;
        if (message.photo != null && message.hasOwnProperty('photo'))
          object.photo = message.photo;
        if (message.cover != null && message.hasOwnProperty('cover'))
          object.cover = message.cover;
        if (message.gender != null && message.hasOwnProperty('gender'))
          object.gender = message.gender;
        if (message.religion != null && message.hasOwnProperty('religion'))
          object.religion = message.religion;
        if (message.createdAt != null && message.hasOwnProperty('createdAt'))
          object.createdAt = message.createdAt;
        if (message.updatedAt != null && message.hasOwnProperty('updatedAt'))
          object.updatedAt = message.updatedAt;
        if (message.deletedAt != null && message.hasOwnProperty('deletedAt'))
          object.deletedAt = message.deletedAt;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof User.ResponseGetAll.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Data;
    })();

    ResponseGetAll.Meta = (function () {
      /**
       * Properties of a Meta.
       * @memberof User.ResponseGetAll
       * @interface IMeta
       * @property {number|null} [pagination] Meta pagination
       * @property {number|null} [limit] Meta limit
       * @property {number|null} [totalPage] Meta totalPage
       * @property {number|null} [count] Meta count
       * @property {string|null} [lowerThan] Meta lowerThan
       * @property {string|null} [greaterThan] Meta greaterThan
       */

      /**
       * Constructs a new Meta.
       * @memberof User.ResponseGetAll
       * @classdesc Represents a Meta.
       * @implements IMeta
       * @constructor
       * @param {User.ResponseGetAll.IMeta=} [properties] Properties to set
       */
      function Meta(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Meta pagination.
       * @member {number} pagination
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.pagination = 0;

      /**
       * Meta limit.
       * @member {number} limit
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.limit = 0;

      /**
       * Meta totalPage.
       * @member {number} totalPage
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.totalPage = 0;

      /**
       * Meta count.
       * @member {number} count
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.count = 0;

      /**
       * Meta lowerThan.
       * @member {string} lowerThan
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.lowerThan = '';

      /**
       * Meta greaterThan.
       * @member {string} greaterThan
       * @memberof User.ResponseGetAll.Meta
       * @instance
       */
      Meta.prototype.greaterThan = '';

      /**
       * Creates a new Meta instance using the specified properties.
       * @function create
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {User.ResponseGetAll.IMeta=} [properties] Properties to set
       * @returns {User.ResponseGetAll.Meta} Meta instance
       */
      Meta.create = function create(properties) {
        return new Meta(properties);
      };

      /**
       * Encodes the specified Meta message. Does not implicitly {@link User.ResponseGetAll.Meta.verify|verify} messages.
       * @function encode
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {User.ResponseGetAll.IMeta} message Meta message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Meta.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.pagination != null &&
          Object.hasOwnProperty.call(message, 'pagination')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.pagination);
        if (
          message.limit != null &&
          Object.hasOwnProperty.call(message, 'limit')
        )
          writer.uint32(/* id 2, wireType 0 =*/ 16).int32(message.limit);
        if (
          message.totalPage != null &&
          Object.hasOwnProperty.call(message, 'totalPage')
        )
          writer.uint32(/* id 3, wireType 0 =*/ 24).int32(message.totalPage);
        if (
          message.count != null &&
          Object.hasOwnProperty.call(message, 'count')
        )
          writer.uint32(/* id 4, wireType 0 =*/ 32).int32(message.count);
        if (
          message.lowerThan != null &&
          Object.hasOwnProperty.call(message, 'lowerThan')
        )
          writer.uint32(/* id 5, wireType 2 =*/ 42).string(message.lowerThan);
        if (
          message.greaterThan != null &&
          Object.hasOwnProperty.call(message, 'greaterThan')
        )
          writer.uint32(/* id 6, wireType 2 =*/ 50).string(message.greaterThan);
        return writer;
      };

      /**
       * Encodes the specified Meta message, length delimited. Does not implicitly {@link User.ResponseGetAll.Meta.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {User.ResponseGetAll.IMeta} message Meta message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Meta.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Meta message from the specified reader or buffer.
       * @function decode
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.ResponseGetAll.Meta} Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Meta.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.ResponseGetAll.Meta();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.pagination = reader.int32();
              break;
            case 2:
              message.limit = reader.int32();
              break;
            case 3:
              message.totalPage = reader.int32();
              break;
            case 4:
              message.count = reader.int32();
              break;
            case 5:
              message.lowerThan = reader.string();
              break;
            case 6:
              message.greaterThan = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Meta message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.ResponseGetAll.Meta} Meta
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Meta.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Meta message.
       * @function verify
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Meta.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.pagination != null && message.hasOwnProperty('pagination'))
          if (!$util.isInteger(message.pagination))
            return 'pagination: integer expected';
        if (message.limit != null && message.hasOwnProperty('limit'))
          if (!$util.isInteger(message.limit)) return 'limit: integer expected';
        if (message.totalPage != null && message.hasOwnProperty('totalPage'))
          if (!$util.isInteger(message.totalPage))
            return 'totalPage: integer expected';
        if (message.count != null && message.hasOwnProperty('count'))
          if (!$util.isInteger(message.count)) return 'count: integer expected';
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan'))
          if (!$util.isString(message.lowerThan))
            return 'lowerThan: string expected';
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        )
          if (!$util.isString(message.greaterThan))
            return 'greaterThan: string expected';
        return null;
      };

      /**
       * Creates a Meta message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.ResponseGetAll.Meta} Meta
       */
      Meta.fromObject = function fromObject(object) {
        if (object instanceof $root.User.ResponseGetAll.Meta) return object;
        var message = new $root.User.ResponseGetAll.Meta();
        if (object.pagination != null)
          message.pagination = object.pagination | 0;
        if (object.limit != null) message.limit = object.limit | 0;
        if (object.totalPage != null) message.totalPage = object.totalPage | 0;
        if (object.count != null) message.count = object.count | 0;
        if (object.lowerThan != null)
          message.lowerThan = String(object.lowerThan);
        if (object.greaterThan != null)
          message.greaterThan = String(object.greaterThan);
        return message;
      };

      /**
       * Creates a plain object from a Meta message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.ResponseGetAll.Meta
       * @static
       * @param {User.ResponseGetAll.Meta} message Meta
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Meta.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.pagination = 0;
          object.limit = 0;
          object.totalPage = 0;
          object.count = 0;
          object.lowerThan = '';
          object.greaterThan = '';
        }
        if (message.pagination != null && message.hasOwnProperty('pagination'))
          object.pagination = message.pagination;
        if (message.limit != null && message.hasOwnProperty('limit'))
          object.limit = message.limit;
        if (message.totalPage != null && message.hasOwnProperty('totalPage'))
          object.totalPage = message.totalPage;
        if (message.count != null && message.hasOwnProperty('count'))
          object.count = message.count;
        if (message.lowerThan != null && message.hasOwnProperty('lowerThan'))
          object.lowerThan = message.lowerThan;
        if (
          message.greaterThan != null &&
          message.hasOwnProperty('greaterThan')
        )
          object.greaterThan = message.greaterThan;
        return object;
      };

      /**
       * Converts this Meta to JSON.
       * @function toJSON
       * @memberof User.ResponseGetAll.Meta
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Meta.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Meta;
    })();

    return ResponseGetAll;
  })();

  User.ResponseDetail = (function () {
    /**
     * Properties of a ResponseDetail.
     * @memberof User
     * @interface IResponseDetail
     * @property {User.ResponseGetAll.IData|null} [data] ResponseDetail data
     * @property {boolean|null} [success] ResponseDetail success
     * @property {string|null} [message] ResponseDetail message
     */

    /**
     * Constructs a new ResponseDetail.
     * @memberof User
     * @classdesc Represents a ResponseDetail.
     * @implements IResponseDetail
     * @constructor
     * @param {User.IResponseDetail=} [properties] Properties to set
     */
    function ResponseDetail(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseDetail data.
     * @member {User.ResponseGetAll.IData|null|undefined} data
     * @memberof User.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.data = null;

    /**
     * ResponseDetail success.
     * @member {boolean} success
     * @memberof User.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.success = false;

    /**
     * ResponseDetail message.
     * @member {string} message
     * @memberof User.ResponseDetail
     * @instance
     */
    ResponseDetail.prototype.message = '';

    /**
     * Creates a new ResponseDetail instance using the specified properties.
     * @function create
     * @memberof User.ResponseDetail
     * @static
     * @param {User.IResponseDetail=} [properties] Properties to set
     * @returns {User.ResponseDetail} ResponseDetail instance
     */
    ResponseDetail.create = function create(properties) {
      return new ResponseDetail(properties);
    };

    /**
     * Encodes the specified ResponseDetail message. Does not implicitly {@link User.ResponseDetail.verify|verify} messages.
     * @function encode
     * @memberof User.ResponseDetail
     * @static
     * @param {User.IResponseDetail} message ResponseDetail message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDetail.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.User.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 2, wireType 0 =*/ 16).bool(message.success);
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      return writer;
    };

    /**
     * Encodes the specified ResponseDetail message, length delimited. Does not implicitly {@link User.ResponseDetail.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.ResponseDetail
     * @static
     * @param {User.IResponseDetail} message ResponseDetail message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDetail.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer.
     * @function decode
     * @memberof User.ResponseDetail
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.ResponseDetail} ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDetail.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.ResponseDetail();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.User.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 2:
            message.success = reader.bool();
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseDetail message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.ResponseDetail
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.ResponseDetail} ResponseDetail
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDetail.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseDetail message.
     * @function verify
     * @memberof User.ResponseDetail
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseDetail.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.User.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseDetail message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.ResponseDetail
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.ResponseDetail} ResponseDetail
     */
    ResponseDetail.fromObject = function fromObject(object) {
      if (object instanceof $root.User.ResponseDetail) return object;
      var message = new $root.User.ResponseDetail();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.User.ResponseDetail.data: object expected');
        message.data = $root.User.ResponseGetAll.Data.fromObject(object.data);
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseDetail message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.ResponseDetail
     * @static
     * @param {User.ResponseDetail} message ResponseDetail
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseDetail.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.success = false;
        object.message = '';
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.User.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      return object;
    };

    /**
     * Converts this ResponseDetail to JSON.
     * @function toJSON
     * @memberof User.ResponseDetail
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseDetail.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseDetail;
  })();

  User.ResponseCreate = (function () {
    /**
     * Properties of a ResponseCreate.
     * @memberof User
     * @interface IResponseCreate
     * @property {User.ResponseGetAll.IData|null} [data] ResponseCreate data
     * @property {string|null} [message] ResponseCreate message
     * @property {boolean|null} [success] ResponseCreate success
     */

    /**
     * Constructs a new ResponseCreate.
     * @memberof User
     * @classdesc Represents a ResponseCreate.
     * @implements IResponseCreate
     * @constructor
     * @param {User.IResponseCreate=} [properties] Properties to set
     */
    function ResponseCreate(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseCreate data.
     * @member {User.ResponseGetAll.IData|null|undefined} data
     * @memberof User.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.data = null;

    /**
     * ResponseCreate message.
     * @member {string} message
     * @memberof User.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.message = '';

    /**
     * ResponseCreate success.
     * @member {boolean} success
     * @memberof User.ResponseCreate
     * @instance
     */
    ResponseCreate.prototype.success = false;

    /**
     * Creates a new ResponseCreate instance using the specified properties.
     * @function create
     * @memberof User.ResponseCreate
     * @static
     * @param {User.IResponseCreate=} [properties] Properties to set
     * @returns {User.ResponseCreate} ResponseCreate instance
     */
    ResponseCreate.create = function create(properties) {
      return new ResponseCreate(properties);
    };

    /**
     * Encodes the specified ResponseCreate message. Does not implicitly {@link User.ResponseCreate.verify|verify} messages.
     * @function encode
     * @memberof User.ResponseCreate
     * @static
     * @param {User.IResponseCreate} message ResponseCreate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseCreate.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.User.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 3, wireType 0 =*/ 24).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseCreate message, length delimited. Does not implicitly {@link User.ResponseCreate.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.ResponseCreate
     * @static
     * @param {User.IResponseCreate} message ResponseCreate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseCreate.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer.
     * @function decode
     * @memberof User.ResponseCreate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.ResponseCreate} ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseCreate.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.ResponseCreate();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.User.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 2:
            message.message = reader.string();
            break;
          case 3:
            message.success = reader.bool();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseCreate message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.ResponseCreate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.ResponseCreate} ResponseCreate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseCreate.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseCreate message.
     * @function verify
     * @memberof User.ResponseCreate
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseCreate.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.User.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      return null;
    };

    /**
     * Creates a ResponseCreate message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.ResponseCreate
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.ResponseCreate} ResponseCreate
     */
    ResponseCreate.fromObject = function fromObject(object) {
      if (object instanceof $root.User.ResponseCreate) return object;
      var message = new $root.User.ResponseCreate();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.User.ResponseCreate.data: object expected');
        message.data = $root.User.ResponseGetAll.Data.fromObject(object.data);
      }
      if (object.message != null) message.message = String(object.message);
      if (object.success != null) message.success = Boolean(object.success);
      return message;
    };

    /**
     * Creates a plain object from a ResponseCreate message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.ResponseCreate
     * @static
     * @param {User.ResponseCreate} message ResponseCreate
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseCreate.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.User.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseCreate to JSON.
     * @function toJSON
     * @memberof User.ResponseCreate
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseCreate.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseCreate;
  })();

  User.ResponseUpdate = (function () {
    /**
     * Properties of a ResponseUpdate.
     * @memberof User
     * @interface IResponseUpdate
     * @property {User.ResponseGetAll.IData|null} [data] ResponseUpdate data
     * @property {boolean|null} [success] ResponseUpdate success
     * @property {string|null} [message] ResponseUpdate message
     */

    /**
     * Constructs a new ResponseUpdate.
     * @memberof User
     * @classdesc Represents a ResponseUpdate.
     * @implements IResponseUpdate
     * @constructor
     * @param {User.IResponseUpdate=} [properties] Properties to set
     */
    function ResponseUpdate(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseUpdate data.
     * @member {User.ResponseGetAll.IData|null|undefined} data
     * @memberof User.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.data = null;

    /**
     * ResponseUpdate success.
     * @member {boolean} success
     * @memberof User.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.success = false;

    /**
     * ResponseUpdate message.
     * @member {string} message
     * @memberof User.ResponseUpdate
     * @instance
     */
    ResponseUpdate.prototype.message = '';

    /**
     * Creates a new ResponseUpdate instance using the specified properties.
     * @function create
     * @memberof User.ResponseUpdate
     * @static
     * @param {User.IResponseUpdate=} [properties] Properties to set
     * @returns {User.ResponseUpdate} ResponseUpdate instance
     */
    ResponseUpdate.create = function create(properties) {
      return new ResponseUpdate(properties);
    };

    /**
     * Encodes the specified ResponseUpdate message. Does not implicitly {@link User.ResponseUpdate.verify|verify} messages.
     * @function encode
     * @memberof User.ResponseUpdate
     * @static
     * @param {User.IResponseUpdate} message ResponseUpdate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseUpdate.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.User.ResponseGetAll.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseUpdate message, length delimited. Does not implicitly {@link User.ResponseUpdate.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.ResponseUpdate
     * @static
     * @param {User.IResponseUpdate} message ResponseUpdate message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseUpdate.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer.
     * @function decode
     * @memberof User.ResponseUpdate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.ResponseUpdate} ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseUpdate.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.ResponseUpdate();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.User.ResponseGetAll.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.success = reader.bool();
            break;
          case 3:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseUpdate message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.ResponseUpdate
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.ResponseUpdate} ResponseUpdate
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseUpdate.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseUpdate message.
     * @function verify
     * @memberof User.ResponseUpdate
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseUpdate.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.User.ResponseGetAll.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseUpdate message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.ResponseUpdate
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.ResponseUpdate} ResponseUpdate
     */
    ResponseUpdate.fromObject = function fromObject(object) {
      if (object instanceof $root.User.ResponseUpdate) return object;
      var message = new $root.User.ResponseUpdate();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.User.ResponseUpdate.data: object expected');
        message.data = $root.User.ResponseGetAll.Data.fromObject(object.data);
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseUpdate message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.ResponseUpdate
     * @static
     * @param {User.ResponseUpdate} message ResponseUpdate
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseUpdate.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.User.ResponseGetAll.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseUpdate to JSON.
     * @function toJSON
     * @memberof User.ResponseUpdate
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseUpdate.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ResponseUpdate;
  })();

  User.ResponseDelete = (function () {
    /**
     * Properties of a ResponseDelete.
     * @memberof User
     * @interface IResponseDelete
     * @property {User.ResponseDelete.IData|null} [data] ResponseDelete data
     * @property {boolean|null} [success] ResponseDelete success
     * @property {string|null} [message] ResponseDelete message
     */

    /**
     * Constructs a new ResponseDelete.
     * @memberof User
     * @classdesc Represents a ResponseDelete.
     * @implements IResponseDelete
     * @constructor
     * @param {User.IResponseDelete=} [properties] Properties to set
     */
    function ResponseDelete(properties) {
      if (properties)
        for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
          if (properties[keys[i]] != null) this[keys[i]] = properties[keys[i]];
    }

    /**
     * ResponseDelete data.
     * @member {User.ResponseDelete.IData|null|undefined} data
     * @memberof User.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.data = null;

    /**
     * ResponseDelete success.
     * @member {boolean} success
     * @memberof User.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.success = false;

    /**
     * ResponseDelete message.
     * @member {string} message
     * @memberof User.ResponseDelete
     * @instance
     */
    ResponseDelete.prototype.message = '';

    /**
     * Creates a new ResponseDelete instance using the specified properties.
     * @function create
     * @memberof User.ResponseDelete
     * @static
     * @param {User.IResponseDelete=} [properties] Properties to set
     * @returns {User.ResponseDelete} ResponseDelete instance
     */
    ResponseDelete.create = function create(properties) {
      return new ResponseDelete(properties);
    };

    /**
     * Encodes the specified ResponseDelete message. Does not implicitly {@link User.ResponseDelete.verify|verify} messages.
     * @function encode
     * @memberof User.ResponseDelete
     * @static
     * @param {User.IResponseDelete} message ResponseDelete message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDelete.encode = function encode(message, writer) {
      if (!writer) writer = $Writer.create();
      if (message.data != null && Object.hasOwnProperty.call(message, 'data'))
        $root.User.ResponseDelete.Data.encode(
          message.data,
          writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
        ).ldelim();
      if (
        message.message != null &&
        Object.hasOwnProperty.call(message, 'message')
      )
        writer.uint32(/* id 2, wireType 2 =*/ 18).string(message.message);
      if (
        message.success != null &&
        Object.hasOwnProperty.call(message, 'success')
      )
        writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.success);
      return writer;
    };

    /**
     * Encodes the specified ResponseDelete message, length delimited. Does not implicitly {@link User.ResponseDelete.verify|verify} messages.
     * @function encodeDelimited
     * @memberof User.ResponseDelete
     * @static
     * @param {User.IResponseDelete} message ResponseDelete message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ResponseDelete.encodeDelimited = function encodeDelimited(message, writer) {
      return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer.
     * @function decode
     * @memberof User.ResponseDelete
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {User.ResponseDelete} ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDelete.decode = function decode(reader, length) {
      if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
      var end = length === undefined ? reader.len : reader.pos + length,
        message = new $root.User.ResponseDelete();
      while (reader.pos < end) {
        var tag = reader.uint32();
        switch (tag >>> 3) {
          case 1:
            message.data = $root.User.ResponseDelete.Data.decode(
              reader,
              reader.uint32(),
            );
            break;
          case 4:
            message.success = reader.bool();
            break;
          case 2:
            message.message = reader.string();
            break;
          default:
            reader.skipType(tag & 7);
            break;
        }
      }
      return message;
    };

    /**
     * Decodes a ResponseDelete message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof User.ResponseDelete
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {User.ResponseDelete} ResponseDelete
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ResponseDelete.decodeDelimited = function decodeDelimited(reader) {
      if (!(reader instanceof $Reader)) reader = new $Reader(reader);
      return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ResponseDelete message.
     * @function verify
     * @memberof User.ResponseDelete
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ResponseDelete.verify = function verify(message) {
      if (typeof message !== 'object' || message === null)
        return 'object expected';
      if (message.data != null && message.hasOwnProperty('data')) {
        var error = $root.User.ResponseDelete.Data.verify(message.data);
        if (error) return 'data.' + error;
      }
      if (message.success != null && message.hasOwnProperty('success'))
        if (typeof message.success !== 'boolean')
          return 'success: boolean expected';
      if (message.message != null && message.hasOwnProperty('message'))
        if (!$util.isString(message.message)) return 'message: string expected';
      return null;
    };

    /**
     * Creates a ResponseDelete message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof User.ResponseDelete
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {User.ResponseDelete} ResponseDelete
     */
    ResponseDelete.fromObject = function fromObject(object) {
      if (object instanceof $root.User.ResponseDelete) return object;
      var message = new $root.User.ResponseDelete();
      if (object.data != null) {
        if (typeof object.data !== 'object')
          throw TypeError('.User.ResponseDelete.data: object expected');
        message.data = $root.User.ResponseDelete.Data.fromObject(object.data);
      }
      if (object.success != null) message.success = Boolean(object.success);
      if (object.message != null) message.message = String(object.message);
      return message;
    };

    /**
     * Creates a plain object from a ResponseDelete message. Also converts values to other types if specified.
     * @function toObject
     * @memberof User.ResponseDelete
     * @static
     * @param {User.ResponseDelete} message ResponseDelete
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ResponseDelete.toObject = function toObject(message, options) {
      if (!options) options = {};
      var object = {};
      if (options.defaults) {
        object.data = null;
        object.message = '';
        object.success = false;
      }
      if (message.data != null && message.hasOwnProperty('data'))
        object.data = $root.User.ResponseDelete.Data.toObject(
          message.data,
          options,
        );
      if (message.message != null && message.hasOwnProperty('message'))
        object.message = message.message;
      if (message.success != null && message.hasOwnProperty('success'))
        object.success = message.success;
      return object;
    };

    /**
     * Converts this ResponseDelete to JSON.
     * @function toJSON
     * @memberof User.ResponseDelete
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ResponseDelete.prototype.toJSON = function toJSON() {
      return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    ResponseDelete.Data = (function () {
      /**
       * Properties of a Data.
       * @memberof User.ResponseDelete
       * @interface IData
       * @property {string|null} [message] Data message
       */

      /**
       * Constructs a new Data.
       * @memberof User.ResponseDelete
       * @classdesc Represents a Data.
       * @implements IData
       * @constructor
       * @param {User.ResponseDelete.IData=} [properties] Properties to set
       */
      function Data(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Data message.
       * @member {string} message
       * @memberof User.ResponseDelete.Data
       * @instance
       */
      Data.prototype.message = '';

      /**
       * Creates a new Data instance using the specified properties.
       * @function create
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {User.ResponseDelete.IData=} [properties] Properties to set
       * @returns {User.ResponseDelete.Data} Data instance
       */
      Data.create = function create(properties) {
        return new Data(properties);
      };

      /**
       * Encodes the specified Data message. Does not implicitly {@link User.ResponseDelete.Data.verify|verify} messages.
       * @function encode
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {User.ResponseDelete.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.message != null &&
          Object.hasOwnProperty.call(message, 'message')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.message);
        return writer;
      };

      /**
       * Encodes the specified Data message, length delimited. Does not implicitly {@link User.ResponseDelete.Data.verify|verify} messages.
       * @function encodeDelimited
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {User.ResponseDelete.IData} message Data message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Data.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Data message from the specified reader or buffer.
       * @function decode
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {User.ResponseDelete.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.User.ResponseDelete.Data();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.message = reader.string();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Data message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {User.ResponseDelete.Data} Data
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Data.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Data message.
       * @function verify
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Data.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.message != null && message.hasOwnProperty('message'))
          if (!$util.isString(message.message))
            return 'message: string expected';
        return null;
      };

      /**
       * Creates a Data message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {User.ResponseDelete.Data} Data
       */
      Data.fromObject = function fromObject(object) {
        if (object instanceof $root.User.ResponseDelete.Data) return object;
        var message = new $root.User.ResponseDelete.Data();
        if (object.message != null) message.message = String(object.message);
        return message;
      };

      /**
       * Creates a plain object from a Data message. Also converts values to other types if specified.
       * @function toObject
       * @memberof User.ResponseDelete.Data
       * @static
       * @param {User.ResponseDelete.Data} message Data
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Data.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) object.message = '';
        if (message.message != null && message.hasOwnProperty('message'))
          object.message = message.message;
        return object;
      };

      /**
       * Converts this Data to JSON.
       * @function toJSON
       * @memberof User.ResponseDelete.Data
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Data.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Data;
    })();

    return ResponseDelete;
  })();

  return User;
})();

$root.google = (function () {
  /**
   * Namespace google.
   * @exports google
   * @namespace
   */
  var google = {};

  google.protobuf = (function () {
    /**
     * Namespace protobuf.
     * @memberof google
     * @namespace
     */
    var protobuf = {};

    protobuf.Any = (function () {
      /**
       * Properties of an Any.
       * @memberof google.protobuf
       * @interface IAny
       * @property {string|null} [type_url] Any type_url
       * @property {Uint8Array|null} [value] Any value
       */

      /**
       * Constructs a new Any.
       * @memberof google.protobuf
       * @classdesc Represents an Any.
       * @implements IAny
       * @constructor
       * @param {google.protobuf.IAny=} [properties] Properties to set
       */
      function Any(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Any type_url.
       * @member {string} type_url
       * @memberof google.protobuf.Any
       * @instance
       */
      Any.prototype.type_url = '';

      /**
       * Any value.
       * @member {Uint8Array} value
       * @memberof google.protobuf.Any
       * @instance
       */
      Any.prototype.value = $util.newBuffer([]);

      /**
       * Creates a new Any instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny=} [properties] Properties to set
       * @returns {google.protobuf.Any} Any instance
       */
      Any.create = function create(properties) {
        return new Any(properties);
      };

      /**
       * Encodes the specified Any message. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny} message Any message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Any.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.type_url != null &&
          Object.hasOwnProperty.call(message, 'type_url')
        )
          writer.uint32(/* id 1, wireType 2 =*/ 10).string(message.type_url);
        if (
          message.value != null &&
          Object.hasOwnProperty.call(message, 'value')
        )
          writer.uint32(/* id 2, wireType 2 =*/ 18).bytes(message.value);
        return writer;
      };

      /**
       * Encodes the specified Any message, length delimited. Does not implicitly {@link google.protobuf.Any.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.IAny} message Any message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Any.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes an Any message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Any
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Any} Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Any.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Any();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.type_url = reader.string();
              break;
            case 2:
              message.value = reader.bytes();
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes an Any message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Any
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Any} Any
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Any.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies an Any message.
       * @function verify
       * @memberof google.protobuf.Any
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Any.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.type_url != null && message.hasOwnProperty('type_url'))
          if (!$util.isString(message.type_url))
            return 'type_url: string expected';
        if (message.value != null && message.hasOwnProperty('value'))
          if (
            !(
              (message.value && typeof message.value.length === 'number') ||
              $util.isString(message.value)
            )
          )
            return 'value: buffer expected';
        return null;
      };

      /**
       * Creates an Any message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Any
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Any} Any
       */
      Any.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Any) return object;
        var message = new $root.google.protobuf.Any();
        if (object.type_url != null) message.type_url = String(object.type_url);
        if (object.value != null)
          if (typeof object.value === 'string')
            $util.base64.decode(
              object.value,
              (message.value = $util.newBuffer(
                $util.base64.length(object.value),
              )),
              0,
            );
          else if (object.value.length) message.value = object.value;
        return message;
      };

      /**
       * Creates a plain object from an Any message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Any
       * @static
       * @param {google.protobuf.Any} message Any
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Any.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.defaults) {
          object.type_url = '';
          if (options.bytes === String) object.value = '';
          else {
            object.value = [];
            if (options.bytes !== Array)
              object.value = $util.newBuffer(object.value);
          }
        }
        if (message.type_url != null && message.hasOwnProperty('type_url'))
          object.type_url = message.type_url;
        if (message.value != null && message.hasOwnProperty('value'))
          object.value =
            options.bytes === String
              ? $util.base64.encode(message.value, 0, message.value.length)
              : options.bytes === Array
              ? Array.prototype.slice.call(message.value)
              : message.value;
        return object;
      };

      /**
       * Converts this Any to JSON.
       * @function toJSON
       * @memberof google.protobuf.Any
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Any.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Any;
    })();

    protobuf.Struct = (function () {
      /**
       * Properties of a Struct.
       * @memberof google.protobuf
       * @interface IStruct
       * @property {Object.<string,google.protobuf.IValue>|null} [fields] Struct fields
       */

      /**
       * Constructs a new Struct.
       * @memberof google.protobuf
       * @classdesc Represents a Struct.
       * @implements IStruct
       * @constructor
       * @param {google.protobuf.IStruct=} [properties] Properties to set
       */
      function Struct(properties) {
        this.fields = {};
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Struct fields.
       * @member {Object.<string,google.protobuf.IValue>} fields
       * @memberof google.protobuf.Struct
       * @instance
       */
      Struct.prototype.fields = $util.emptyObject;

      /**
       * Creates a new Struct instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct=} [properties] Properties to set
       * @returns {google.protobuf.Struct} Struct instance
       */
      Struct.create = function create(properties) {
        return new Struct(properties);
      };

      /**
       * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct} message Struct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Struct.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.fields != null &&
          Object.hasOwnProperty.call(message, 'fields')
        )
          for (
            var keys = Object.keys(message.fields), i = 0;
            i < keys.length;
            ++i
          ) {
            writer
              .uint32(/* id 1, wireType 2 =*/ 10)
              .fork()
              .uint32(/* id 1, wireType 2 =*/ 10)
              .string(keys[i]);
            $root.google.protobuf.Value.encode(
              message.fields[keys[i]],
              writer.uint32(/* id 2, wireType 2 =*/ 18).fork(),
            )
              .ldelim()
              .ldelim();
          }
        return writer;
      };

      /**
       * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.IStruct} message Struct message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Struct.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Struct message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Struct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Struct} Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Struct.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Struct(),
          key,
          value;
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              if (message.fields === $util.emptyObject) message.fields = {};
              var end2 = reader.uint32() + reader.pos;
              key = '';
              value = null;
              while (reader.pos < end2) {
                var tag2 = reader.uint32();
                switch (tag2 >>> 3) {
                  case 1:
                    key = reader.string();
                    break;
                  case 2:
                    value = $root.google.protobuf.Value.decode(
                      reader,
                      reader.uint32(),
                    );
                    break;
                  default:
                    reader.skipType(tag2 & 7);
                    break;
                }
              }
              message.fields[key] = value;
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Struct message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Struct
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Struct} Struct
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Struct.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Struct message.
       * @function verify
       * @memberof google.protobuf.Struct
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Struct.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.fields != null && message.hasOwnProperty('fields')) {
          if (!$util.isObject(message.fields)) return 'fields: object expected';
          var key = Object.keys(message.fields);
          for (var i = 0; i < key.length; ++i) {
            var error = $root.google.protobuf.Value.verify(
              message.fields[key[i]],
            );
            if (error) return 'fields.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a Struct message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Struct
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Struct} Struct
       */
      Struct.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Struct) return object;
        var message = new $root.google.protobuf.Struct();
        if (object.fields) {
          if (typeof object.fields !== 'object')
            throw TypeError('.google.protobuf.Struct.fields: object expected');
          message.fields = {};
          for (
            var keys = Object.keys(object.fields), i = 0;
            i < keys.length;
            ++i
          ) {
            if (typeof object.fields[keys[i]] !== 'object')
              throw TypeError(
                '.google.protobuf.Struct.fields: object expected',
              );
            message.fields[keys[i]] = $root.google.protobuf.Value.fromObject(
              object.fields[keys[i]],
            );
          }
        }
        return message;
      };

      /**
       * Creates a plain object from a Struct message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Struct
       * @static
       * @param {google.protobuf.Struct} message Struct
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Struct.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.objects || options.defaults) object.fields = {};
        var keys2;
        if (message.fields && (keys2 = Object.keys(message.fields)).length) {
          object.fields = {};
          for (var j = 0; j < keys2.length; ++j)
            object.fields[keys2[j]] = $root.google.protobuf.Value.toObject(
              message.fields[keys2[j]],
              options,
            );
        }
        return object;
      };

      /**
       * Converts this Struct to JSON.
       * @function toJSON
       * @memberof google.protobuf.Struct
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Struct.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Struct;
    })();

    protobuf.Value = (function () {
      /**
       * Properties of a Value.
       * @memberof google.protobuf
       * @interface IValue
       * @property {google.protobuf.NullValue|null} [nullValue] Value nullValue
       * @property {number|null} [numberValue] Value numberValue
       * @property {string|null} [stringValue] Value stringValue
       * @property {boolean|null} [boolValue] Value boolValue
       * @property {google.protobuf.IStruct|null} [structValue] Value structValue
       * @property {google.protobuf.IListValue|null} [listValue] Value listValue
       */

      /**
       * Constructs a new Value.
       * @memberof google.protobuf
       * @classdesc Represents a Value.
       * @implements IValue
       * @constructor
       * @param {google.protobuf.IValue=} [properties] Properties to set
       */
      function Value(properties) {
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * Value nullValue.
       * @member {google.protobuf.NullValue|null|undefined} nullValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.nullValue = null;

      /**
       * Value numberValue.
       * @member {number|null|undefined} numberValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.numberValue = null;

      /**
       * Value stringValue.
       * @member {string|null|undefined} stringValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.stringValue = null;

      /**
       * Value boolValue.
       * @member {boolean|null|undefined} boolValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.boolValue = null;

      /**
       * Value structValue.
       * @member {google.protobuf.IStruct|null|undefined} structValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.structValue = null;

      /**
       * Value listValue.
       * @member {google.protobuf.IListValue|null|undefined} listValue
       * @memberof google.protobuf.Value
       * @instance
       */
      Value.prototype.listValue = null;

      // OneOf field names bound to virtual getters and setters
      var $oneOfFields;

      /**
       * Value kind.
       * @member {"nullValue"|"numberValue"|"stringValue"|"boolValue"|"structValue"|"listValue"|undefined} kind
       * @memberof google.protobuf.Value
       * @instance
       */
      Object.defineProperty(Value.prototype, 'kind', {
        get: $util.oneOfGetter(
          ($oneOfFields = [
            'nullValue',
            'numberValue',
            'stringValue',
            'boolValue',
            'structValue',
            'listValue',
          ]),
        ),
        set: $util.oneOfSetter($oneOfFields),
      });

      /**
       * Creates a new Value instance using the specified properties.
       * @function create
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue=} [properties] Properties to set
       * @returns {google.protobuf.Value} Value instance
       */
      Value.create = function create(properties) {
        return new Value(properties);
      };

      /**
       * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue} message Value message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Value.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (
          message.nullValue != null &&
          Object.hasOwnProperty.call(message, 'nullValue')
        )
          writer.uint32(/* id 1, wireType 0 =*/ 8).int32(message.nullValue);
        if (
          message.numberValue != null &&
          Object.hasOwnProperty.call(message, 'numberValue')
        )
          writer.uint32(/* id 2, wireType 1 =*/ 17).double(message.numberValue);
        if (
          message.stringValue != null &&
          Object.hasOwnProperty.call(message, 'stringValue')
        )
          writer.uint32(/* id 3, wireType 2 =*/ 26).string(message.stringValue);
        if (
          message.boolValue != null &&
          Object.hasOwnProperty.call(message, 'boolValue')
        )
          writer.uint32(/* id 4, wireType 0 =*/ 32).bool(message.boolValue);
        if (
          message.structValue != null &&
          Object.hasOwnProperty.call(message, 'structValue')
        )
          $root.google.protobuf.Struct.encode(
            message.structValue,
            writer.uint32(/* id 5, wireType 2 =*/ 42).fork(),
          ).ldelim();
        if (
          message.listValue != null &&
          Object.hasOwnProperty.call(message, 'listValue')
        )
          $root.google.protobuf.ListValue.encode(
            message.listValue,
            writer.uint32(/* id 6, wireType 2 =*/ 50).fork(),
          ).ldelim();
        return writer;
      };

      /**
       * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.IValue} message Value message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      Value.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a Value message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.Value
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.Value} Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Value.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.Value();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              message.nullValue = reader.int32();
              break;
            case 2:
              message.numberValue = reader.double();
              break;
            case 3:
              message.stringValue = reader.string();
              break;
            case 4:
              message.boolValue = reader.bool();
              break;
            case 5:
              message.structValue = $root.google.protobuf.Struct.decode(
                reader,
                reader.uint32(),
              );
              break;
            case 6:
              message.listValue = $root.google.protobuf.ListValue.decode(
                reader,
                reader.uint32(),
              );
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a Value message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.Value
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.Value} Value
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      Value.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a Value message.
       * @function verify
       * @memberof google.protobuf.Value
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      Value.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        var properties = {};
        if (message.nullValue != null && message.hasOwnProperty('nullValue')) {
          properties.kind = 1;
          switch (message.nullValue) {
            default:
              return 'nullValue: enum value expected';
            case 0:
              break;
          }
        }
        if (
          message.numberValue != null &&
          message.hasOwnProperty('numberValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (typeof message.numberValue !== 'number')
            return 'numberValue: number expected';
        }
        if (
          message.stringValue != null &&
          message.hasOwnProperty('stringValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (!$util.isString(message.stringValue))
            return 'stringValue: string expected';
        }
        if (message.boolValue != null && message.hasOwnProperty('boolValue')) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          if (typeof message.boolValue !== 'boolean')
            return 'boolValue: boolean expected';
        }
        if (
          message.structValue != null &&
          message.hasOwnProperty('structValue')
        ) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          {
            var error = $root.google.protobuf.Struct.verify(
              message.structValue,
            );
            if (error) return 'structValue.' + error;
          }
        }
        if (message.listValue != null && message.hasOwnProperty('listValue')) {
          if (properties.kind === 1) return 'kind: multiple values';
          properties.kind = 1;
          {
            var error = $root.google.protobuf.ListValue.verify(
              message.listValue,
            );
            if (error) return 'listValue.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a Value message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.Value
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.Value} Value
       */
      Value.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.Value) return object;
        var message = new $root.google.protobuf.Value();
        switch (object.nullValue) {
          case 'NULL_VALUE':
          case 0:
            message.nullValue = 0;
            break;
        }
        if (object.numberValue != null)
          message.numberValue = Number(object.numberValue);
        if (object.stringValue != null)
          message.stringValue = String(object.stringValue);
        if (object.boolValue != null)
          message.boolValue = Boolean(object.boolValue);
        if (object.structValue != null) {
          if (typeof object.structValue !== 'object')
            throw TypeError(
              '.google.protobuf.Value.structValue: object expected',
            );
          message.structValue = $root.google.protobuf.Struct.fromObject(
            object.structValue,
          );
        }
        if (object.listValue != null) {
          if (typeof object.listValue !== 'object')
            throw TypeError(
              '.google.protobuf.Value.listValue: object expected',
            );
          message.listValue = $root.google.protobuf.ListValue.fromObject(
            object.listValue,
          );
        }
        return message;
      };

      /**
       * Creates a plain object from a Value message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.Value
       * @static
       * @param {google.protobuf.Value} message Value
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      Value.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (message.nullValue != null && message.hasOwnProperty('nullValue')) {
          object.nullValue =
            options.enums === String
              ? $root.google.protobuf.NullValue[message.nullValue]
              : message.nullValue;
          if (options.oneofs) object.kind = 'nullValue';
        }
        if (
          message.numberValue != null &&
          message.hasOwnProperty('numberValue')
        ) {
          object.numberValue =
            options.json && !isFinite(message.numberValue)
              ? String(message.numberValue)
              : message.numberValue;
          if (options.oneofs) object.kind = 'numberValue';
        }
        if (
          message.stringValue != null &&
          message.hasOwnProperty('stringValue')
        ) {
          object.stringValue = message.stringValue;
          if (options.oneofs) object.kind = 'stringValue';
        }
        if (message.boolValue != null && message.hasOwnProperty('boolValue')) {
          object.boolValue = message.boolValue;
          if (options.oneofs) object.kind = 'boolValue';
        }
        if (
          message.structValue != null &&
          message.hasOwnProperty('structValue')
        ) {
          object.structValue = $root.google.protobuf.Struct.toObject(
            message.structValue,
            options,
          );
          if (options.oneofs) object.kind = 'structValue';
        }
        if (message.listValue != null && message.hasOwnProperty('listValue')) {
          object.listValue = $root.google.protobuf.ListValue.toObject(
            message.listValue,
            options,
          );
          if (options.oneofs) object.kind = 'listValue';
        }
        return object;
      };

      /**
       * Converts this Value to JSON.
       * @function toJSON
       * @memberof google.protobuf.Value
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      Value.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return Value;
    })();

    /**
     * NullValue enum.
     * @name google.protobuf.NullValue
     * @enum {number}
     * @property {number} NULL_VALUE=0 NULL_VALUE value
     */
    protobuf.NullValue = (function () {
      var valuesById = {},
        values = Object.create(valuesById);
      values[(valuesById[0] = 'NULL_VALUE')] = 0;
      return values;
    })();

    protobuf.ListValue = (function () {
      /**
       * Properties of a ListValue.
       * @memberof google.protobuf
       * @interface IListValue
       * @property {Array.<google.protobuf.IValue>|null} [values] ListValue values
       */

      /**
       * Constructs a new ListValue.
       * @memberof google.protobuf
       * @classdesc Represents a ListValue.
       * @implements IListValue
       * @constructor
       * @param {google.protobuf.IListValue=} [properties] Properties to set
       */
      function ListValue(properties) {
        this.values = [];
        if (properties)
          for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
            if (properties[keys[i]] != null)
              this[keys[i]] = properties[keys[i]];
      }

      /**
       * ListValue values.
       * @member {Array.<google.protobuf.IValue>} values
       * @memberof google.protobuf.ListValue
       * @instance
       */
      ListValue.prototype.values = $util.emptyArray;

      /**
       * Creates a new ListValue instance using the specified properties.
       * @function create
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue=} [properties] Properties to set
       * @returns {google.protobuf.ListValue} ListValue instance
       */
      ListValue.create = function create(properties) {
        return new ListValue(properties);
      };

      /**
       * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @function encode
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      ListValue.encode = function encode(message, writer) {
        if (!writer) writer = $Writer.create();
        if (message.values != null && message.values.length)
          for (var i = 0; i < message.values.length; ++i)
            $root.google.protobuf.Value.encode(
              message.values[i],
              writer.uint32(/* id 1, wireType 2 =*/ 10).fork(),
            ).ldelim();
        return writer;
      };

      /**
       * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
       * @function encodeDelimited
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
       * @param {$protobuf.Writer} [writer] Writer to encode to
       * @returns {$protobuf.Writer} Writer
       */
      ListValue.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
      };

      /**
       * Decodes a ListValue message from the specified reader or buffer.
       * @function decode
       * @memberof google.protobuf.ListValue
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @param {number} [length] Message length if known beforehand
       * @returns {google.protobuf.ListValue} ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      ListValue.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader)) reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length,
          message = new $root.google.protobuf.ListValue();
        while (reader.pos < end) {
          var tag = reader.uint32();
          switch (tag >>> 3) {
            case 1:
              if (!(message.values && message.values.length))
                message.values = [];
              message.values.push(
                $root.google.protobuf.Value.decode(reader, reader.uint32()),
              );
              break;
            default:
              reader.skipType(tag & 7);
              break;
          }
        }
        return message;
      };

      /**
       * Decodes a ListValue message from the specified reader or buffer, length delimited.
       * @function decodeDelimited
       * @memberof google.protobuf.ListValue
       * @static
       * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
       * @returns {google.protobuf.ListValue} ListValue
       * @throws {Error} If the payload is not a reader or valid buffer
       * @throws {$protobuf.util.ProtocolError} If required fields are missing
       */
      ListValue.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader)) reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
      };

      /**
       * Verifies a ListValue message.
       * @function verify
       * @memberof google.protobuf.ListValue
       * @static
       * @param {Object.<string,*>} message Plain object to verify
       * @returns {string|null} `null` if valid, otherwise the reason why it is not
       */
      ListValue.verify = function verify(message) {
        if (typeof message !== 'object' || message === null)
          return 'object expected';
        if (message.values != null && message.hasOwnProperty('values')) {
          if (!Array.isArray(message.values)) return 'values: array expected';
          for (var i = 0; i < message.values.length; ++i) {
            var error = $root.google.protobuf.Value.verify(message.values[i]);
            if (error) return 'values.' + error;
          }
        }
        return null;
      };

      /**
       * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
       * @function fromObject
       * @memberof google.protobuf.ListValue
       * @static
       * @param {Object.<string,*>} object Plain object
       * @returns {google.protobuf.ListValue} ListValue
       */
      ListValue.fromObject = function fromObject(object) {
        if (object instanceof $root.google.protobuf.ListValue) return object;
        var message = new $root.google.protobuf.ListValue();
        if (object.values) {
          if (!Array.isArray(object.values))
            throw TypeError(
              '.google.protobuf.ListValue.values: array expected',
            );
          message.values = [];
          for (var i = 0; i < object.values.length; ++i) {
            if (typeof object.values[i] !== 'object')
              throw TypeError(
                '.google.protobuf.ListValue.values: object expected',
              );
            message.values[i] = $root.google.protobuf.Value.fromObject(
              object.values[i],
            );
          }
        }
        return message;
      };

      /**
       * Creates a plain object from a ListValue message. Also converts values to other types if specified.
       * @function toObject
       * @memberof google.protobuf.ListValue
       * @static
       * @param {google.protobuf.ListValue} message ListValue
       * @param {$protobuf.IConversionOptions} [options] Conversion options
       * @returns {Object.<string,*>} Plain object
       */
      ListValue.toObject = function toObject(message, options) {
        if (!options) options = {};
        var object = {};
        if (options.arrays || options.defaults) object.values = [];
        if (message.values && message.values.length) {
          object.values = [];
          for (var j = 0; j < message.values.length; ++j)
            object.values[j] = $root.google.protobuf.Value.toObject(
              message.values[j],
              options,
            );
        }
        return object;
      };

      /**
       * Converts this ListValue to JSON.
       * @function toJSON
       * @memberof google.protobuf.ListValue
       * @instance
       * @returns {Object.<string,*>} JSON object
       */
      ListValue.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
      };

      return ListValue;
    })();

    return protobuf;
  })();

  return google;
})();

module.exports = $root;
