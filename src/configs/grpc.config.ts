import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import * as dotenv from 'dotenv';
dotenv.config();
const protoDir = join(__dirname, '../../', 'protobufs');
const port: number = +process.env.PORT + 1;
const grpcHost: string = process.env.GRPC_HOST;

export const grpcServerOptions = {
  transport: Transport.GRPC,
  options: {
    url: `${grpcHost}:${port}`,
    package: 'DataCore',
    protoPath: protoDir + '/data-core/data-core.proto',
    loader: {
      keepCase: true,
      longs: Number,
      defaults: false,
      arrays: true,
      objects: true,
      includeDirs: [protoDir+ '/data-core'],
    },
  },
};
