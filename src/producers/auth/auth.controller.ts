import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { ServerUnaryCall } from 'grpc';
import { MetaData } from 'src/utils/api/metadata.interface';
import { AuthService } from './auth.service';
import { genSalt, hash } from 'bcryptjs'
import { AuthGetCurrentUserRequest, AuthLoginRequest, AuthLogoutRequest, AuthRefreshTokenRequest } from './interfaces/requests.interface';
import { AuthGetCurrentUserResponse, AuthLoginResponse, AuthLogoutResponse, AuthRefreshTokenResponse } from './interfaces/responses.interface';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @GrpcMethod('Greeter', 'login')
  async login(
    data: AuthLoginRequest,
    meta: MetaData,
    call: ServerUnaryCall<any>
  ): Promise<AuthLoginResponse> {
    try {
      const resultData = await this.authService.login(data.body);

      return {
        success: true,
        data: resultData
      };
    } catch(e) {
      return {
        success: false,
        data: null,
        message: e
      };
    }
  }

  @GrpcMethod('Greeter', 'getCurrentUser')
  async getCurrentUser(
    data: AuthGetCurrentUserRequest,
    meta: MetaData,
    call: ServerUnaryCall<any>
  ): Promise<AuthGetCurrentUserResponse> {
    try {
      const token = meta.get('token')[0].toString();

      const resultData = await this.authService.getCurrentUser(token);

      return {
        success: true,
        data: resultData
      }
    } catch(e) {
      return {
        success: true,
        data: null,
        message: e
      }
    }
  }

  @GrpcMethod('Greeter', 'refreshToken')
  async refreshToken(
    data: AuthRefreshTokenRequest,
    meta: MetaData,
    call: ServerUnaryCall<any>
  ): Promise<AuthRefreshTokenResponse> {
    try {
      const token = meta.get('token')[0].toString();

      const resultData = await this.authService.refreshToken(token);

      return {
        success: true,
        data: resultData
      }
    } catch(e) {
      return {
        success: true,
        data: null,
        message: e
      }
    }
  }

  @GrpcMethod('Greeter', 'checkToken')
  async checkToken(
    data: AuthLoginRequest,
    meta: MetaData,
    call: ServerUnaryCall<any>
  ): Promise<any> {
    try {
      const token = data.body.token

      const resultData = await this.authService.getCurrentUser(token);

      return {
        success: true,
        data: resultData
      }
    } catch(e) {
      return {
        success: false,
        data: null,
        message: e
      }
    }
  }

  @GrpcMethod('Greeter', 'logout')
  async logout(
    data: AuthLogoutRequest,
    meta: MetaData,
    call: ServerUnaryCall<any>
  ): Promise<AuthLogoutResponse> {
    try {
      const token = meta.get('token')[0].toString();

      const resultData = await this.authService.logout(token);

      return {
        success: true,
        message: resultData
      }
    } catch(e) {
      return {
        success: true,
        message: e
      }
    }
  }
}
