import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from "@nestjs/common";
import { Observable, of } from "rxjs";
import { MetaData } from "src/utils/api/metadata.interface";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthInterceptor implements NestInterceptor {
    constructor(private authService: AuthService) {}

    async intercept(context: ExecutionContext, next: CallHandler): Promise<Observable<any>> {
        const metadata: MetaData = context.getArgByIndex(1);

        let authorization = metadata.get('token')[0];

        if(!authorization) { 
            return of ({
                success: false,
                data: null,
                message: 'Authorization token is not provided'
            })
        } 
        
        authorization = authorization.toString();

        try {
            const isAuthenticated = await this.authService.isAuthenticated(authorization);

            if(!isAuthenticated) {
                return of ({
                    success: false,
                    data: null,
                    message: 'User is not authenticated'
                })
            }
        } catch(e) {
            return of ({
                success: false,
                data: null,
                message: e
            });
        }

        return next
            .handle()
            .pipe();
    }
}