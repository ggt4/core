import { Global, Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { Session } from './constant/auth.constant';
import { SessionSchema } from './schemas/session.schema';
import { User } from '../user/constant/user.constant';
import { UserSchema } from '../user/schema/user.schema';
import { UserService } from '../user/user.service';

@Global()
@Module({
  controllers: [AuthController],
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { 
          expiresIn: configService.get<string>('JWT_EXPIRE') 
        }
      }),
      inject: [ConfigService]
    }),
    MongooseModule.forFeature([
      {
        name: Session,
        schema: SessionSchema,
      },
    ]),

    MongooseModule.forFeature([
      {
        name: User,
        schema: UserSchema,
      },
    ]),
  ],
  providers: [AuthService,UserService],
})
export class AuthModule {}
