import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { getConnectionToken, TypeOrmModule } from '@nestjs/typeorm';
import { Company } from 'src/company/company.entity';
import { MtDepartment } from 'src/mt-department/entities/mt-department.entity';
import { Plant } from 'src/plant/entities/plant.entity';
import { Department } from 'src/department/entities/department.entity';
import { User } from 'src/user/user.entity';
import { UserService } from 'src/user/user.service';
import { AuthService } from './auth.service';
import { Session } from './entities/session.entity';

jest.setTimeout(60000);

describe('AuthService', () => {
  let service: AuthService;
  let userService: UserService;
  let jwtService: JwtService;
  let connection;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AuthService, UserService],
      imports: [
        ConfigModule.forRoot({
          isGlobal: true,
          envFilePath: '.env.test',
        }),
        TypeOrmModule.forFeature([Session, User, Company, Department, MtDepartment, Plant]),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.DB_HOST,
          port: parseInt(process.env.DB_PORT),
          username: process.env.DB_USERNAME,
          password: process.env.DB_PASSWORD,
          database: process.env.DB_NAME,
          autoLoadEntities: true,
          synchronize: true,
        }),
        JwtModule.registerAsync({
          imports: [ConfigModule],
          useFactory: async (configService: ConfigService) => ({
            secret: configService.get<string>('JWT_SECRET'),
            signOptions: { 
              expiresIn: configService.get<string>('JWT_EXPIRE') 
            }
          }),
          inject: [ConfigService]
        }),
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
    jwtService = new JwtService(new JwtModule());
    connection = module.get(getConnectionToken());
  });

  afterAll( async () => { 
    await connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('login should return a token', async () => {
    await service.resetSession();
    await userService.resetTable();

    await userService.create({
      username: 'aaa',
      password: 'bbb'
    });

    const loginData = await service.login({
      username: 'aaa',
      password: 'bbb'
    });
    
    const isValidToken = jwtService.decode(loginData.token);

    expect(isValidToken).toBeTruthy();
  });

  it('refreshToken should return new token', async () => {
    await service.resetSession();
    await userService.resetTable();

    await userService.create({
      username: 'aaa',
      password: 'bbb'
    });

    const oldData = await service.login({
      username: 'aaa',
      password: 'bbb'
    });

    const newData = await service.refreshToken(`Bearer ${oldData.token}`);

    const isValidToken = jwtService.decode(newData.token);

    expect(isValidToken).toBeTruthy();
  });

  it('logout should logout successfully', async () => { 
    await service.resetSession();
    await userService.resetTable();
    
    await userService.create({
      username: 'aaa',
      password: 'bbb'
    });

    const loginData = await service.login({
      username: 'aaa',
      password: 'bbb'
    });

    const logoutMessage = await service.logout(`Bearer ${loginData.token}`);

    expect(logoutMessage).toBe(`Logout successfully`);
  });

  it('isAuthenticated should return true if user alredy logged in', async () => {
    await service.resetSession();
    await userService.resetTable();
    
    await userService.create({
      username: 'aaa',
      password: 'bbb'
    });

    const loginData = await service.login({
      username: 'aaa',
      password: 'bbb'
    });

    const isAuthenticated = await service.isAuthenticated(`Bearer ${loginData.token}`);

    expect(isAuthenticated).toBe(true);
  });
});
