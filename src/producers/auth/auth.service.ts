import { Injectable } from '@nestjs/common';
import { LoginAuthDto } from './dto/login-auth.dto';
import { compare } from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtDecode } from './interfaces/jwt.interface';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../user/constant/user.constant';
import { UserInterface } from '../user/interface/user.interface';
import { Model } from 'mongoose';
import { Session } from './constant/auth.constant';
import { SessionInterface } from './interfaces/session.interface';
import { jwtSecret } from 'src/configs/env.config';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User) private userModel: Model<UserInterface>,
    @InjectModel(Session) private sessionModel: Model<SessionInterface>,
    private jwtService: JwtService
    ) {}


  async login(loginAuthDto: LoginAuthDto): Promise<{ token: string }> {
    const { username, password } = loginAuthDto;

    const foundUser = await this.userModel.findOne({ username });
    
    if(!foundUser) throw "User doesn't exist";

    const isValidPassword = await compare(password, foundUser.password);

    if(!isValidPassword) throw "Password didn't match";

    const token = this.jwtService.sign(
      { 
      claims: { user: foundUser.id },
      }
    );

    if(!token) throw "Failed to generate token";

    const newSession = {
      userId: foundUser.id,
      token,
      loginAt: new Date()
    };

    await new this.sessionModel(newSession).save();

    return { token };
  }

  async getCurrentUser(authorization: string): Promise<UserInterface> {
    const [type, token] = authorization && authorization.split(' ');

    if(type !== 'Bearer') throw "Invalid authorization type";

    if(!token) throw "Error no authorization token";

    const foundSession = await this.sessionModel.findOne({ token });

    if(foundSession.logoutAt) throw "User alreafy logged out";

    const decodedJwt: JwtDecode = this.jwtService.verify(token);
    
    const foundUser = await this.userModel.findById(decodedJwt.claims.user);
    
    return foundUser;
  }

  async refreshToken(authorization: string): Promise<{ token: string }> {
    const [type, token] = authorization && authorization.split(' ');

    if(type !== 'Bearer') throw "Invalid authorization type";

    if(!token) throw "Error no authorization token";

    const oldDecodedJwt: JwtDecode = await this.jwtService.verify(token);

    delete oldDecodedJwt.exp;
    delete oldDecodedJwt.iat;

    const newToken = this.jwtService.sign(oldDecodedJwt);

    const newDecodedJwt: JwtDecode = await this.jwtService.verify(newToken);

    const foundUser = await this.userModel.findById(newDecodedJwt.claims.user);

    await this.sessionModel.updateOne({ user:  foundUser }, { token: newToken });

    return { token: newToken };
  }

  async checkToken(authorization: string): Promise<any> {
    const [type, token] = authorization && authorization.split(' ');

    if(type !== 'Bearer') throw "Invalid authorization type";

    if(!token) throw "Error no authorization token";

    const foundSession = await this.sessionModel.findOne({ token });

    if(foundSession.logoutAt) throw "User alreafy logged out";

    const decodedJwt: JwtDecode = this.jwtService.verify(token);
    
    const foundUser = await this.userModel.findById(decodedJwt.claims.user);
    
    return foundUser;
  }

  async logout(authorization: string) {
    const [type, token] = authorization && authorization.split(' ');

    if(type !== 'Bearer') throw "Invalid authorization type";

    if(!token) throw "Error no authorization token";

    await this.sessionModel.updateOne({ token }, { logoutAt: new Date() });

    return "Logout successfully";
  }

  async isAuthenticated(authorization: string): Promise<boolean> {
    const [type, token] = authorization && authorization.split(' ');

    if(type !== 'Bearer') throw "Invalid authorization type";

    if(!token) throw "Error no authorization token";

    try {
      const foundUser = await this.sessionModel.findOne({ token });

      if(!foundUser) throw "Invalid token";

      if(foundUser.logoutAt) throw "User already logged out";

      this.jwtService.verify(token);

      const decodedJwt = this.jwtService.decode(token);

      if(!decodedJwt) return false;

      return true;
    } catch(e) {
      throw e;
    }
  }
}
