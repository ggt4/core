export class LoginAuthDto {
    username: string;
    password: string;
    token: string;
}
