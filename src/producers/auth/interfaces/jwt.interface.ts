export interface JwtDecode {
    claims: {
        user: string;
    };
    iat: number;
    exp: number;
}