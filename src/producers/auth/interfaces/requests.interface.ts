import { LoginAuthDto } from "../dto/login-auth.dto";

export interface AuthLoginRequest {
    body: LoginAuthDto;
}

export interface AuthGetCurrentUserRequest { }

export interface AuthRefreshTokenRequest { }

export interface AuthLogoutRequest { }