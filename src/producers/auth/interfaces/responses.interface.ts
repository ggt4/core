import { BaseResponse } from "src/utils/api/responses.interface";

export interface AuthLoginResponse extends BaseResponse {
    data: {
        token: string;
    }
}

export interface AuthGetCurrentUserResponse extends BaseResponse { 
    data: {
        username: string;
        password: string;
    }
}

export interface AuthRefreshTokenResponse extends BaseResponse { 
    data: {
        token: string;
    }
}

export interface AuthLogoutResponse extends BaseResponse { }