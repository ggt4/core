export interface SessionInterface {
  id: string;
  userId: string;
  token: string;
  loginAt: Date;
  logoutAt: Date;
}