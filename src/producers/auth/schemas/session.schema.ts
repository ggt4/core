import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const SessionSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: 'User' },
  token: { type: String, default: '' },
  loginAt: { type: Date, default: new Date() },
  logoutAt: { type: Date, default: null },
  createdAt: { type: Date, default: new Date() },
  updatedAt: { type: Date, default: new Date() },
  deletedAt: { type: Date, default: null },
});
