import { BaseInterface } from 'src/utils/base/base.interface';

export interface UserInterface extends BaseInterface {
  username: string;
  password: string;
  isActive: boolean;
  additional: any;
  fullName: string;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  address: string;
  postCode: string;
  photo: string;
  cover: string;
  gender: string;
  religion: string;
}
