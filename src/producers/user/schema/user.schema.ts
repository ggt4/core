import * as mongoose from 'mongoose';
const schema = mongoose.Schema;

export const UserSchema = new schema(
  {
    username: { type: String, default: '' },
    password: { type: String, default: '' },
    isActive: { type: Boolean, default: false },
    additional: { type: schema.Types.Mixed, default: null },
    fullName: { type: String, default: '' },
    firstName: { type: String, default: '' },
    lastName: { type: String, default: '' },
    email: { type: String, default: '' },
    phone: { type: String, default: '' },
    address: { type: String, default: '' },
    postCode: { type: String, default: '' },
    photo: { type: String, default: '' },
    cover: { type: String, default: '' },
    gender: { type: String, default: '' },
    religion: { type: String, default: '' },
    createdBy: { type: Number, default: 0 },
    createdAt: { type: Date, default: new Date() },
    updatedAt: { type: Date, default: new Date() },
    deletedAt: { type: Date, default: null },
  },
  {
    toObject: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
    toJSON: {
      transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
      },
    },
  },
);
