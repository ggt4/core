import { FilterQuery, Types } from 'mongoose';
import { createPaginationOptions, Pagination } from 'src/utils/pagination';
import { UserInterface } from './interface/user.interface';
import { UserService } from './user.service';
import { Message } from 'src/utils/constants/response-message';
import { GrpcMethod } from '@nestjs/microservices';
import { User } from 'proto-bundle/protoBundle';
import { Controller } from '@nestjs/common';
import { genSalt, hash } from 'bcryptjs'

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @GrpcMethod('Greeter', 'users')
  async findAll(req: User.IRequest): Promise<User.IResponseGetAll> {
    try {
      const { filterBy, search, ids } = req.query || {};

      const paginationOptions = createPaginationOptions(req);

      const filter: FilterQuery<UserInterface> = {
        deletedAt: null,
      };

      if (
        filterBy &&
        search
      ) {
        filter[filterBy] = {
          $regex: `.*${search}.*`,
          $options: 'i',
        };
      }

      if (typeof ids && ids instanceof Array && ids.length) {
        const objectIds = ids.map((id) => Types.ObjectId(id));
        filter._id = { $in: objectIds };
      }

      const data = await this.userService.findAll(filter, paginationOptions);
      const pagination = new Pagination(null, paginationOptions);

      const result: User.IResponseGetAll = {
        data,
        message: Message.SHOWEDLIST,
        success: true,
        meta: pagination,
      };

      return result;
    } catch (e) {
      const result: User.IResponseGetAll = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'userCreate')
  async create(req: User.IRequest): Promise<User.IResponseCreate> {
    try {
      const { body } = req;
      const salt = await genSalt(12)
      body.password = await hash(body.password, salt)
      body.createdBy = req.user.id || 0;

      const data = await this.userService.create(body);

      const result: User.IResponseCreate = {
        data: data as User.ResponseGetAll.IData,
        message: Message.CREATED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: User.IResponseCreate = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'user')
  async detail(req: User.IRequest): Promise<User.IResponseDetail> {
    try {
      const { params } = req;
      const filter: FilterQuery<any> = {
        deletedAt: { $eq: null },
        _id: { $eq: Types.ObjectId(params.id) },
      };

      const data = await this.userService.findOne(filter);

      const result: User.IResponseDetail = {
        data: data as User.ResponseGetAll.IData,
        message: Message.SHOWDETAIL,
        success: true,
      };

      return result;
    } catch (e) {
      const result: User.IResponseDetail = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'userUpdate')
  async update(req: User.IRequest): Promise<User.IResponseUpdate> {
    try {
      const { params, body } = req;
      const data = await this.userService.update(params.id, body);

      const result: User.IResponseUpdate = {
        data: data as User.ResponseGetAll.IData,
        message: Message.UPDATED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: User.IResponseUpdate = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }

  @GrpcMethod('Greeter', 'userDelete')
  async delete(req: User.IRequest): Promise<User.IResponseDelete> {
    try {
      const { params, body } = req;
      const data = await this.userService.remove(params.id);

      const result: User.IResponseDelete = {
        data: null,
        message: Message.DELETED,
        success: true,
      };

      return result;
    } catch (e) {
      const result: User.IResponseDelete = {
        data: null,
        message: e || e.message,
        success: false,
      };

      return result;
    }
  }
}
