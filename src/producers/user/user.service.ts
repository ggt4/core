import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { PaginationOptions } from 'src/utils/pagination';
import { User } from './constant/user.constant';
import { UserInterface } from './interface/user.interface';

@Injectable()
export class UserService {
  constructor(@InjectModel(User) private userModel: Model<UserInterface>) {}

  async findAll(
    filter: FilterQuery<UserInterface>,
    paginationOptions: PaginationOptions,
  ) {
    const { skip, limit } = paginationOptions;
    const pipelines: any = [
      {
        $match: filter,
      },
      {
        $addFields: {
          id: '$_id',
        },
      },
      { $unset: ['_id', '__v'] },
    ];

    if (limit !== null && skip !== null) {
      pipelines.push({ $skip: skip }, { $limit: limit });
    }

    return await this.userModel.aggregate(pipelines).exec();
  }

  async findOne(filter) {
    const pipelines: any = [
      {
        $match: filter,
      },
      {
        $addFields: {
          id: '$_id',
        },
      },
      { $unset: ['_id', '__v'] },
    ];

    return await this.userModel
      .aggregate(pipelines)
      .exec()
      .then((items) => {
        if (items instanceof Array && items.length) {
          return items[0];
        }
        throw 'not found or not exist';
      });
  }

  async create(userDTO) {
    userDTO.createdAt = new Date();
    userDTO.updatedAt = new Date();

    const createUser = new this.userModel(userDTO);
    await createUser.save();
    return createUser;
  }

  async update(id: string, userDTO) {
    userDTO.updatedAt = new Date();
    await this.userModel.findByIdAndUpdate(id, userDTO);
    return await this.userModel.findById(id);
  }

  async remove(id: string) {
    const userDTO = {
      updatedAt: new Date(),
      deletedAt: new Date(),
    }
    await this.userModel.findByIdAndUpdate(id, userDTO);
    return await this.userModel.findById(id);
  }
}
