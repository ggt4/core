import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { seeder } from 'nestjs-seeder';
import databaseConfig from './configs/database.config';
import { UserSchema } from './producers/user/schema/user.schema';
import { UserSeeder } from './seeders/user.seeder';

seeder({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
    }),
    MongooseModule.forRoot(process.env.MONGODB_URL),
    MongooseModule.forFeature([
      {
        name: 'User',
        schema: UserSchema,
      },
    ]),
  ],
}).run([UserSeeder]);
