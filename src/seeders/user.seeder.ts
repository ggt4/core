import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Seeder } from 'nestjs-seeder';
import { UserInterface } from 'src/producers/user/interface/user.interface';
import { users } from './data';
import { genSalt, hash } from 'bcryptjs'

@Injectable()
export class UserSeeder implements Seeder {
  constructor(
    @InjectModel('User') private userModel: Model<UserInterface>,
  ) {}

  async seed(): Promise<any> {
    const fts = await this.userModel.find().exec();
    if (fts.length <= 0) {
      const usersTansform = Promise.all(users.map(async user=> {
        const salt = await genSalt(12)
        const pw = await hash(user.password, salt)
        user.password = String(pw)
        return user
      }))
      return await this.userModel.insertMany(await usersTansform);
    }
    return null;
  }

  async drop(): Promise<any> {
    return this.userModel.deleteMany();
  }
}
