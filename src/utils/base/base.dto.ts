import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';

export class BaseDTO {
  @ApiHideProperty()
  _id: string;

  @ApiHideProperty()
  public createdBy: number;

  @ApiHideProperty()
  public createdAt: Date;

  @ApiHideProperty()
  public updatedAt: Date = new Date();

  @ApiHideProperty()
  public deletedAt: Date;

  @ApiProperty({ default: null })
  public additional: object;
}
