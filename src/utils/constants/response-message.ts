export enum Message {
  UPDATED = 'Succesfully updated',
  CREATED = 'Succesfully created',
  DELETED = 'Succesfully deleted',
  SHOWEDLIST = 'Succesfully get list',
  SHOWDETAIL = 'Succesfully get detail',
  FAILED = 'Action failed',
  SUCCES = 'Action succes',
  NOT_FOUND = 'Not found',
}
