import { struct } from 'pb-util';

export class ProtobufTypes {
  public static structDecode(value: any): any {
    if (!value) return null;
    return struct.decode(value);
  }

  public static structEndcode(jsonValue: any): any {
    if (!jsonValue) return null;
    return struct.encode(jsonValue);
  }
}
