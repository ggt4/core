ECHO Generating typescript file from protobuf

node_modules/.bin/pbjs -t static-module -w commonjs -o proto-bundle/protoBundle.js protobufs/*/*.proto
node_modules/.bin/prettier --write proto-bundle/protoBundle.js


ECHO Generating typescript file from protobuf

node_modules/.bin/pbjs -t json protobufs/*/*.proto > proto-bundle/protoBundle.json

ECHO Generating typescript file from protobuf

node_modules/.bin/pbts -o proto-bundle/protoBundle.d.ts proto-bundle/protoBundle.js
node_modules/.bin/prettier --write proto-bundle/protoBundle.d.ts